const helper = require("../utils/helper");
const { createJwt, encrypt, decrypt } = require('../utils/common');
const { validationResult } = require('express-validator');

const axios = require('axios');

const {
    Users,
    Roles,
    PatientRegistration,
    Insurancepolicy,
    Specialist,
    Patientvisited
   
} = require('../models/kevellCareModels')
const moment = require('moment')
const mongoose = require('mongoose')
const _ = require('lodash');
const { forEach } = require("lodash");
const APIResp = require('../utils/APIResp')


const DoctorController = () => {
    const saveAndUpdateUser = async (req, res) => {

        try {
            let docotrid
            var password={}
           
            let userInput = helper.getReqValues(req)
 
           //.match(/^[0-9a-fA-F]{24}$/)
            if (req.body.Id==0) {
          
              docotrid =null 
            }   
            else{
                docotrid = await Users.findOne({ _id: req.body.Id })
                if(!docotrid) return APIResp.getSuccessResult('false', "Failed", res)
                
            }
            
            // if (req.body.Password === req.body.ConfirmPassword) {
            //      password = await encrypt(req.body.Password)
            // }
            // else {
            //     return res.json({
            //         success: false,
            //         error: 'password & confirmPassword mismatch!',
            //     })
            // }

        //    const rolefound= await Roles.findOne({_id:req.body.SelectedRoleId,RoleName:req.body.SelectedRole})
        //     if (!rolefound) return res.status(400).json({ success: false, error: "roleid not found" })
            
        //    const specilaistfound= await Specialist.findOne({_id:req.body.SelectedSpecialistId})
        //    if (!specilaistfound) return res.status(400).json({ success: false, error: "specialist id not found" })

        //    const insufound= await Insurancepolicy.findOne({_id:req.body.Insuranceid,Insurancename:req.body.Insurancename})
        //    if (!insufound) return res.status(400).json({ success: false, error: "insurance not found" })

            if (!docotrid) {

                const docotr = new Users({
                    "name": req.body.Name,
                    "usename": req.body.Username,
                    "password": req.body.Password,
                    // "ConfirmPassword": password, 
                    "Emailid": req.body.EmailId,
                    "MobileNo": req.body.MobileNo,
                    "Gender": req.body.Gender,
                    "Address": req.body.Address,
                    "City": req.body.City,
                    "Country": req.body.Country,
                    "Pincode": req.body.Pincode,
                    "roleid": req.body.SelectedRoleId,
                    "SpecialistId": req.body.SelectedRoleId == 1001 ? req.body.SelectedSpecialistId : null,
                    "IsActive": 1,
                    "ProfileImagelink": req.body.ProfileImagelink,
                    "ServerPath": req.body.ServerPath,
                    "DoctorProfile":{
                        "MedicalSchool": req.body.MedicalSchool,
                        "Yearsofexperience": req.body.Yearsofexperience,
                        "Languages": req.body.Languages,
                        "Doctorfees": req.body.Doctorfees,
                        "Insuranceid": req.body.Insuranceid,
                        "Insurancename": req.body.Insurancename,
                        "About": req.body.About,
                        "Qualification": req.body.Qualification,
                    },
                    // "Doctorapproved":false
                    "Doctorapproved":req.body.SelectedRoleId == 1001 ? false : true,
                    "Doctorrating":null

                })
                const savedDocotor = await docotr.save()

                // return res.status(200).json({
                //     success: true,
                //     message: 'Doctor saved successfully',
                //     registeredUserId: savedDocotor,
                // })
                return APIResp.getSuccessResult({Result:true}, "Success", res)
            }

            const updateit = await Users.updateOne(
                {
                    _id: req.body.Id
                },
                {
                    $set: {
                        "name": req.body.Name,
                        "usename": req.body.Username,
                        "password": req.body.Password,
                        // "ConfirmPassword": password, 
                        "Emailid": req.body.EmailId,
                        "MobileNo": req.body.MobileNo,
                        "Gender": req.body.Gender,
                        "Address": req.body.Address,
                        "City": req.body.City,
                        "Country": req.body.Country,
                        "Pincode": req.body.Pincode,
                        "roleid": req.body.SelectedRoleId,
                        "SpecialistId": req.body.SelectedSpecialistId,
                        "IsActive": 1,
                        "ProfileImagelink": req.body.ProfileImagelink,
                        "ServerPath": req.body.ServerPath,
                        "DoctorProfile":{
                            "MedicalSchool": req.body.MedicalSchool,
                            "Yearsofexperience": req.body.Yearsofexperience,
                            "Languages": req.body.Languages,
                            "Doctorfees": req.body.Doctorfees,
                            "Insuranceid": req.body.Insuranceid,
                            "Insurancename": req.body.Insurancename,
                            "About": req.body.About,
                            "Qualification": req.body.Qualification,
                        },
                        "Doctorrating":null
                        // "Doctorapproved":req.body.SelectedRoleId == 1001 ? false : true
                    }
                })
            // res.status(200).json({ success: true, message: "Updated Doctor !!", response: updateit })
            APIResp.getSuccessResult({Result:true}, "Success", res)

        } catch (err) {
            console.log(err)
            res.status(400).send({ error: `${err}` });
        }
    }
  
    const uploadProfileImage = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)

            // const updateit = await Users.updateOne(
            //     {
            //         _id: userInput.id
            //     },
            //     {
            //         $set: {
            //             "ProfileImagelink": req.files,
            //         }
            //     })
            res.status(200).json({Result:req.files[0].path})
    // APIResp.getSuccessResult({Result:req.files[0].path}, "Success", res)
        } catch (err) {
            res.status(400).send({ error: `${err}` });
        }
    }

    const deleteUser = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)

            const deluser = await Users.findOne({ _id: userInput.id })
            if (!deluser) {
                return res.status(400).json({ success: false, error: "doctor not found" })
            }

            const updateit = await Users.updateMany({ _id: { $in: [userInput.id] }, },
                {
                    $set: {
                        'IsActive': 0,
                    }
                })
            // res.status(200).json({ success: true, message: "Deleted doctor!!", response: updateit })
            res.status(200).json ({  "status": true,responsecode:200, message: "Success","data": {
                "Result": true
            } })
            
        }
        catch (err) {
            console.log(err)
            res.status(400).json({ error: `${err}` });
        }
    }
    const getUserdetailsBy = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)
            console.log(userInput,"userInput")
            const getallid = await Users.find({_id:userInput.userid}).populate([{
                path:'roleid',
                model:'Roles'
            }])
            .populate([{
                path:'DoctorProfile.Insuranceid',
                model:'Insurancepolicy'
            }])
            // SpecialistId
            .populate([{
                path:'SpecialistId',
                model:'Specialist'
            }])
           
            if(getallid.length ==0)      return res.status(400).json ({ success: false,responsecode:201, message: "mail dostnot match!","data": {
                "Result": []
            } })
            
            getallid.forEach((val)=>{
              console.log(val,"valvalvalvalvalvalvalvalval")
                let result ={}
                result["Id"]=val._id,
                result["Name"]=val.name,
                result["Username"]=val.usename,
                result["Password"]=val.password,
                result["ConfirmPassword"]=val.password,
                result["EmailId"]=val.Emailid,
                result["MobileNo"]=val.MobileNo,
                result["Gender"]=val.Gender,
                result["Address"]=val.Address,
                
                result["Pincode"]=val.Pincode,
               
               
                result["City"]=val.City,
                result["Country"]=val.Country,
                result["SpecialistId"]=val.SpecialistId._id,
                result["SelectedRoleId"]=val.roleid._id,
                result["SelectedRole"] = val.roleid.RoleName,
                result["IsActive"]=val.IsActive==1 ? true:false,
                result["SelectedSpecialistId"]=val.SpecialistId==null ? 0 :val.SpecialistId.name,
                
                result["Doctorfees"]=val.DoctorProfile.Doctorfees,
                // result["Qualification"]=val.Qualification,
                result["DoctorProfile"]="null",
                result["Doctorapproved"]=val.Doctorapproved,
                result["ProfileImagelink"]=val.ProfileImagelink,
                // result["ServerPath"]=val.ServerPath,
                
                
                result["MedicalSchool"]=val.DoctorProfile.MedicalSchool,
                result["Yearsofexperience"]=val.DoctorProfile.Yearsofexperience
                result["Languages"]=val.DoctorProfile.Languages,
                result["About"]=val.DoctorProfile.About,
                result["Insuranceid"]=val.DoctorProfile.Insuranceid == null ? "" : val.DoctorProfile.Insuranceid._id,
                result["Qualification"] = val.DoctorProfile.Qualification,
                result["Insurancename"]=val.DoctorProfile.Insuranceid == null ? "" : val.DoctorProfile.Insuranceid.Insurancename,
                
            
                APIResp.getSuccessResult(result, "Success", res)
            })
            // res.status(200).json(getallid)
            // const doctorLocation = await DoctorLocation.find()
            // res.status(200).json({ data: doctorLocation })
        }
        catch (err) {
            console.log(err);
        }
    }
    
    const GetUsernNameBy = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)
            // const getallid = await Users.find({Emailid:userInput.mailid})
            
            const doctorspeclist = await Users.find({ Emailid: userInput.mailid }).populate([{
                path: "roleid",
                model: "Roles"
            }])
            // res.status(200).json(doctorspeclist)
            if(doctorspeclist.length ==0)      return res.status(400).json ({ success: false,responsecode:201, message: "mail dostnot match!","data": {
                "Result": []
            } })
            let all=[]
            doctorspeclist.forEach((val)=>{
               
                let result ={}
                result["Id"]=val._id,
                result["Name"]=val.name,
                result["Username"]=val.usename,
                result["Emailid"]=val.Emailid,
                result["Password"]=val.password,
                result["UserType"]="",
                result["Role"]=val.roleid.RoleName
            all.push(result)
            
            })
            APIResp.getSuccessResult(helper.convertarrtoObj(all), "Success", res)
            
            // const doctorLocation = await DoctorLocation.find()
            // res.status(200).json({ data: doctorLocation })
           
        }
        catch (err) {
            console.log(err);
        }
    }
    const GetAllUsersBy = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)
            // const getallid = await Users.find({Emailid:userInput.mailid})
         
            const doctorspeclist = await Users.find({
                $and:[{ roleid: userInput.roleid,Doctorapproved:true,IsActive:1 }]
            }).populate([{
                path: "roleid",
                model: "Roles"
            }])
            // res.status(200).json(doctorspeclist)
            if(doctorspeclist.length ==0)      return res.status(400).json ({ 
                success: false,
                responsecode:201,
                 message: "mail dostnot match!",
                 "data": {
                "Result": []
            } })
           let result1 =[]
           let result ={}
            doctorspeclist.forEach((val)=>{
                
                 result ={}
                result["Id"]=val._id,
                result["Name"]=val.name,
                result["Username"]=val.usename,
                result["Emailid"]=val.Emailid,
                result["Password"]=val.password,
                result["MobileNo"]=val.MobileNo,
                result["Gender"]=val.Gender,
                result["City"]=val.City,
                result["Country"]=val.Country,
                result["Address"]=val.Address,
                result["Pincode"]=val.Pincode,
                result["SelectedRole"]=val.roleid.RoleName,
                result["SelectedRoleId"]=val.roleid._id,
                   result["SpecialistId"]=val.SpecialistId,
                   
              
               result1.push(result)
           
            })
            APIResp.getSuccessResult(result1, "Success", res)
            // const doctorLocation = await DoctorLocation.find()
            // res.status(200).json({ data: doctorLocation })
           
        }
        catch (err) {
            console.log(err);
            APIResp.getErrorResult(err,res)
        }
    }
    
     
    const getUserrole = async (req, res) => {
        try {
            // let userInput = helper.getReqValues(req)

            const userdetail = await Roles.find({})
            let obj=[];
            userdetail.forEach((e)=>{
               
                let result ={}
                result["Id"]=e._id,
                result["RoleName"]=e.RoleName,
                obj.push(result)
                
                
            })
              
            // res.status(200).json({ success: true, result: userdetail })
            APIResp.getSuccessResult(obj, "Success", res)
        }
        catch (err) {
            
            res.status(400).send({ error: `${err}` });
        }
    }
    const getNamerole = async (req, res) => {
     
        try {
            // let userInput = helper.getReqValues(req)
            let userInput = helper.getReqValues(req)
          
            
            const doctorspeclist = await Users.find({ usename: userInput.username })
            .populate([{
                path: "roleid",
                model: "Roles"
            }])

            
            // res.status(200).json(doctorspeclist)
            if(doctorspeclist.length ==0)      return res.status(400).json ({ success: false,responsecode:201, message: "username dostnot match!","data": {
                "Result": []
            } })
            let kioskStateall=[]
            doctorspeclist.forEach((val)=>{
               
                // let result ={}
              
                // result["Role"]=val.Roleid.RoleName
                kioskStateall.push(val.roleid.RoleName)
              
          
        })
        
        APIResp.getSuccessResult(kioskStateall, "Success", res)
    }
        catch (err) {
console.log(err)
            res.status(400).send({ error: `${err}` });
        }
    } 
    
    const GetAdminUserList = async (req, res) => {
        try {
            // let userInput = helper.getReqValues(req)
            let userInput = helper.getReqValues(req)
            // const getallid = await Users.find({Emailid:userInput.mailid})
            
            const doctorspeclist = await Users.find({ Roleid: 1000 }).populate([{
                path: "Roleid",
                model: "Roles"
            }])
            
            if(doctorspeclist.length ==0)      return res.status(400).json ({ success: false,responsecode:201, message: "admin dostnot match!","data": {
                "Result": []
            } })
            // res.status(200).json(doctorspeclist)
            let alladmin =[]
            let result ={}
            doctorspeclist.forEach((val)=>{
               
               
                result ={}
                result["Id"]=val._id
                result["Username"]=val.Username
                result["Name"]=val.Username
                result["Password"]=val.Password.iv
                result["EmailId"]=val.Emailid
                result["MobileNo"]=val.MoblieNo
                result["Address"]=val.City
                result["City"]=val.City
                result["Country"]="india"
                result["SelectedRoleId"]=val.Roleid._id
                result["SelectedRole"]=val.Roleid.RoleName
                result["SelectedSpecialistId"]=val.SpecialistId
                result["SelectedSpecialist"]="null"
            alladmin.push(result)
                
          
        })
        APIResp.getSuccessResult(alladmin, "Success", res)
    }
        catch (err) {

            res.status(400).send({ error: `${err}` });
        }
    } 

    const GetDoctorUserList = async (req, res) => {
        try {
            // let userInput = helper.getReqValues(req)
            let userInput = helper.getReqValues(req)
            // const getallid = await Users.find({Emailid:userInput.mailid})
            
            const doctorspeclist = await Users.find({ roleid: 1001 }).populate([{
                path: "roleid",
                model: "Roles"
            }]).populate([{
                path: "SpecialistId",
                model: "Specialist"
            }])
            
            if(doctorspeclist.length ==0)      return res.status(400).json ({ success: false,responsecode:201, message: "admin dostnot match!","data": {
                "Result": []
            } })
            // res.status(200).json(doctorspeclist)
            let alladmin =[]
            let result ={}
           
            doctorspeclist.forEach((val,i)=>{
               
               
                result ={}
                result["sno"]=i+1,
                result["Id"]=val._id
                result["Username"]=val.usename
                result["Name"]=val.name
                result["Password"]=val.Password
                result["EmailId"]=val.Emailid
                result["MobileNo"]=val.MobileNo
                result["Address"]=val.Address
                result["City"]=val.City
                result["Country"]=val.Country
                result["SelectedRoleId"]=val.roleid._id
                result["SelectedRole"]=val.roleid.RoleName
                result["SelectedSpecialistId"]=val.SpecialistId._id
                result["SelectedSpecialist"]=val.SpecialistId.name
              
                
            alladmin.push(result)
                
          
        })
        APIResp.getSuccessResult(alladmin, "Success", res)
    }
        catch (err) {

            res.status(400).send({ error: `${err}` });
        }
    } 
    const  IsEmailExits = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)
            const doctorlist = await Users.find(
                {
                    _id: userInput.Id,
                    Emailid: userInput.emailid
                })

                if(doctorlist.length ==0) {     return res.status(400).json ({ success: false,responsecode:201, message: "mail dostnot match!","data": {
                    "Result": true
                } })
            }
            else{
                return res.status(200).json ({ success: false,responsecode:200, message: "success","data": {
                    "Result": false
                } })
            }

                // APIResp.getSuccessResult(doctorlist, "Success", res)
            // APIResp.getSuccessResult(helper.convertarrtoObj(doctorlist)._doc.doctorscheduleinfo, "Success", res)

        } catch (err) {
            console.log(err)
            APIResp.getErrorResult(err, res)
        }
    }
    const  IsUserNameExits = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)
            const doctorlist = await Users.find(
                {
                    _id: userInput.Id,
                    Username: userInput.username
                })

                if(doctorlist.length ==0) {     return res.status(400).json ({ success: false,responsecode:201, message: "mail dostnot match!","data": {
                    "Result": true
                } })
            }
            else{
                return res.status(200).json ({ success: false,responsecode:200, message: "success","data": {
                    "Result": false
                } })
            }

                // APIResp.getSuccessResult(doctorlist, "Success", res)
            // APIResp.getSuccessResult(helper.convertarrtoObj(doctorlist)._doc.doctorscheduleinfo, "Success", res)

        } catch (err) {
            APIResp.getErrorResult(err, res)
        }
    }
    const  IsMobileNoExits = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)
            const doctorlist = await Users.find(
                {
                    _id: userInput.Id,
                    MoblieNo: userInput.MoblieNo
                })

                if(doctorlist.length ==0) {     return res.status(400).json ({ success: false,responsecode:201, message: "mail dostnot match!","data": {
                    "Result": true
                } })
            }
            else{
                return res.status(200).json ({ success: false,responsecode:200, message: "success","data": {
                    "Result": false
                } })
            }

                // APIResp.getSuccessResult(doctorlist, "Success", res)
            // APIResp.getSuccessResult(helper.convertarrtoObj(doctorlist)._doc.doctorscheduleinfo, "Success", res)

        } catch (err) {
            APIResp.getErrorResult(err, res)
        }
    }

    const gettoken=async (req,res)=>{
        try{
    let a=await createJwt('Users')
    // res.send(a)
    res.status(200).json({ access_token: a, Error: null })
        }
        catch(err){
            res.status(400).json({Error:err})
        }
    }
    return {
        
        saveAndUpdateUser,
        uploadProfileImage,
        deleteUser,
        getUserrole,
        getUserdetailsBy,
        getNamerole,
        GetUsernNameBy,
        GetAllUsersBy,
        IsEmailExits,
        IsMobileNoExits,
        IsUserNameExits,
        GetAdminUserList,
        GetDoctorUserList,
gettoken
    }
}

module.exports = DoctorController()

