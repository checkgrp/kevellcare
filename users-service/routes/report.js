const express = require('express');
const router = express.Router();
const reportController = require('../controllers/reportController')
const upload = require('../utils/multer')
const {verifyToken} = require('../utils/verifyToken')

// const {
//   validateUserSignUpfields,
//   // validateUserLoginfields,
//   validateUserForgotPasswordFields,
//   // validateUserResetPasswordFields
// } = require('../validations/doctorValidation');
// const upload = require('../utils/multer')
// const auth = require("../utils/auth")


// const { verifyDoctorAuthentication } = require('./verifyDoctorAuthentication');


router.post('/saveAndUpdateUser',reportController.saveAndUpdateUser)
router.post('/uploadProfileImage', upload.array('file',1),reportController.uploadProfileImage)
router.post('/DeleteUser',reportController.deleteUser)
// router.get('/getUserdetailsBy',verifyToken,reportController.getUserdetailsBy)
router.get('/getUserdetailsBy',reportController.getUserdetailsBy)
//IsUserNameExits
//IsMobileNoExits
router.get('/IsUserNameExits',reportController.IsUserNameExits);
router.get('/IsMobileNoExits',reportController.IsMobileNoExits);
router.get('/getRoles',reportController.getUserrole);
//IsEmailExits
//GetAllUsersBy
router.get('/IsEmailExits',reportController.IsEmailExits);
// router.get('/GetAllUsersBy',verifyToken,reportController.GetAllUsersBy);
router.get('/GetAllUsersBy',reportController.GetAllUsersBy);
router.get('/GetRolesForUser',reportController.getNamerole);
router.get('/GetUsernNameBy',verifyToken,reportController.GetUsernNameBy);
router.get('/GetAdminUserList',verifyToken,reportController.GetAdminUserList);


router.get('/GetDoctorUserList',verifyToken,reportController.GetDoctorUserList);

router.get('/gettoken',reportController.gettoken);


//GetUsernNameBy


module.exports = router;
