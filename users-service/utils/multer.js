const multer = require('multer');


var multerStorage  = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/pdf')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now()+file.originalname);
  }
})
//MULTER FILTER
const multerFilter = (req, file, cb) => {
  // console.log(req,file)
  if (file.mimetype.split("/")[1] === "octet-stream") {
    cb(null, true);
  } else {
    cb(new Error("Not a jpeg File!!"), false);
  }
};

const upload = multer({
  storage: multerStorage,
  fileFilter: multerFilter,
});



module.exports = upload