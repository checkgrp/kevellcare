require('dotenv').config()
const jwt = require("jsonwebtoken");

//Use the ApiKey and APISecret from config.js
const payload = {
    iss: process.env.APIKey,
    exp: ((new Date()).getTime() + 5000)
};
const token = jwt.sign(payload, process.env.APISecret);

function addToken(req, res, next) {
    req.body["token"] = token;
    next();
}

module.exports = { addToken }