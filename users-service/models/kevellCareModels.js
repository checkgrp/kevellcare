const mongoose = require('mongoose')
const autoIncrement = require("mongoose-auto-increment");

const PatientvisitedSchema = new mongoose.Schema({
    visiteddate: {type: Date},
    patientId: {type: String},
    patientinfo: {type: JSON},
    temperatureinfo: {type: JSON},
    bpinfo: {type: JSON},
    hwinfo: {type: JSON},
    spO2info: {type: JSON},
    bodyfatinfo: {type: JSON},
    ecginfo: {type: Date},
    stestoscopeinfo: {type: JSON},
    otoscopeinfo1: {type: JSON},
    otoscopeinfo2: {type: JSON},
    otoscopeinfo3: {type: JSON},
    otoscopeinfo4: {type: Date},
    otoscopeinfo5: {type: JSON},
    otoscopeinfo6: {type: JSON},
    otoscopeinfo7: {type: JSON},
    otoscopeinfo8: {type: JSON},
    otoscopeinfo9: {type: Date},
    otoscopeinfo10: {type: JSON},
    appointmentId: {type: Number},
    patientDescription: {type: Date},
    visistedstarttime:{type: String},
    visistedendtime:{type: String},
    glucometerinfo:{type: String},
    doctorid: {type: Number},
    createdby:{type: String},
    modifiedby:{type: String}
},{ timestamps: true });
const UsersSchema = new mongoose.Schema({
    name: { type: String },
    usename: { type: String },
    password: { type: String },
    ConfirmPassword: { type: JSON },
    roleid: {type: Number},
    Emailid: { type: String },
    MobileNo: { type: String },
    Gender: { type: String },
    City: { type: String }, 
    Country: { type: String },
    Address: { type: String },
    Pincode: { type: String },
    SpecialistId: {type: Number},
    IsActive: { type: Number },
    ProfileImagelink: { type: String },
    DoctorProfile: { type: JSON },
    Doctorrating: {type: JSON},
    Doctorapproved: {type: Boolean},
    createdby: { type: String },
    modifiedby: { type: String }
}, { timestamps: true });
autoIncrement.initialize(mongoose.connection);
UsersSchema.plugin(autoIncrement.plugin, {
  model: "Users", // collection or table name in which you want to apply auto increment
  field: "_id", // field of model which you want to auto increment
  startAt: 1000, // start your auto increment value from 1
  incrementBy: 1, // incremented by 1
});

const PatientRegistrationSchema = new mongoose.Schema({
    // patientid: {type: String},
    patientinfo: {type: JSON},
    documentinfo: {type: JSON},
    appointmentdate: {type: Date},
    kittype: {type: String},
    createdby:{type: String},
    modifiedby:{type: String},
    otp:{type:String}
},{ timestamps: true });

const RolesSchema = new mongoose.Schema({

    RoleName: {type: String},
    createdby:{type: String},
    modifiedby:{type: String}
},{ timestamps: true });
autoIncrement.initialize(mongoose.connection);
RolesSchema.plugin(autoIncrement.plugin, {
  model: "Roles", // collection or table name in which you want to apply auto increment
  field: "_id", // field of model which you want to auto increment
  startAt: 1000, // start your auto increment value from 1
  incrementBy: 1, // incremented by 1
});
const InsurancepolicySchema = new mongoose.Schema({
    Insurancename: {type: String},
},{ timestamps: false });
autoIncrement.initialize(mongoose.connection);
InsurancepolicySchema.plugin(autoIncrement.plugin, {
  model: "Insurancepolicy", // collection or table name in which you want to apply auto increment
  field: "_id", // field of model which you want to auto increment
  startAt: 1000, // start your auto increment value from 1
  incrementBy: 1, // incremented by 1
});

const SpecialistSchema = new mongoose.Schema({
    name: {type: String},
    createdby:{type: String},
    modifiedby:{type: String}
},{ timestamps: true });
autoIncrement.initialize(mongoose.connection);
SpecialistSchema.plugin(autoIncrement.plugin, {
  model: "Specialist", // collection or table name in which you want to apply auto increment
  field: "_id", // field of model which you want to auto increment
  startAt: 1, // start your auto increment value from 1
  incrementBy: 1, // incremented by 1
});

const Patientvisited= mongoose.model('Patientvisited', PatientvisitedSchema,'Patientvisited');
const PatientRegistration= mongoose.model('PatientRegistration', PatientRegistrationSchema,'PatientRegistration');
const Users= mongoose.model('Users', UsersSchema,'Users');
const Roles= mongoose.model('Roles', RolesSchema,'Roles');
const Insurancepolicy = mongoose.model('Insurancepolicy', InsurancepolicySchema,'Insurancepolicy');
const Specialist= mongoose.model('Specialist', SpecialistSchema,'Specialist');


module.exports = {
   
    Patientvisited,
    PatientRegistration,
    Users,
    Roles,
    Insurancepolicy,
    Specialist

    
}