var express = require('express');
var router = express.Router();
const doctorRoute = require('./users')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('index');
});

router.use('/zoom', doctorRoute);

module.exports = router;
