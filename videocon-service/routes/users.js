const express = require('express');
const router = express.Router();
const DoctorController = require('../controllers/zoomController')

// const {
//   validateUserSignUpfields,
//   // validateUserLoginfields,
//   validateUserForgotPasswordFields,
//   // validateUserResetPasswordFields
// } = require('../validations/doctorValidation');
// const upload = require('../utils/multer')
const auth = require("../utils/auth")
const {verifyToken} = require('../utils/verifyToken')


// const { verifyDoctorAuthentication } = require('./verifyDoctorAuthentication');

router.post("/GetDoctorMeetingbyIds",verifyToken,DoctorController.GetDoctorMeetingbyIds)
router.get("/GetDoctorsWithMeeting",verifyToken,DoctorController.GetDoctorsWithMeeting)
router.post("/CreateDoctorMeeting",[verifyToken,auth.addToken],DoctorController.CreateDoctorMeetingbydoctor)//
router.post("/GetZoomMeetingInfo",verifyToken,DoctorController.GetZoomMeetingInfo)
router.post("/CreateZoomMeeting", [verifyToken,auth.addToken], DoctorController.createZoomMeeting)

router.get('/gettoken',DoctorController.gettoken);

module.exports = router;



