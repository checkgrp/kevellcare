const helper = require("../utils/helper");
const { createJwt, encrypt, decrypt } = require("../utils/common");
const { validationResult } = require("express-validator");
// const sendEmail = require("../utils/sendEmail")
const axios = require("axios");

const {
  Users,
  PatientRegistration,
  Roles,
  Specialist,
  DoctorSchedule,
  Patientvisited,
  PatientKitMapping,
  ZoomMeetingSettings,
} = require("../models/kevellCareModels");
const moment = require("moment");
const mongoose = require("mongoose");
const _ = require("lodash");
const { forEach } = require("lodash");
const APIResp = require("../utils/APIResp.js");
let crypto = require("crypto");
const DoctorController = () => {
  const GetDoctorMeetingbyIds = async (req, res) => {
    try {
      let userInput = helper.getReqValues(req);
      console.log(userInput, "----");

      let all = [];
      for (var i = 0; i <= Object.keys(userInput).length - 1; i++) {
        let doctorlist;
        doctorlist = await Users.find({ _id: userInput[i].doctorid })
          .populate([
            {
              path: "SpecialistId",
              model: "Specialist",
            },
          ])
          .populate([
            {
              path: "roleid",
              model: "Roles",
            },
          ]);
        var a = helper.convertarrtoObj(doctorlist);
        all.push(a._doc);
      }

      let resultall = [];
      all.forEach(async (val, i) => {
        if (val.roleid.RoleName == "Doctor") {
          var len = await ZoomMeetingSettings.find({ doctorId: val._id });
          // console.log(len, "----");
          if (len.length == 0) {
            //empty obj set
            let result = {};
            (result["Doctorid"] = val._id),
              (result["Zoomid"] = 0),
              (result["DoctorName"] = val.usename),
              (result["Location"] = val.City),
              (result["SpecialistName"] =
                val.SpecialistId === null ? null : val.SpecialistId.name),
              (result["MeetingId"] = ""),
              (result["Meetingpassword"] = ""),
              (result["Meetinglink"] = ""),
              (result["Isactive"] = 0);
            resultall.push(result);
          } else {
            //fetch
            let result = {};
            (result["Doctorid"] = val._id),
              (result["Zoomid"] = len[0]._id),
              (result["DoctorName"] = val.usename),
              (result["Location"] = val.City),
              (result["SpecialistName"] =
                val.SpecialistId === null ? null : val.SpecialistId.name),
              (result["MeetingId"] = len[0].meetingId),
              (result["Meetingpassword"] = len[0].meetingpassword),
              (result["Meetinglink"] = len[0].meetinglink),
              (result["Isactive"] = len[0].IsActive == true ? 1 : 0);
            resultall.push(result);
          }
        }

        if (i + 1 == all.length) {
          return APIResp.getSuccessResult(resultall, "Success", res);
        }
      });

      // APIResp.getSuccessResult(resultall, "Success", res)
    } catch (err) {
      console.log(err);
      res.status(400).json({ error: `${err}` });
    }
  };

  const GetZoomMeetingInfo = async (req, res) => {
    try {
      let currentdata = [];
      const todayPatient = await ZoomMeetingSettings.find();
      let isactive;
      todayPatient.forEach((e) => {
        // console.log(isactive);
        if (e.isactive == true) {
          isactive = e;
          return;
        }
      });
      console.log(isactive);
      //  isactive.forEach((val,i) => {
      let result1 = {};
      result1["MeetingId"] = isactive.meetingId;
      result1["MeetingPassword"] = isactive.meetingpassword;
      result1["MeetingLink"] = "";
      // if(i+1 == isactive.length){
      APIResp.getSuccessResult(result1, "Success", res);
      //   }
      // });
      // res.status(200).json({ success: true, result: isactive });
    } catch (err) {
      res.status(400).json({ error: `${err}` });
    }
  };

  const GetDoctorsWithMeeting = async (req, res) => {
    // let userInput = helper.getReqValues(req)
    try {
      let resultall = [];
      const doctorlist = await Users.find({})
        .populate([
          {
            path: "SpecialistId",
            model: "Specialist",
          },
        ])
        .populate([
          {
            path: "roleid",
            model: "Roles",
          },
        ]);
      var docid = [];
      var doclist = [];
      doctorlist.forEach(async (val, i) => {
        docid.push(val._id);
        doclist.push(val);
      });
      var len = await ZoomMeetingSettings.find({ doctorId: { $in: docid } });
      const results = doclist.filter(
        ({ _id: id1 }) => !len.some(({ doctorId: id2 }) => id2 === id1)
      );

      results.forEach((val) => {
           
        if (val.roleid.RoleName == "Doctor") {
          var result = {
            Doctorid:val._id,
            Zoomid:0,
            DoctorName:val.usename,
            Location:val.City,
            SpecialistName:val.SpecialistId === null ? null : val.SpecialistId.name,
            MeetingId:"",
            Meetingpassword:"",
            Meetinglink:"",
            Isactive:0
          };
        }
        resultall.push(result);
      });

      len.forEach((val)=>{
        var result = {
          Doctorid:val.doctorId,
          DoctorName: doclist.filter(v=>v._id==val.doctorId)[0].usename,
          Location: doclist.filter(v=>v._id==val.doctorId)[0].City,
          SpecialistName: doclist.filter(v=>v._id==val.doctorId)[0].SpecialistId=== null ? null : doclist.filter(v=>v._id==val.doctorId)[0].SpecialistId.name,
          MeetingId:val.meetingId,
          Meetingpassword:val.meetingpassword,
          Meetinglink:val.meetinglink,
          Isactive:val.IsActive == true ? 1 : 0
        };
          resultall.push(result);
      })
        APIResp.getSuccessResult(resultall.filter(v=>v!=null), "Success", res);
    } catch (err) {
      res.status(400).send({ error: `${err}` });
    }
  };

  const CreateDoctorMeetingbydoctor = async (req, res) => {
    let userInput = helper.getReqValues(req);
    console.log("userInput", userInput.doctorId);
    const doctorprofilelist = await ZoomMeetingSettings.find({
      doctorId: userInput.doctorId,
    });
    console.log(doctorprofilelist, "---");
    if (doctorprofilelist.length == 0) {
      //insert
      console.log("insert");
      const doctoravailable = await Users.findOne({ _id: userInput.doctorId });
      // console.log(doctoravailable,"doctoravailable")
      if (!doctoravailable)
        return res.json({ message: "docotr id not available!" });

      console.log("userInput.doctorId", userInput.doctorId);
      // console.log("doctorprofilelist",doctorprofilelist)

      try {
        const token = req.body.token;
        const email = "kaleeswaran.b@kevells.com"; //host email id;
        const result = await axios.post(
          "https://api.zoom.us/v2/users/" + email + "/meetings",
          {
            topic: "Discussion about today's Demo",
            type: 2,
            start_time: "2021-03-18T17:00:00",
            duration: 20,
            timezone: "India",
            password: "12345678",
            agenda: "We will discuss about Today's Demo process",
            settings: {
              host_video: true,
              participant_video: true,
              cn_meeting: false,
              in_meeting: true,
              join_before_host: false,
              mute_upon_entry: false,
              watermark: false,
              use_pmi: false,
              approval_type: 2,
              audio: "both",
              auto_recording: "local",
              enforce_login: false,
              registrants_email_notification: false,
              waiting_room: true,
              allow_multiple_devices: true,
            },
          },
          {
            headers: {
              Authorization: "Bearer " + token,
              "User-Agent": "Zoom-api-Jwt-Request",
              "content-type": "application/json",
            },
          }
        );
        // sendResponse.setSuccess(200, 'Success', result.data);
        // let meetingId = result.data.id;
        // let meetinglink =result.data.join_url;
        // let meetingpassword = result.data.password;
        const user = new ZoomMeetingSettings({
          meetingId: result.data.id,
          meetinglink: result.data.join_url,
          meetingpassword: result.data.password,
          doctorId: userInput.doctorId,
          isactive: true,
        });
        const savedUser = await user.save();

        //    const personinsert = await ZoomMeetingSettings.insertOne(
        //     { doctorId: userInput.doctorId,
        //         meetinglink:result.data.join_url
        //     })
        //  { $set: {meetingId:meetingId ,meetinglink:meetinglink,meetingpassword:meetingpassword } })
        //  res.status(200).json({
        //       message: "Inserted----",

        //    });
        APIResp.getSuccessResult({ Result: true }, "Success", res);
      } catch (error) {
        console.log("err", error);
        res.send(error.message);
      }
    } else {
      //update
      try {
        console.log("update");
        const token = req.body.token;
        const email = "kaleeswaran.b@kevells.com"; //host email id;
        const result = await axios.post(
          "https://api.zoom.us/v2/users/" + email + "/meetings",
          {
            topic: "Discussion about today's Demo",
            type: 2,
            start_time: "2021-03-18T17:00:00",
            duration: 20,
            timezone: "India",
            password: "1234567",
            agenda: "We will discuss about Today's Demo process",
            settings: {
              host_video: true,
              participant_video: true,
              cn_meeting: false,
              in_meeting: true,
              join_before_host: false,
              mute_upon_entry: false,
              watermark: false,
              use_pmi: false,
              approval_type: 2,
              audio: "both",
              auto_recording: "local",
              enforce_login: false,
              registrants_email_notification: false,
              waiting_room: true,
              allow_multiple_devices: true,
            },
          },
          {
            headers: {
              Authorization: "Bearer " + token,
              "User-Agent": "Zoom-api-Jwt-Request",
              "content-type": "application/json",
            },
          }
        );
        // sendResponse.setSuccess(200, 'Success', result.data);
        let meetingId = result.data.id;
        let meetinglink = result.data.join_url;
        let meetingpassword = result.data.password;

        const personupdate = await ZoomMeetingSettings.updateOne(
          { doctorId: userInput.doctorId },
          {
            $set: {
              meetingId: meetingId,
              meetinglink: meetinglink,
              meetingpassword: meetingpassword,
            },
          }
        );
        //  res.status(200).send({
        //       message: "update Successfully"
        //    });
        //   return res.status(200).json({ success: true,responsecode:200, message: "success","data": {
        //     "Result": true
        // } })
        return APIResp.getSuccessResult({ Result: true }, "Success", res);
      } catch (error) {
        console.log("errwww", error);
        res.send(error.message);
      }
    }
  };

  const createZoomMeeting = async (req, res) => {
    try {
      let result1 = {};
      const token = req.body.token;
      const email = "kaleeswaran.b@kevells.com"; //host email id;
      const result = await axios.post(
        "https://api.zoom.us/v2/users/" + email + "/meetings",
        {
          topic: "Discussion about today's Demo",
          type: 2,
          start_time: "2021-03-18T17:00:00",
          duration: 20,
          timezone: "India",
          password: "1234567",
          agenda: "We will discuss about Today's Demo process",
          settings: {
            host_video: true,
            participant_video: true,
            cn_meeting: false,
            in_meeting: true,
            join_before_host: false,
            mute_upon_entry: false,
            watermark: false,
            use_pmi: false,
            approval_type: 2,
            audio: "both",
            auto_recording: "local",
            enforce_login: false,
            registrants_email_notification: false,
            waiting_room: true,
            allow_multiple_devices: true,
          },
        },
        {
          headers: {
            Authorization: "Bearer " + token,
            "User-Agent": "Zoom-api-Jwt-Request",
            "content-type": "application/json",
          },
        }
      );
      // sendResponse.setSuccess(200, 'Success', result.data);
      // res.send(result.data)
      let dataall = [];
      dataall.push(result.data);
      console.log("dataall", dataall);
      dataall.forEach((val) => {
        (result1["MeetingId"] = val.id),
          (result1["MeetingPassword"] = val.password),
          (result1["MeetingLink"] = val.join_url);
        //  MeetingPassword
      });
      APIResp.getSuccessResult([result1], "Success", res);
    } catch (error) {
      res.send(error.message);
    } // return sendResponse.send(res);
  };
  const gettoken = async (req, res) => {
    try {
      let a = await createJwt("VideoCon");
      // res.send(a)
      res.status(200).json({ access_token: a, Error: null });
    } catch (err) {
      res.status(400).json({ Error: err });
    }
  };
  return {
    GetDoctorMeetingbyIds,
    CreateDoctorMeetingbydoctor,
    GetDoctorsWithMeeting,
    GetZoomMeetingInfo,
    createZoomMeeting,
    gettoken,
  };
};

module.exports = DoctorController();
