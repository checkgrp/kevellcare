const helper = require("../utils/helper");
const { createJwt, encrypt, decrypt } = require("../utils/common");
const { validationResult } = require("express-validator");
const sendEmail = require("../utils/sendEmail");
const axios = require("axios");
const APIResp = require("../utils/APIResp");

const {
  Users,
  PatientRegistration,
  Roles,
  Specialist,
  DoctorSchedule,
  Patientvisited,
  PatientKitMapping,
  ZoomMeetingSettings,
  Insurancepolicy,
} = require("../models/kevellCareModels");
const moment = require("moment");
const mongoose = require("mongoose");
const _ = require("lodash");
const { forEach } = require("lodash");
const fs = require("fs");
var AWS = require("aws-sdk");

const s3 = new AWS.S3({
  accessKeyId: process.env.AWS_S3_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_S3_SECRET_ACCESS_KEY,
})


const DoctorController = () => {
  const register = async (req, res) => {
    let userInput = helper.getReqValues(req);
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      const err = errors.array();
      return res.status(400).json({
        success: false,
        error: err[0].msg,
      });
    }

    const usrnameExist = await Users.findOne({ Username: userInput.username });
    if (usrnameExist)
      return res.status(400).json({ error: "username already exists!" });

    const emailExist = await Users.findOne({ Emailid: userInput.email });
    if (emailExist)
      return res.status(400).json({ error: "Email already exists!" });

    const MoblieNoExist = await Users.findOne({ MoblieNo: userInput.mobile });
    if (MoblieNoExist)
      return res.status(400).json({ error: "MoblieNo already exists!" });

    const RoleExist = await Roles.findOne({ RoleName: userInput.role });
    if (!RoleExist)
      return res.status(400).json({ error: "Role does not exists!" });

    const SpecialistExist = await Specialist.findOne({
      name: userInput.Specialist,
    });
    if (!SpecialistExist)
      return res.status(400).json({ error: "Specialist does not exists!" });

    const InsuranceExist = await Insurancepolicy.findOne({
      Insurancename: userInput.Insurance,
    });
    if (!InsuranceExist)
      return res.status(400).json({ error: "Insurance does not exists!" });

    var hashedPassword = await encrypt(userInput.password);

    const user = new Users({
      Username: userInput.username,
      Emailid: userInput.email,
      Password: hashedPassword,
      MoblieNo: userInput.mobile,
      Roleid: RoleExist._id,
      SpecialistId: SpecialistExist._id,
      IsActive: userInputIsActive,
      City: userInputCity,
      Doctorapproved: userInput.Doctorapproved,
      Insuranceid: InsuranceExist_id,
      ProfileImagelink: userInputProfileimg,
      Doctorfees: userInputdoctorfees,
      Languages: userInputLanguages,
      About: userInputAbout,
      Qualification: userInputQualification,
      Yearsofexperience: userInputYearsofexperience,
      Doctorcollege: userInputDoctorcollege,
    });
    try {
      const savedUser = await user.save();

      // const token = createJwt(user._id)
      // res.header('auth-token', token)
      res.status(200).json({
        success: true,
        message: "Registered successfully",
        registeredUserId: user._id,
        // accessToken: token
      });
    } catch (err) {
      res.status(400).send({ error: `${err}` });
    }
  };

  const login = async (req, res) => {
    try {
      let userInput = helper.getReqValues(req);
      console.log(userInput.username, "---------");
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        const err = errors.array();
        return res.status(400).json({
          success: false,
          error: err[0].msg,
        });
      }
      const user = await Users.findOne({ Username: userInput.username });
      if (!user)
        return res.status(400).json({ error: "username does not exists" });

      var decryptPassword = await decrypt(user.Password);
      if (decryptPassword !== userInput.password)
        return res.status(400).json({ error: "Invalid Password" });

      const roleexists = await Users.findOne({ Role: user.Roleid }).populate([
        {
          path: "Roleid",
          model: "Roles",
        },
      ]);

      if (roleexists.Roleid.RoleName !== userInput.role)
        return res.status(400).json({ error: "Invalid role" });

      const token = createJwt(user._id);

      res.header("token", token);
      res.json({
        success: true,
        message: "Logged in successfully",
        loginUserId: user._id,
        accessToken: token,
      });
    } catch (err) {
      res.status(400).json({ error: `${err}` });
    }
  };

  const forgotpassword = async (req, res) => {
    let userInput = helper.getReqValues(req);

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      const err = errors.array();
      return res.status(400).json({
        success: false,
        error: err[0].msg,
      });
    }

    try {
      const user = await Users.findOne({ Emailid: userInput.email });
      if (!user) {
        return res
          .status(400)
          .json({ success: false, error: "Email could not be sent" });
      }

      var decryptPassword = await decrypt(user.password);
      // const resetUrl = `http://localhost:5000/UserAuthenticationAPI/api/UserAuthentication/reset/${resetToken}`
      const message = `
     <h3>please take your password</h3>
     <p>${decryptPassword}</p>
     `;
      try {
        await sendEmail({
          to: user.Emailid,
          subject: "Forgot Password Request",
          text: message,
        });
        res.status(200).json({ success: true, message: "Email sented" });
      } catch (error) {
        res
          .status(500)
          .json({ success: false, error: "Email could not be sent" });
      }
    } catch (err) {
      res.status(400).json({ error: `${err}` });
    }
  };

  // const visitappointment=(req,res) =>{

  // }

  const getAllDoctorRoles = async (req, res) => {
    try {
      let userInput = helper.getReqValues(req);

      //'Roleid.RoleName':userInput.role
      const doctorlist = await Users.find({}).populate([
        {
          path: "Roleid",
          model: "Roles",
          match: { RoleName: userInput.role },
          select: "RoleName -_id",
        },
      ]);

      if (!helper.convertarrtoObj(doctorlist)._doc.Roleid)
        return res.status(400).json({ error: "Role not found!" });

      res
        .status(200)
        .json({
          success: true,
          result: doctorlist.filter((item) => item.Roleid !== null),
        });
    } catch (err) {
      res.status(400).send({ error: `${err}` });
    }
  };

  const getDoctorsList = async (req, res) => {
    try {
      let obj = [];
      let result = {};
      const doctorlist = await Users.find({})
        .populate([
          {
            path: "roleid",
            model: "Roles",
          },
        ])
        .populate([
          {
            path: "SpecialistId",
            model: "Specialist",
          },
        ]);

      doctorlist.forEach((val, i) => {
        console.log(val.roleid._id === 1001, "___________________")
        if (val.roleid._id === 1001 && val.Doctorapproved === true) {
          result = {};

          (result["Id"] = val._id),
            (result["DoctorName"] = val.usename),
            (result["SpecialistId"] =
              val.SpecialistId == null ? 0 : val.SpecialistId._id),
            (result["Specialist"] =
              val.SpecialistId == null ? null : val.SpecialistId.name),
            (result["City"] = val.City);
          obj.push(result);
          // if(i+1 == )
        }
      });
      // console.log(obj,"---")
      // res.status(200).json({ success: true, result: doctorlist })
      APIResp.getSuccessResult(obj, "Success", res);
    } catch (err) {
      console.log(err);
      res.status(400).send({ error: `${err}` });
    }
  };
  const GetAvailableDoctors = async (req, res) => {
    let obj = [];
    try {
      let ghj = [];
      var currentmonth = moment().month() + 1;
      var currentyear = moment().year();

      let doctorschedulelist = await DoctorSchedule.find({
        $and: [
          {
            schedulemonth: currentmonth,
            scheduleyear: currentyear,
          },
        ],
      });
      // console.log("doctorschedulelist[0].doctorscheduleinfo",doctorschedulelist[0].doctorscheduleinfo)
      doctorschedulelist[0].doctorscheduleinfo.map(async (val) => {
        //

        var t;
        if (
          val.Status == "Available" &&
          moment(val.StartDate).format("DD/MM/YYYY") ==
          moment(new Date()).format("DD/MM/YYYY")
        ) {
          ghj.push(val.DoctorID);

        }
      });
      console.log(ghj, "ghj")
      var result = [];
      var vall = []

      ghj.forEach(async (val, i) => {
        vall.push(parseInt(val))
      })

      var t = await Users.find({ _id: { $in: vall } }).populate([
        {
          path: "SpecialistId",
          model: "Specialist",
        },
      ]);
      console.log(t.length, "t----")
      result.push(t);
      // console.log("new one",result)

      // console.log(result,"result")
      // if (i + 1 == ghj.length) {
      let result2 = [];
      result.forEach((loop1) => {
        loop1.forEach((val) => {
          // console.log(val._id,"_____________________________6463");
          let obj = {

          };
          (obj["Id"] = val._id),
            (obj["City"] = val.City),
            (obj["DoctorName"] = val.name),
            (obj["Specialist"] =
              val.SpecialistId == null ? 0 : val.SpecialistId.name),
            (obj["SpecialistId"] =
              val.SpecialistId == null ? 0 : val.SpecialistId._id),
            result2.push(obj);
        });
      });
      APIResp.getSuccessResult(result2, "Success", res);
      // }
      // });
      // console.log(result)

      // doctorids.forEach(async (val)=>{
      //     let dataaa = await Users.find({ _id:val })
      // })

      // const doctorlist = await Users.find({}).populate([{
      //     path: "Roleid",
      //     model: "Roles",
      // }]).populate([{
      //     path: "SpecialistId",
      //     model: "Specialist",
      // }])
      // doctorlist.forEach(val => {
      //     console.log(val, "val")
      //     result = {}
      //     result["Id"] = val._id,
      //         result["DoctorName"] = val.Username,
      //         result["SpecialistId"] = val.SpecialistId._id,
      //         result["Specialist"] = val.SpecialistId.name,
      //         result["City"] = val.City
      //     obj.push(result)
      // })
      // res.status(200).json({ success: true, result: doctorlist })
      // APIResp.getSuccessResult(obj, "Success", res)
    } catch (err) {
      res.status(400).send({ error: `${err}` });
    }
  };

  const CurrentMonthAvailableDoctors = async (req, res) => {
    try {
      let userInput = helper.getReqValues(req);
      let result = {};
      console.log("moment().month()");
      var currentmonth = moment().month() + 2;
      var currentyear = moment().year();

      const a = await DoctorSchedule.aggregate([
        {
          $match: {
            schedulemonth: currentmonth,
            scheduleyear: currentyear,
          },
        },
      ]);

      // console.log("a",helper.convertarrtoObj(a)._doc.doctorscheduleinfo)
      let result12 = [];
      // console.log("a",a)

      a.forEach((val) => {
        // console.log(val,"val")
        let result2 = {};
        val.doctorscheduleinfo.forEach((val1) => {
          console.log("val1", val1);
          result2 = {};
          (result2["EventID"] = val1.EventID),
            (result2["ThemeColor"] = val1.ThemeColor),
            (result2["DoctorID"] = val1.DoctorID),
            (result2["Subject"] = val1.Subject),
            (result2["StartDate"] = val1.StartDate),
            (result2["EndDate"] = val1.EndDate),
            (result2["IsFullDay"] = val1.IsFullDay),
            (result2["Status"] = val1.Status);

          result12.push(result2);
        });
      });
      APIResp.getSuccessResult(result12, "Success", res);
    } catch (err) {
      APIResp.getErrorResult(err, res);
    }
  };
  // const CurrentMonthAvailableDoctors = async (req, res) => {
  //     let obj = []
  //     try {
  //         let result = {}
  //         const doctorlist = await Users.find({}).populate([{
  //             path: "Roleid",
  //             model: "Roles",
  //         }]).populate([{
  //             path: "SpecialistId",
  //             model: "Specialist",
  //         }])
  //         doctorlist.forEach(val => {
  //             console.log(val,"val")
  //             result = {}
  //             result["Id"] = val._id,
  //                 result["DoctorName"] = val.Username,
  //                 result["SpecialistId"] = val.SpecialistId._id,
  //                 result["Specialist"] = val.SpecialistId.name,
  //                 result["City"] = val.City
  //             obj.push(result)
  //         })
  //         // res.status(200).json({ success: true, result: doctorlist })
  //         APIResp.getSuccessResult(obj, "Success", res)
  //     }
  //     catch (err) {
  //         res.status(400).send({ error: `${err}` });
  //     }
  // }

  const createzoommeeting = async (req, res) => {
    try {
    } catch (err) {
      res.status(400).send({ error: `${err}` });
    }
  };

  const getDoctorProfile = async (req, res) => {
    try {
      let userInput = helper.getReqValues(req);
      console.log("userInput", userInput)
      let result = {};
      // let agg;

      const doctorprofilelist = await Users.find({
        _id: userInput.doctorid,
      })
      // .populate([
      //   {
      //     path: "Insuranceid",
      //     model: "Insurancepolicy",
      //   },
      // ]);
      // doctorprofilelist.map(async val => {
      //     if (val.Doctorrating) {
      //         agg = await Users.aggregate([
      //             {
      //                 '$match': {
      //                     _id: mongoose.Types.ObjectId(userInput.doctorid)
      //                 }
      //             }, {
      //                 '$project': {
      //                     'len': {
      //                         '$size': '$Doctorrating'
      //                     },
      //                     'sum': {
      //                         '$sum': '$Doctorrating.userdoctorrating'
      //                     }
      //                 }
      //             }
      //         ])

      //     }
      //     else
      //         return
      // })

      let ans = helper.convertarrtoObj(doctorprofilelist)._doc.Doctorrating != null
        ? helper.convertarrtoObj(doctorprofilelist)._doc.Doctorrating.reduce(
          (prev, curr) => prev + curr.userdoctorrating,
          0
        ) / helper.convertarrtoObj(doctorprofilelist)._doc.Doctorrating.length
        : 0;

      // helper.convertarrtoObj(doctorprofilelist)._docDoctorrating ?
      doctorprofilelist.forEach((val) => {
        result["Doctorname"] = val.name;
        result["Doctorcommands"] = val.Doctorrating;
        result["Doctorrating"] = Math.round(ans);
        result["Yearofexperience"] = val.DoctorProfile.Yearsofexperience;
        result["Doctorfees"] = val.DoctorProfile.Doctorfees;
        result["Qualification"] = val.DoctorProfile.Qualification;
        result["language"] = val.DoctorProfile.Languages;
        result["Aboutdoctor"] = val.DoctorProfile.About;
        result["Doctorcollage"] = val.DoctorProfile.MedicalSchool;
        result["Insurancename"] = val.DoctorProfile.Insurancename;
        result["ProfileImagelink"] = val.ProfileImagelink;
      });
      // : null
      APIResp.getSuccessResult(result, "Success", res);
      // res.status(200).json({ success: true, result: result })
    } catch (err) {
      console.log(err)
      res.status(400).send({ error: `${err}` });
    }
  };

  const getDoctors = async (req, res) => {
    try {
      let userInput = helper.getReqValues(req);
      let result = {};
      const doctorexists = await Users.findOne({
        Username: userInput.doctorName,
      });
      if (!doctorexists)
        return res.status(400).json({ error: "Doctor not found!" });

      const doctorrolelist = await Users.find({
        Username: userInput.doctorName,
      }).populate([
        {
          path: "Roleid",
          model: "Roles",
          // match: { _id: doctorexists.Roleid },
          select: "RoleName -_id",
        },
      ]);
      console.log(doctorrolelist);
      // if (helper.convertarrtoObj(doctorrolelist)._doc.Roleid.RoleName === 'Admin') return res.status(400).json({ error: 'Doctor is not present in doctor role' })

      const doctorspeclist = await Users.find({
        Username: userInput.doctorName,
      }).populate([
        {
          path: "SpecialistId",
          model: "Specialist",
          match: { name: userInput.specialist },
          // select: 'name -_id'
        },
      ]);

      if (!helper.convertarrtoObj(doctorspeclist)._doc.SpecialistId)
        return res.status(400).json({ error: "Specialist not found!" });

      doctorspeclist.forEach((val) => {
        (result["ID"] = val._id),
          (result["DoctorName"] = val.Username),
          (result["Specialist"] = val.SpecialistId.name),
          (result["SpecialistId"] = val.SpecialistId._id),
          (result["City"] = val.City);
      });

      APIResp.getSuccessResult([result], "Success", res);
    } catch (err) {
      res.status(400).send({ error: `${err}` });
    }
  };

  const htmlToPdfUpload = async (req, res) => {
    try {
      
      // const updateit = await Users.updateOne(
      //     {
      //         _id: userInput.id
      //     },
      //     {
      //         $set: {
      //             "ProfileImagelink": req.files,
      //         }
      //     })
      
      const blob = fs.readFileSync(req.files[0].path)
      

      const uploadedImage = await s3.upload({
        Bucket: process.env.AWS_S3_BUCKET_NAME,
        Key: req.files[0].originalname,
        Body: blob,
      }).promise()
      console.log(uploadedImage,"uploadedImage")
      console.log("req.files[0].path", (req.files[0].path).toString().replace('public', '').replace('api', '').substring(2));
      res.status(200).json({ Result: `${(req.files[0].path).toString().replace('public', '').replace('api', '').substring(2)}` });
    } catch (err) {
      console.log(err)
      res.status(400).send({ error: `${err}` });
    }
  };

  const getAvailableDoctorlist = async (req, res) => {
    try {
      let userInput = helper.getReqValues(req);
      console.log(userInput, req.params, req.body.doctorpractitionerid, "________________________________________")
      console.log(userInput.doctorpractitionerid, "doctorpractitionerid")
      console.log(userInput.appointmentlocation, "appointmentlocation")
      console.log(userInput.appointmentdate, "appointmentdate")
      //let result = [];
      let final = [];



      let a = await DoctorSchedule.aggregate([
        {
          $unwind: {
            path: "$doctorscheduleinfo",
          },
        },
        {
          $match: {
            "doctorscheduleinfo.StartDate": moment(userInput.appointmentdate).format('YYYY-MM-DD'),
            "doctorscheduleinfo.Status": "Available",
          },
        },
        {
          $group: {
            _id: {
              scheduleDate: "$scheduleDate",
              schedulemonth: "$schedulemonth",
              scheduleyear: "$scheduleyear",
            },
            doctorscheduleinfo: {
              $push: "$doctorscheduleinfo",
            },
          },
        },
      ]);
      let d = helper.convertarrtoObj(a);

      var docid = []


      d.doctorscheduleinfo.forEach(async (val, index) => {
        docid.push(val.DoctorID)
        // console.log(val.DoctorID,val.Subject)
      })
      console.log(docid)


      let result = await Users.find({
        _id: { $in: docid },
        SpecialistId: userInput.doctorpractitionerid,
        City: userInput.appointmentlocation,
        Doctorapproved: true,
      })
      console.log("result", result)

      if (result.length != 0) {

        let d1 = helper.convertarrtoObj(result);
        // console.log(d1,"____________________________________allnew")

        // 
        result.forEach((val, index) => {

          var sum = 0;
          var len = 0;
          len = val._doc.Doctorrating != null ? val._doc.Doctorrating.length : 0;
          val._doc.Doctorrating != null &&
            val._doc.Doctorrating.forEach((val1, i) => {
              sum += val1.userdoctorrating;
            });
          console.log(sum, len, "----")

          let obj = {};
          var rating = 0
          if (sum == 0 && len == 0) { rating = 0 }
          else { rating = sum / len }
          obj["Doctorrating"] = Math.round(rating),
            helper.convertarrtoObj(a).doctorscheduleinfo.forEach((m) => {
              obj["Doctoravaliabledate"] = m.StartDate;
              // console.log(d1._doc.name, m.Subject)
              if (
                val._doc.name === m.Subject ||
                val._doc._id === m.DoctorID ||
                val._doc.SpecialistId === userInput.doctorpractitionerid
              ) {
                // console.log(m)

                // console.log(d1._doc.name)
                obj["Doctorname"] = val._doc.name;
                obj["DoctorID"] = val._doc._id;
                obj["Profileimg"] = val._doc.ProfileImagelink;
                obj["Doctorfees"] = val._doc.DoctorProfile.Doctorfees == "" ? "0" : val._doc.DoctorProfile.Doctorfees;
                obj["Insuranceid"] = val._doc.DoctorProfile.Insuranceid;
                obj["Insurancename"] = val._doc.DoctorProfile.Insurancename;
              }

            })
          // console.log(obj)

          final.push(obj);

          // }

          if (index + 1 == result.length) {
            // console.log("result",result.length,final.length)   
            return APIResp.getSuccessResult(final, "Success", res);
          }
          // })

        })
      }
      else {
        APIResp.getSuccessResult([], "failed", res);
      }


      // console.log("result",result.length,final.length)
      //         APIResp.getSuccessResult(final, "Success", res);

      // d.doctorscheduleinfo.forEach(async (val, index) => {
      //   result = [];
      //   console.log("val.DoctorID", val.DoctorID);

      //   result = await Users.find({
      //     _id: val.DoctorID,
      //     SpecialistId: userInput.doctorpractitionerid,
      //     City: userInput.appointmentlocation,
      //     Doctorapproved: "true",
      //   })
      //   .populate([
      //     {
      //       path: "Insuranceid",
      //       model: "Insurancepolicy",
      //     },
      //   ]);
      //   console.log("resultalllllllllll", result);

      //   if (result.length !== 0) {
      //     console.log("21");
      //     let d1 = helper.convertarrtoObj(result);
      //     var sum = 0;
      //     var len = 0;
      //     len = d1._doc.Doctorrating ? d1._doc.Doctorrating.length : 0;
      //     // console.log(d1._doc)
      //     d1._doc.Doctorrating &&
      //       d1._doc.Doctorrating.forEach((val1,i) => {
      //         sum += val1.Rating;
      //       });

      //     let obj = {};
      //     (obj["Doctorrating"] = sum / len),
      //       (obj["Doctoravaliabledate"] = val.StartDate);

      //     if (
      //       d1._doc.Username === val.Subject ||
      //       d1._doc._id === val.DoctorID ||
      //       d1._doc.SpecialistId === userInput.doctorpractitionerid
      //     ) {
      //       obj["Doctorname"] = d1._doc.Username;
      //       obj["DoctorID"] = d1._doc._id;
      //       obj["Doctorspecialist"] = d1._doc.SpecialistId;
      //       obj["Profileimg"] = d1._docProfileImagelink;
      //       obj["Doctorfees"] = d1._docDoctorfees;
      //       obj["Subject"] = d1._docAbout;
      //       obj["SpecialistId"] = d1._docSpecialistId;
      //       obj["Insuranceid"] = d1._docInsuranceid_id;
      //       obj["Insurancename"] = d1._docInsuranceidInsurancename;
      //     }
      //     final.push(obj);
      //     // console.log("final", final);


      //   }

      //   if(d.doctorscheduleinfo.length==index+1){
      //   APIResp.getSuccessResult(final, "Success", res);
      //   }
      // });
    } catch (err) {
      console.log(err)
      APIResp.getErrorResult(err, res);
    }
  };

  const getDoctorScheduleList = async (req, res) => {
    try {
      let userInput = helper.getReqValues(req);
      const doctorlist = await DoctorSchedule.find({
        schedulemonth: userInput.month,
        scheduleyear: userInput.year,
      });
      APIResp.getSuccessResult(
        helper.convertarrtoObj(doctorlist)._doc.doctorscheduleinfo,
        "Success",
        res
      );
    } catch (err) {
      APIResp.getErrorResult(err, res);
    }
  };

  const ViewPatientReport = async (req, res) => {
    try {
      let userInput = helper.getReqValues(req);

      const ViewPatientReport = await Patientvisited.aggregate([
        {
          $match: {
            patientId: userInput.patientId,
            appointmentId: userInput.appointmentId,
          },
        },
      ]);
      console.log("userInput.patientId", userInput.patientId);
      console.log("userInput.appointmentId", userInput.appointmentId);
      res.status(200).json({ success: true, result: ViewPatientReport });
    } catch (err) {
      res.status(400).send({ error: `${err}` });
    }
  };

  const getAvailableDoctorSpecialist = async (req, res) => {
    try {
      const doctorlist = await DoctorSchedule.find({
        schedulemonth: 9,
        scheduleyear: 2021,
      }).populate([
        {
          path: "doctorscheduleinfo.DoctorID",
          model: "Users",

          select: "SpecialistId",
          populate: {
            path: "SpecialistId",
            model: "Specialist",
          },
        },
      ]);
      // console.log(result.scheduleyear)
      res.status(200).json({ success: true, result: doctorlist });
      // console.log(result.scheduleyear)
    } catch (err) {
      APIResp.getErrorResult(err, res);
    }
  };

  const getAvailableDoctorbyDateAvaliabledate = async (req, res) => {
    try {
      let userInput = helper.getReqValues(req);
      console.log(userInput.Avaliabledate, "userInput___________________________")
      let result = {};
      // const y = moment().format("YYYY-MM-DD")
      // var date = new Date(userInput.Avaliabledate);
      var date = moment(userInput.Avaliabledate, 'DD/MM/YYYY').format('YYYY-MM-DD')
      // let res = userInput.Avaliabledate.replace('/', "-");

      console.log("date", date)

      const a = await DoctorSchedule.aggregate([
        {
          $match: {
            "schedulemonth": moment().month() + 1,
            "scheduleyear": moment().year(),
          },
        },
        {
          $unwind: {
            path: "$doctorscheduleinfo",
          },
        },
        {
          $match: {
            "doctorscheduleinfo.DoctorID": parseInt(userInput.Doctorid),
            // "doctorscheduleinfo.StartDate": moment(userInput.Avaliabledate).format("YYYY-MM-DD"),

            "doctorscheduleinfo.StartDate": date

          },
        },
      ]);
      console.log("a", a)
      // console.log("a",helper.convertarrtoObj(a)._doc.doctorscheduleinfo)
      let result12 = [];
      a.forEach((val) => {
        console.log(val);
        (result["EventID"] = val.doctorscheduleinfo.EventID),
          (result["ThemeColor"] = val.doctorscheduleinfo.ThemeColor),
          (result["DoctorID"] = val.doctorscheduleinfo.DoctorID),
          (result["Subject"] = val.doctorscheduleinfo.Subject),
          (result["StartDate"] = val.doctorscheduleinfo.StartDate),
          (result["EndDate"] = val.doctorscheduleinfo.EndDate),
          (result["IsFullDay"] = val.doctorscheduleinfo.IsFullDay),
          (result["Status"] = val.doctorscheduleinfo.Status);
        result12.push(result);
      });
      APIResp.getSuccessResult(result12, "Success", res);
    } catch (err) {
      console.log(err, "err")
      APIResp.getErrorResult(err, res);
    }
  };

  const getUserdetailsByIdAndEmail = async (req, res) => {
    try {
      let userInput = helper.getReqValues(req);

      const userdetail = await Users.aggregate([
        {
          $match: {
            _id: mongoose.Types.ObjectId(userInput.id),
            Emailid: userInput.Emailid,
          },
        },
        {
          $project: {
            username: 1,
            role: 1,
            Emailid: 1,
            MoblieNo: 1,
          },
        },
      ]);
      res.status(200).json({ success: true, result: userdetail });
    } catch (err) {
      res.status(400).send({ error: `${err}` });
    }
  };
  const getUserdetailsByIdAndMobile = async (req, res) => {
    try {
      let userInput = helper.getReqValues(req);

      const userdetailMobile = await Users.aggregate([
        {
          $match: {
            _id: mongoose.Types.ObjectId(userInput.id),
            MoblieNo: userInput.MoblieNo,
          },
        },
        {
          $project: {
            username: 1,
            role: 1,
            Emailid: 1,
            MoblieNo: 1,
          },
        },
      ]);
      res.status(200).json({ success: true, result: userdetailMobile });
    } catch (err) {
      res.status(400).send({ error: `${err}` });
    }
  };
  const getUserdetailsByIdAndUsername = async (req, res) => {
    try {
      let userInput = helper.getReqValues(req);

      const userdetailMobile = await Users.aggregate([
        {
          $match: {
            _id: mongoose.Types.ObjectId(userInput.id),
            Username: userInput.username,
          },
        },
        {
          $project: {
            username: 1,
            role: 1,
            Emailid: 1,
            MoblieNo: 1,
          },
        },
      ]);
      res.status(200).json({ success: true, result: userdetailMobile });
    } catch (err) {
      res.status(400).send({ error: `${err}` });
    }
  };

  const ViewVisitedDoctorlistBy = async (req, res) => {
    // console.log("doctorlist")
    try {
      let userInput = helper.getReqValues(req);
      console.log(userInput.Patientid, "userInputuserInputuserInputuserInputuserInputuserInput")

      const doctorlist = await Patientvisited.find({
        patientId: userInput.Patientid,
      });
      // console.log("doctorlistaaaaaaaaaaaaa", doctorlist);
      let doctorlistall = [];
      doctorlist.forEach((val) => {
        // console.log(val, "valalllllllllllllllllllllllllll");
        doctorlistall.push(val.doctorId);
      });
      let uniqueChars = [...new Set(doctorlistall)];
      let arr = []
      console.log(uniqueChars, "uniqueChars----")
      uniqueChars.forEach(val => {
        arr.push({ doctorId: parseInt(val) })
      })
      APIResp.getSuccessResult(arr, "Success", res);
    } catch (err) {
      res.status(400).send({ error: `${err}` });
    }
  };
  const AddDoctorSchedule = async (req, res) => {
    try {
      let userInput = helper.getReqValues(req);
      let result1 = [];
      //    console.log(req.body,"-------")
      var currentmonth = moment().month() + 1;
      var currentyear = moment().year();

      const a = await DoctorSchedule.aggregate([
        {
          $match: {
            schedulemonth: currentmonth,
            scheduleyear: currentyear,
          },
        },
      ]);

      var scheduleDate = moment().format("MM/DD/YYYY");
      // console.log("scheduleDate", scheduleDate)
      if (a.length == 0) {
        let input = req.body
        input.forEach(async (val, i1) => {


          var eve = 1;

          var start = moment(new Date(val.StartDate)).format("DD");
          var end = moment(new Date(val.EndDate)).format("DD");
          //   var starttime = moment(new Date(val.StartDate)).format("HH:mm:ss")
          //   var endtime = moment(new Date(val.EndDate)).format("HH:mm:ss")
          for (var i = start; i <= end; i++) {

            let obj = {};
            obj["EventID"] = eve;
            obj["ThemeColor"] = val.ThemeColor;
            obj["DoctorID"] = val.DoctorID;
            obj["Subject"] = val.Subject;
            obj["StartDate"] = moment(`${currentyear}/${currentmonth}/${/^\d$/.test(i) ? '0' + i : i}`).format("YYYY-MM-DD");
            obj["EndDate"] = moment(`${currentyear}/${currentmonth}/${/^\d$/.test(i) ? '0' + i : i}`).format("YYYY-MM-DD");
            obj["IsFullDay"] = val.IsFullDay;
            obj["Status"] = val.Status;
            eve += 1;
            result1.push(obj);
          }

          if (i1 + 1 == input.length) {
            const user = new DoctorSchedule({
              scheduleDate: scheduleDate,
              schedulemonth: currentmonth,
              scheduleyear: currentyear,
              doctorscheduleinfo: result1
            });
            const savedUser = await user.save();
            return APIResp.getSuccessResult({ Result: true }, "sucess", res)
            // res.status(200).send(true);
          }

        });


      } else {
        //let b = req.body

        let input = req.body

        input.forEach(async (val3, i4) => {
          var start2 = moment(new Date(val3.StartDate)).format("YYYY-MM-DD");
          var end2 = moment(new Date(val3.EndDate)).format("YYYY-MM-DD");
          console.log(start2, end2)

          if (start2 == end2) {
            console.log('update -> if...')
            let d;
            d = await DoctorSchedule.updateMany(
              {
                'doctorscheduleinfo.StartDate': start2,
                'doctorscheduleinfo.EndDate': end2
              },
              {
                $set: {
                  'doctorscheduleinfo.$.DoctorID': val3.DoctorID,
                  'doctorscheduleinfo.$.ThemeColor': val3.ThemeColor,
                  'doctorscheduleinfo.$.StartDate': val3.StartDate,
                  'doctorscheduleinfo.$.EndDate': val3.EndDate,
                  'doctorscheduleinfo.$.Status': val3.Status,
                }
              }
            );
            // console.log(d)                

          }
          else {
            console.log('update -> else...')


            var len = await DoctorSchedule.find({})
            console.log(len.length, "len---")
            if (i4 == 0) {//normal insert
              await DoctorSchedule.deleteMany({
                schedulemonth: currentmonth,
                scheduleyear: currentyear
              });
              //  console.log(await DoctorSchedule.find({}),"-----")
              var eve = 1;

              var start = moment(new Date(val3.StartDate)).format("DD");
              var end = moment(new Date(val3.EndDate)).format("DD");
              // console.log(start,end)
              for (var i = start; i <= end; i++) {
                //   console.log(i)
                let obj = {};
                obj["EventID"] = eve;
                obj["ThemeColor"] = val3.ThemeColor;
                obj["DoctorID"] = val3.DoctorID;
                obj["Subject"] = val3.Subject;
                obj["StartDate"] = moment(`${currentyear}/${currentmonth}/${/^\d$/.test(i) ? '0' + i : i}`).format("YYYY-MM-DD");
                obj["EndDate"] = moment(`${currentyear}/${currentmonth}/${/^\d$/.test(i) ? '0' + i : i}`).format("YYYY-MM-DD");

                obj["IsFullDay"] = val3.IsFullDay;
                obj["Status"] = val3.Status;
                eve += 1;
                result1.push(obj);
              }

              const user = new DoctorSchedule({
                scheduleDate: scheduleDate,
                schedulemonth: currentmonth,
                scheduleyear: currentyear,
                doctorscheduleinfo: result1
              });
              const savedUser = await user.save();

            }
            else {

              var eve = 1;

              var start = moment(new Date(val3.StartDate)).format("DD");
              var end = moment(new Date(val3.EndDate)).format("DD");
              // console.log(start,end)
              for (var i = start; i <= end; i++) {
                //   console.log(i)
                let obj = {};
                obj["EventID"] = eve;
                obj["ThemeColor"] = val3.ThemeColor;
                obj["DoctorID"] = val3.DoctorID;
                obj["Subject"] = val3.Subject;
                obj["StartDate"] = moment(`${currentyear}/${currentmonth}/${/^\d$/.test(i) ? '0' + i : i}`).format("YYYY-MM-DD");
                obj["EndDate"] = moment(`${currentyear}/${currentmonth}/${/^\d$/.test(i) ? '0' + i : i}`).format("YYYY-MM-DD");

                obj["IsFullDay"] = val3.IsFullDay;
                obj["Status"] = val3.Status;
                eve += 1;
                result1.push(obj);
              }

              let d;
              d = await DoctorSchedule.updateMany(
                {
                  'schedulemonth': currentmonth,
                  'scheduleyear': currentyear
                },
                {
                  $push: {
                    'doctorscheduleinfo': result1,
                  }
                }
              );

            }

          }

          if (i4 + 1 == input.length) {
            return APIResp.getSuccessResult({ Result: true }, "sucess", res)
          }
        })




        // return res.status(200).send(true);

        // return APIResp.getSuccessResult(d, 'Success', res)
      }
    } catch (err) {
      console.log(err);
      APIResp.getErrorResult(err, res);
    }
  };
  const getAllDoctorRole = async (req, res) => {
    try {
      let userInput = helper.getReqValues(req);

      const userdetail = await Users.aggregate([
        {
          $match: {
            Roleid: userInput.role,
          },
        },
        {
          $project: {
            roleid: 1,
            username: 1,
            role: 1,
            Emailid: 1,
            MoblieNo: 1,
          },
        },
      ]);
      res.status(200).json({ success: true, result: userdetail });
    } catch (err) {
      res.status(400).send({ error: `${err}` });
    }
  };

  const gettoken = async (req, res) => {
    try {
      let a = await createJwt("Doctor");
      // res.send(a)
      res.status(200).json({ access_token: a, Error: null });
    } catch (err) {
      res.status(400).json({ Error: err });
    }
  };

  const GetDoctorPenddingApprovals = async (req, res) => {
    try {
      let userInput = helper.getReqValues(req)
      let result = await Users.find({
        $and: [
          {
            roleid: userInput.roleid,
            IsActive: 1,
            Doctorapproved: false
          }
        ]
      }).populate([{
        path: 'SpecialistId',
        model: 'Specialist'
      }])
      let all = []
      result.forEach((val, i) => {
        let obj = {}
        obj["sno"] = i + 1,
          obj["Id"] = val._id,
          obj["Name"] = val.name,
          obj["Username"] = val.usename,
          obj["Password"] = val.password,
          obj["EmailId"] = val.Emailid,
          obj["MobileNo"] = val.MobileNo,
          obj["Gender"] = val.Gender,
          obj["Address"] = val.Address,
          obj["City"] = val.City,
          obj["Country"] = val.Country,
          obj["Pincode"] = val.Pincode,
          obj["SelectedRoleId"] = val.roleid,
          obj["SelectedRole"] = "Doctor",
          obj["SelectedSpecialistId"] = val.SpecialistId._id,
          obj["SelectedSpecialist"] = val.SpecialistId.name
        all.push(obj)
      })
      APIResp.getSuccessResult(all, 'Success', res)
    }
    catch (err) {
      console.log(err)
      APIResp.getErrorResult(err, res)
    }
  }

  const UpdateDoctorApproval = async (req, res) => {
    try {
      let userInput = helper.getReqValues(req)
      console.log(userInput, "-----")
      const updateit = await Users.updateOne(
        {
          _id: userInput.Doctorid
        },
        {
          $set: {
            "Doctorapproved": true,
          }
        })
      APIResp.getSuccessResult({ Result: true }, 'success', res)
    } catch (err) {
      APIResp.getErrorResult(err, res)
    }
  }
  return {
    register,
    login,
    forgotpassword,
    getAllDoctorRoles,
    getDoctorsList,
    getDoctorProfile,
    getDoctors,
    htmlToPdfUpload,

    getAvailableDoctorlist,
    getDoctorScheduleList,
    getAvailableDoctorSpecialist,
    getAvailableDoctorbyDateAvaliabledate,
    GetAvailableDoctors,

    ViewVisitedDoctorlistBy,

    ViewPatientReport,
    CurrentMonthAvailableDoctors,
    AddDoctorSchedule,
    getUserdetailsByIdAndEmail,
    getUserdetailsByIdAndMobile,
    getUserdetailsByIdAndUsername,
    getAllDoctorRole,
    gettoken,
    GetDoctorPenddingApprovals,
    UpdateDoctorApproval
  };
};

module.exports = DoctorController();
