var express = require('express');
var router = express.Router();
const doctorRoute = require('./doctor')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('index');
});

router.use('/doctor', doctorRoute);

module.exports = router;
