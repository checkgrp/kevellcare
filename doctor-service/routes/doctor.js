const express = require('express');
const router = express.Router();
const DoctorController = require('../controllers/doctorController')
const {verifyToken} = require('../utils/verifyToken')

const {
  validateUserSignUpfields,
  // validateUserLoginfields,
  validateUserForgotPasswordFields,
  // validateUserResetPasswordFields
} = require('../validations/doctorValidation');
const upload = require('../utils/multer')
const auth = require("../utils/auth")


const { verifyDoctorAuthentication } = require('../utils/verifyToken');

router.post('/HtmlToPdfUpload',upload.array('file',1),DoctorController.htmlToPdfUpload);
router.post('/getAvailableDoctorlist',DoctorController.getAvailableDoctorlist);
router.get('/getDoctors',DoctorController.getDoctors);
router.get('/getDoctorScheduleList',verifyToken,DoctorController.getDoctorScheduleList);
//AddDoctorSchedule
//ViewVisitedDoctorlistBy
router.get('/ViewVisitedDoctorlistBy',DoctorController.ViewVisitedDoctorlistBy);
router.post('/AddDoctorSchedule',verifyToken,DoctorController.AddDoctorSchedule);//express.json({type: '/'})
router.get('/GetAvailableDoctorbyDate',verifyToken,DoctorController.getAvailableDoctorbyDateAvaliabledate);
//CurrentMonthAvailableDoctors
//GetAvailableDoctors
router.get('/CurrentMonthAvailableDoctors',DoctorController.CurrentMonthAvailableDoctors);
router.get('/GetAvailableDoctors',verifyToken,DoctorController.GetAvailableDoctors);
router.get('/getDoctorsList',DoctorController.getDoctorsList);
router.get('/getDoctorProfile',DoctorController.getDoctorProfile);

//
//GetDoctorPenddingApprovals
router.get('/GetDoctorPenddingApprovals',verifyToken,DoctorController.GetDoctorPenddingApprovals);

router.post('/UpdateDoctorApproval',verifyToken,DoctorController.UpdateDoctorApproval);


router.get("/gettoken",DoctorController.gettoken)



router.post('/register', validateUserSignUpfields, DoctorController.register);
router.post('/login', DoctorController.login);
router.post('/forgotpassword', validateUserForgotPasswordFields, DoctorController.forgotpassword);
router.get('/getAllDoctorRole',DoctorController.getAllDoctorRoles);
router.post('/getAvailableDoctorSpecialist',DoctorController.getAvailableDoctorSpecialist);
router.get('/getUserdetailsByIdAndEmail', DoctorController.getUserdetailsByIdAndEmail);
router.get('/getUserdetailsByIdAndMobile', DoctorController.getUserdetailsByIdAndMobile);
router.get('/getUserdetailsByIdAndUsername', DoctorController.getUserdetailsByIdAndUsername);
router.get('/getAllDoctororAdminRole',DoctorController.getAllDoctorRole);



module.exports = router;

