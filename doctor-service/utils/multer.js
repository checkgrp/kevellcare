const multer = require('multer');


var multerStorage  = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/api/UploadedFile/HtmlPdfs')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
})
//MULTER FILTER
const multerFilter = (req, file, cb) => {
// console.log(file)
//octet-stream
  if (file.mimetype.split("/")[1] === "pdf") {
    cb(null, true);
  } else {
    cb(new Error("Not a pdf File!!"), false);
  }
};

const upload = multer({
  storage: multerStorage,
  fileFilter: multerFilter,
});



module.exports = upload