var express = require('express');
var router = express.Router();
const adminRoute = require('./admin')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('index');
});

router.use('/admin', adminRoute);

module.exports = router;
