const express = require('express');
const router = express.Router();
const adminController = require('../controllers/adminController')
const {verifyToken} = require('../utils/verifyToken')




//GetDevicesSetting
router.post('/SaveSetting',verifyToken, adminController.saveSetting);
//GetDeviceDetails
router.get('/GetDeviceUnitValue', adminController.getDeviceUnitValue);
router.get('/GetAllKioskCountry', adminController.getAllKioskCountry);
router.get('/GetAllKioskStateBy', adminController.getAllKioskStateBy);
router.get('/GetAllKioskCityBy', adminController.getAllKioskCityBy);
router.get('/GetAllKioskLocationBy', adminController.getAllKioskLocationBy);
router.get('/GetAllKioskLocationAndKisokIdBy', adminController.getAllKioskLocationByKioskId);

//GetAllKioskLocationAndKisokIdBy
router.get('/GetPreExistingDisease', adminController.getPreExistingDisease);
router.get('/GetPractitioners', adminController.getPractitioners);
router.get('/GetDoctorLocations', adminController.getDoctorLocations);
router.get('/GetDevicesSetting', verifyToken,adminController.adminDevicesetting);




router.get('/GetDeviceDetails', adminController.adminDevicedetails);
router.get('/getAllRole',adminController.getAllRole);
router.get('/getAllInsurancepolicy', adminController.getAllInsurancepolicy);
router.get('/getdetailEmail',adminController.getdetailEmail);

router.get('/gettoken',adminController.gettoken);







module.exports = router;