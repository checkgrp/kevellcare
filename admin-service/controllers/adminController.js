const helper = require("../utils/helper");
const { createJwt, encrypt, decrypt } = require('../utils/common');
const { validationResult } = require('express-validator');
const sendEmail = require("../utils/sendEmail")

const mongoose = require('mongoose')
const {
    AdminSetting,
    KioskLocations,
    Preexistingdisease,
    Specialist,
    DoctorLocation,
    Insurancepolicy,
    Users,
    Roles
} = require('../models/kevellCareModels')
const crypto = require('crypto')
const moment = require('moment')
const data = require('../utils/admin.json')
const APIResp = require('../utils/APIResp');
const { result } = require("lodash");


const adminController = () => {

    const adminDevicedetails = async (req, res) => {
        try {
            const personall = await AdminSetting.find()

            APIResp.getSuccessResult(helper.convertarrtoObj(personall)._doc.Devicename, "Success", res)
        }
        catch (err) {
            console.log(err);
        }
    };
    const adminDevicesetting = async (req, res) => {
        try {
            const personall = await AdminSetting.find()
            // res.status(200).json({ data: personall })
            var obj={}
            // var result=[]
obj['ID']=personall[0]._id
obj['Deviceinfo']=personall[0].Devicename
// result.push(obj)
            APIResp.getSuccessResult(obj, "Success", res)
        }
        catch (err) {
            console.log(err);
        }
    };
    
    const getAllRole = async (req, res) => {
        try {
            const Role = await Roles.find()
            res.status(200).json({ data: Role })
        }
        catch (err) {
            console.log(err);
        }
    };

    const getDeviceUnitValue = async (req, res) => {
        try {
            res.status(200).json({ data })
        }
        catch (err) {
            console.log(err);
        }
    }
    const getAllKioskCountry = async (req, res) => {
        try {
            
            const kioskCountry = await KioskLocations.aggregate([
                {
                    '$group': {
                        '_id': '$Country'
                    }
                }
            ])
            let kioskCountryall=[]
            kioskCountry.forEach((val)=>{
              
                
                kioskCountryall.push(val._id)
            }
                
            )
            // res.status(200).json({ success: true, result: kioskCountryall })
            APIResp.getSuccessResult(kioskCountryall, "Success", res)
        }
        catch (err) {
            console.log(err);
        }
    }
    const getAllKioskStateBy = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)
            console.log(userInput,"userinput")

            const kioskState = await KioskLocations.aggregate(
                [
                    {
                        '$match': {
                            'Country': userInput.country
                        }
                    }, {
                        '$group': {
                            '_id': '$State'
                        }
                    }
                ]
            )
            console.log("kioskState",kioskState)
            let kioskStateall=[]
            kioskState.forEach((val)=>{
                console.log(val,"val")
                kioskStateall.push(val._id)
            })
            // res.status(200).json({ success: true, result: kioskState })
            APIResp.getSuccessResult(kioskStateall, "Success", res)
        }
        catch (err) {
            console.log(err);
        }
    }
    const getAllKioskCityBy = async (req, res) => {
        try {

            let userInput = helper.getReqValues(req)

            const kioskState = await KioskLocations.aggregate(
                [
                    {
                        '$match': {
                            'Country': userInput.country,
                            'State': userInput.state
                        }
                    }, {
                        '$group': {
                            '_id': '$City'
                        }
                    }
                ]
            )
            console.log("kioskState",kioskState)
            let kioskStateall=[]
            kioskState.forEach((val)=>{
                console.log(val,"val")
                kioskStateall.push(val._id)
            })
            // res.status(200).json({ success: true, result: kioskState })
            APIResp.getSuccessResult(kioskStateall, "Success", res)

            // let userInput = helper.getReqValues(req)

            // const kioskCity = await KioskLocations.aggregate([
            //     {
            //         '$match': {
            //             'Country': userInput.country,
            //             'State': userInput.state
            //         }
            //     }, {
            //         '$project': {
            //             'City': 1
            //         }
            //     }
            // ])
            // let kioskCityall=[]
            // kioskCity.forEach((val)=>{
            //     console.log(val,"val")
            //     kioskCityall.push(val.City)
            // })
            // // res.status(200).json({ success: true, result: kioskState })
            // APIResp.getSuccessResult(kioskCityall, "Success", res)
        }
        catch (err) {
            console.log(err);
        }
    }

    const getAllKioskLocationBy = async (req, res) => {
        try {

            
            let userInput = helper.getReqValues(req)

            const kioskState = await KioskLocations.aggregate(
                [
                    {
                        '$match': {
                            'Country': userInput.country,
                        'State': userInput.state,
                        'City': userInput.city
                        }
                    }, {
                        '$group': {
                            '_id': '$Location'
                        }
                    }
                ]
            )
            console.log("kioskState",kioskState)
            let kioskStateall=[]
            kioskState.forEach((val)=>{
                console.log(val,"val")
                kioskStateall.push(val._id)
            })
            APIResp.getSuccessResult(kioskStateall, "Success", res)

            // res.status(200).json({ success: true, result: kioskState })
            // APIResp.getSuccessResult(k

            // let userInput = helper.getReqValues(req)

            // const kioskCity = await KioskLocations.aggregate([
            //     {
            //         '$match': {
            //             'Country': userInput.country,
            //             'State': userInput.state,
            //             'City': userInput.city
            //         },
                    
                    
            //     },
            //     {
            //         '$project': {
            //             "_id":0,
            //            "Location":1
                       
            //         }
            //     }
                
            // ])
            // let kioskCityall=[]
            // kioskCity.forEach((val)=>{
            //     // console.log(val,"val")
            //     kioskCityall.push(val.Location)
            // })
            // // res.status(200).json({ success: true, result: kioskCity })
        }
        catch (err) {
            console.log(err);
        }
    }
    const getAllKioskLocationByKioskId = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)

            const kioskCity = await KioskLocations.aggregate([
                {
                    '$match': {
                        'Country': userInput.country,
                        'State': userInput.state,
                        'City': userInput.city
                    },
                    
                    
                },
                {
                    '$project': {
                        "_id":0,
                       "Location":1,
                       "KioskId":1
                       
                    }
                }
                // {
                //     '$group': {
                //         '_id': '$KioskId'
                //     }
                // }
                
            ])
            //  let kioskCityall=[]
            // kioskCity.forEach((val)=>{
            //     // console.log(val,"val")
            //     kioskCityall.push(val._id)
            // })
            // // res.status(200).json({ success: true, result: kioskCity })
           
            // res.status(200).json({ success: true, result: kioskCity })
            APIResp.getSuccessResult(kioskCity, "Success", res)
        }
        catch (err) {
            console.log(err);
        }
    }
    const getPreExistingDisease = async (req, res) => {
        try {
            let obj =[];
            const diseses = await Preexistingdisease.find()
            // obj.push(diseses)
            // console.log(diseses)
            diseses.forEach((e)=>{
                console.log(e.id)
                let result ={}
                result["existingDiseaseid"]=e.id,
                result["existingdisease"]=e.existingdisease,
                obj.push(result)
                
            })
            APIResp.getSuccessResult(obj, "Success", res)
        }
        catch (err) {
            console.log(err);
        }
    }
    const getPractitioners = async (req, res) => {
        try {
            const specialist = await Specialist.find()
            // res.status(200).json({ data: specialist })
            let obj=[];
            specialist.forEach((e)=>{
               
                let result ={}
                result["DoctorSpecialistid"]=e._id,
                result["DoctorSpecialistvalue"]=e.name,
                obj.push(result)
                
                
            })

            APIResp.getSuccessResult(obj, "Success", res)
        }
        catch (err) {
            console.log(err);
        }
    }

    const getDoctorLocations = async (req, res) => {
        try {
            const doctorLocation = await DoctorLocation.find()
            // res.status(200).json({ data: doctorLocation })
            APIResp.getSuccessResult(doctorLocation, "Success", res)
        }
        catch (err) {
            console.log(err);
        }
    }
    
   
    
   
 
    
    const saveSetting = async (req, res) => {

        try {
            let userInput = helper.getReqValues(req)
// console.log(userInput,"---")
            if (userInput.ID==0) {
                const admin = new AdminSetting({
                    'Devicename':userInput.Deviceinfo
                })
                const savedAdmin = await admin.save()

                return res.status(200).json({
                    success: true,
                    message: "Success",
                    "responsecode": 200,
                    "data": {
                        "Result": "Create sucessfully"
                    }
                })
            }
            const adminid = await AdminSetting.findOne({ _id: userInput.ID })
            if(!adminid) return res.status(200).json({
                success: true,
                message: "Failed",
                "responsecode": 400,
                "data": {
                    "Result": "admin id not found"
                }
            })
            const updateit = await AdminSetting.updateOne(
            
                {
                    _id: userInput.ID
                },
                {
                    $set: {
                        'Devicename':userInput.Deviceinfo
                    }
                })
                return res.status(200).json({
                    success: true,
                    message: "Success",
                    "responsecode": 200,
                    "data": {
                        "Result": "Update sucessfully"
                    }
                })

        } catch (err) {
            res.status(400).send({ error: `${err}` });
        }
    }
   
    const getAllInsurancepolicy = async (req, res) => {
        try {
            const personall = await Insurancepolicy.find()
            res.status(200).json({ data: personall })
        }
        catch (err) {
            console.log(err);
        }
    };

    const  getdetailEmail = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)
    
            const userdetail = await Users.aggregate([
                {
                    '$match': {
                        "Emailid":userInput.Emailid,
                       
                    }
                }, {
                    '$project': {
                        'roleid':1,
                        'username': 1, 
                        'role': 1, 
                        'Emailid': 1, 
                        'MoblieNo': 1 
                       
                    }
                }
            ])
            res.status(200).json({ success: true, result: userdetail })
        }
        catch (err) {
            
            res.status(400).send({ error: `${err}` });
        }
    }

    const gettoken=async (req,res)=>{
        try{
let a=await createJwt('Admin')
// res.send(a)
res.status(200).json({ access_token: a, Error: null })
        }
        catch(err){
            res.status(400).json({Error:err})
        }
    }
    return {

        adminDevicesetting,
        adminDevicedetails,
        getDeviceUnitValue,
        getAllKioskCountry,
        getAllKioskStateBy,
        getAllKioskCityBy,
        getAllKioskLocationBy,
        getPreExistingDisease,
        getPractitioners,
        getDoctorLocations,
        saveSetting,
        getAllRole,
        getAllInsurancepolicy,
        getdetailEmail,
        getAllKioskLocationByKioskId,
        gettoken
        // fileUpload
        // login,
        // forgotpassword,
        // reset
    }
}



module.exports = adminController()