const express = require('express');
const router = express.Router();
const reportController = require('../controllers/reportController')
const upload = require('../utils/multer')
const {verifyToken} = require('../utils/verifyToken')

// const {
//   validateUserSignUpfields,
//   // validateUserLoginfields,
//   validateUserForgotPasswordFields,
//   // validateUserResetPasswordFields
// } = require('../validations/doctorValidation');
// const upload = require('../utils/multer')
// const auth = require("../utils/auth")


// const { verifyDoctorAuthentication } = require('./verifyDoctorAuthentication');


// router.get('/todayWaitingHallPatientsdetails',verifyToken,reportController.todayWaitingHallPatientsdetails);
router.get('/todayWaitingHallPatientsdetails',reportController.todayWaitingHallPatientsdetails);
// router.get('/todayVisitedPatientsdetails',verifyToken,reportController.visistedpatient)
router.get('/todayVisitedPatientsdetails',reportController.visistedpatient)


router.get('/getPatientRegistrationByEmail',reportController.getPatientRegistrationByEmail);
router.get('/getPatientRegistrationByid', reportController.getPatientRegistrationByid);
router.get('/GetPatientRegistrationBy', reportController.getPatientRegistrationBy);
router.get('/getallPatientRegistration', reportController.getAllPatientRegistrationByid);
router.get('/VerifyEmailBy', reportController.verifyEmailID);
router.get('/VerifyCellnumber', reportController.verifycellnumber);
router.post('/SaveAndUpdatepatientVisitedData',reportController.storingdata);

router.get('/GetPatientinfoBy',reportController.GetPatientinfoBy);


//Fileupload
//SaveAndUpdatepatientVisitedData
router.get('/GetVisitedPatients',verifyToken,reportController.getVisitedPatients);
router.get('/viewVisitedDoctorlistBy', reportController.viewVisitedDoctorlistBy);
//GetDoctorMappingPatientDetails
router.post("/GetDoctorMappingPatientDetails",verifyToken,reportController.mappingkit)
router.put('/updateDoctorRatingAndCommendByAppointment', reportController.updateDoctorRatingAndCommendByAppointment);
router.post('/SaveAndUpdatePatintRegistration', reportController.SaveAndUpdatePatintRegistration); 
router.put('/UpdatePatientRegistration', reportController.SaveAndUpdatePatintRegistration); 
router.post('/Fileupload',upload.array('file'), reportController.Fileupload); 

router.get("/gettoken",reportController.gettoken)


router.post("/AWScreatePhoneNumber",reportController.AWScreatePhoneNumber)
router.post("/AWSverifyPhoneNumber",reportController.AWSverifyPhoneNumber)
router.post("/AWSSubscription",reportController.AWSSubscription)
router.post("/AWSmessage",reportController.AWSmessage)

module.exports = router;
