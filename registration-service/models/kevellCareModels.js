const mongoose = require('mongoose')
const autoIncrement = require("mongoose-auto-increment");


const PatientvisitedSchema = new mongoose.Schema({
    visiteddate: {type: String},
    patientId: {type: String},
    doctorId: {type: Number},
    patientinfo: {type: JSON},
    temperatureinfo: {type: JSON},
    // doctorId:{type: String},
    bpinfo: {type: JSON},
    hwinfo: {type: JSON},
    spO2info: {type: JSON},
    bodyfatinfo: {type: JSON},
    ecginfo: {type: JSON},
    stestoscopeinfo: {type: JSON},
    otoscopeinfo1: {type: JSON},
    otoscopeinfo2: {type: JSON},
    otoscopeinfo3: {type: JSON},
    otoscopeinfo4: {type: JSON},
    otoscopeinfo5: {type: JSON},
    otoscopeinfo6: {type: JSON},
    
    otoscopeinfo7: {type: JSON},
    otoscopeinfo8: {type: JSON},
    otoscopeinfo9: {type: JSON},
    otoscopeinfo10: {type: JSON},
    appointmentid: {type: Number},
    patientDescription: {type: JSON},
    visistedstarttime:{type: String},
    visisted_start_time:{type: String},
    visisted_end_time:{type: String},
    visistedendtime:{type: String},
    // glucometerinfo:{type: String},
    glucometerinfo:{type: JSON},
    emginfo:{type: JSON},
    gsrinfo:{type: JSON},
    doctorid: {type: Number},
    createdby:{type: String},
    modifiedby:{type: String}
},{ timestamps: true });

autoIncrement.initialize(mongoose.connection);
PatientvisitedSchema.plugin(autoIncrement.plugin, {
  model: "Patientvisited", // collection or table name in which you want to apply auto increment
  field: "_id", // field of model which you want to auto increment
  startAt: 1000, // start your auto increment value from 1
  incrementBy: 1, // incremented by 1
});

const PatientRegistrationSchema = new mongoose.Schema({
    patientid: {type: String},
    patientinfo: {type: JSON},
    documentinfo: {type: JSON},
    appointmentdate: {type: Date},
    kittype: {type: String},
    createdby:{type: String},
    modifiedby:{type: String},
    otp:{type:String},
    macid:{type:String},
    username:{type:String}

},{ timestamps: true });

autoIncrement.initialize(mongoose.connection);
PatientRegistrationSchema.plugin(autoIncrement.plugin, {
  model: "PatientRegistration", // collection or table name in which you want to apply auto increment
  field: "_id", // field of model which you want to auto increment
  startAt: 1000, // start your auto increment value from 1
  incrementBy: 1, // incremented by 1
});

const PatientKitMappingSchema = new mongoose.Schema({
    macid: {type: String},
    patientkitid: {type: String},
    patientid: {type: String},
    createdby:{type: String},
    modifiedby:{type: String}
},{ timestamps: true });
autoIncrement.initialize(mongoose.connection);
PatientKitMappingSchema.plugin(autoIncrement.plugin, {
  model: "PatientKitMapping", // collection or table name in which you want to apply auto increment
  field: "_id", // field of model which you want to auto increment
  startAt: 1000, // start your auto increment value from 1
  incrementBy: 1, // incremented by 1
});

const UsersSchema = new mongoose.Schema({
    Name: { type: String },
    Username: { type: String },
    Password: { type: JSON },
    ConfirmPassword: { type: JSON },
    Roleid: {type: String},
    //Role: { type: String },
    Emailid: { type: String },
    MoblieNo: { type: String },
    Gender: { type: String },
    City: { type: String },
    Country: { type: String },
    Address: { type: String },
    Pincode: { type: String },
    SpecialistId: {type: String},
    IsActive: { type: String },
    ProfileImagelink: { type: String },
    DoctorProfile: { type: String },
    Doctorrating: {type: JSON},
    Doctorapproved: {type: String},//true/false
    // SelectedRoleId: {type: String},
    // SelectedRole: { type: String },
    // SelectedSpecialistId: { type: String },
    Qualification: { type: String },
    MedicalSchool: { type: String },
    Yearsofexperience: { type: String },
    Languages: { type: String },
    Doctorfees: { type: String },
    ServerPath: { type: String },
    About: { type: String },
    Insuranceid: { type: String },
    Doctorcollege:{type: String},
    //Insurancename: { type: String },
    
    createdby: { type: String },
    modifiedby: { type: String }
}, { timestamps: true });

const Patientvisited= mongoose.model('Patientvisited', PatientvisitedSchema,'Patientvisited');
const PatientRegistration= mongoose.model('PatientRegistration', PatientRegistrationSchema,'PatientRegistration');
const PatientKitMapping= mongoose.model('PatientKitMapping', PatientKitMappingSchema,'PatientKitMapping');
const Users= mongoose.model('Users', UsersSchema,'Users');


module.exports = {
   
    Patientvisited,
    PatientRegistration,
    PatientKitMapping,
    Users

    
}