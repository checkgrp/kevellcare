const helper = require("../utils/helper");
const { createJwt, encrypt, decrypt } = require('../utils/common');
const { validationResult } = require('express-validator');
const axios = require('axios');

var AWS = require("aws-sdk");

const {
    Users,
    PatientRegistration,
    PatientKitMapping,

    Patientvisited

} = require('../models/kevellCareModels')
const moment = require('moment')
const mongoose = require('mongoose')
const _ = require('lodash');
const { forEach } = require("lodash");
const APIResp = require('../utils/APIResp')


const DoctorController = () => {

    const todayWaitingHallPatientsdetails = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)
            console.log(userInput)
            // console.log( moment().format("YYYY/MM/DD").toString())
            const todayPatient = await PatientRegistration.aggregate([
                {
                    '$unwind': {
                        'path': '$patientinfo.Appointmentinfo'
                    }
                }, {
                    '$match': {
                        'patientinfo.Appointmentinfo.isvisited': false,
                        'patientinfo.Appointmentinfo.doctornameid': parseInt(userInput.doctorId),
                        "patientinfo.Appointmentinfo.appointmentdate": moment().format("YYYY-MM-DD").toString()
                    }
                }
            ])
            console.log(todayPatient)
            let final=[]
            if (todayPatient.length > 0) {
                // res.status(200).json({ success: true, result: todayPatient })
                let result = {}
                todayPatient.forEach((val) => {

                    result = {}

                    result["Sno"] = val.patientinfo.Appointmentinfo.sno,
                        result["PatientID"] = val.patientid,
                        result["ID"] = val._id,
                        result["Patientname"] = val.patientinfo.LastName,
                        result["DOB"] = val.patientinfo.Dob,
                        result["Age"] = val.patientinfo.Age,
                        result["Email"] = val.patientinfo.EmailID,
                        result["Gender"] = val.patientinfo.Gender,
                        result["PhoneNumber"] = val.patientinfo.CellPhoneNo,
                        result["SocialSecurityNumber"] = val.patientinfo.SocialSecurityNo,
                        result["MaritalStatus"] = val.patientinfo.MaritalStatus,
                        result["Reason_Tomeetoctor"] = val.patientinfo.Appointmentinfo.Reasonformeetingdoctor,
                        result["Appoinmentinfoid"] = val.patientinfo.Appointmentinfo.sno,
                        result["Preexistingdisease"] = val.patientinfo.Appointmentinfo.preexistingdisease,
                        result["WaitingPatientinfos"] = null
final.push(result)
                })
                return APIResp.getSuccessResult(final, "Success", res)
            }
            else {
                return res.status(200).json({
                    success: true, responsecode: 201, message: "No data found", "data": []

                })
            }
        }
        catch (err) {
            res.status(400).json({ error: `${err}` });
        }
    }

    const SaveAndUpdatePatintRegistration = async (req, res) => {

        let userInput = helper.getReqValues(req)
        console.log(userInput.Id, "---", req.body, "req.body")
        // const emailExist = await PatientRegistration.findOne({ 'patientinfo.EmailID': req.body.EmailID })
        // if (emailExist) return res.status(201).json({ Result: 0 })

        // const MoblieNoExist = await PatientRegistration.findOne({ 'patientinfo.mobileno': userInput.mobile })
        // if (MoblieNoExist) return res.status(400).json({ error: 'MoblieNo already exists!' })

        // const AadharExist = await PatientRegistration.findOne({ 'patientinfo.aadhar': userInput.aadhar }) 
        // if (AadharExist) return res.status(400).json({ error: 'AadharNo already exists!' })
        let patientExist = null
        //!req.body.Id.match(/^[0-9a-fA-F]{24}$/)
        if (req.body.Id === 0) {
            console.log('0')
            docotrid = null
        }
        else {

            patientExist = await PatientRegistration.findOne({ _id: req.body.Id })
            if (!patientExist) return APIResp.getErrorResult('false', res)
        }


        let user=null
            if(req.body.UserName){
                user = req.body.UserName
            }


        if (patientExist) {
            console.log('Updated..')    
            // console.log(req.body.PatientId)
            console.log(req.body.Id, req.body.PatientId,"-----")
            const patientupdate = await PatientRegistration.updateOne(
                { _id: req.body.Id },
                {
                    $set: {
                        patientinfo: req.body.PatientInfo,
                        // filedetailjson: req.body.FileDetailjson,
                        documentinfo: JSON.parse(req.body.FileDetailjson),
                        kittype: req.body.KitType,
                        macid: req.body.MacId,
                        username:user 
                    }
                })

            // res.status(200).json({ patientupdate,msg: 'Updated it !' })
            return APIResp.getSuccessResult({ Result:req.body.Id}, "Success", res)
        }
        console.log('Inserted..')
        const patient = new PatientRegistration({
            patientinfo: req.body.PatientInfo,
            // filedetailjson: req.body.FileDetailjson,
            documentinfo: JSON.parse(req.body.FileDetailjson),
            kittype: req.body.KitType,
            macid: req.body.MacId,
           
        })
        try {
            const savedPatient = await patient.save()

            // const token = createJwt(user._id)
            // res.header('auth-token', token)
            // res.status(200).json({
            //     success: true,
            //     message: 'Registered successfully',
            //     registeredUserId: patient._id,
            //     // accessToken: token
            // })
            // console.log(,"===")
            await PatientRegistration.find({ _id: savedPatient._id }).updateOne((
                { _id: savedPatient._id },
                {
                    $set: {
                        patientid: `P${savedPatient._id}`
                    }
                }))
            let result = await PatientRegistration.find()

            APIResp.getSuccessResult({ Result: savedPatient._id }, "Success", res)
        } catch (err) {
            res.status(400).send({ error: `${err}` });
        }
    }

    // const visistedpatient = async (req, res) => {
    //     try {
    //         let userInput = helper.getReqValues(req)
    //         const todayvisitedPatient = await PatientRegistration.aggregate([
    //             {
    //                 '$unwind': {
    //                     'path': '$patientinfo.Appointmentinfo'
    //                 }
    //             }, {
    //                 '$match': {
    //                     'patientinfo.Appointmentinfo.isvisited': true,
    //                     'patientinfo.Appointmentinfo.doctornameid':  parseInt(userInput.doctorId),
    //                     "patientinfo.Appointmentinfo.appointmentdateforstring": moment().format("YYYY/MM/DD").toString()
    //                 }
    //             }
    //         ])
    //         console.log("todayvisitedPatient", todayvisitedPatient)
    //         let result = {}
    //         if (todayvisitedPatient.length > 0) {
    //             todayvisitedPatient.forEach((val) => {

    //                 result = {}

    //                 result["Sno"] = val.patientinfo.Appointmentinfo.sno,
    //                     result["PatientID"] = val.patientinfo.Appointmentinfo.patientId,
    //                     result["Patientname"] = val.patientinfo.LastName,
    //                     result["DOB"] = val.patientinfo.Dob,
    //                     result["Age"] = val.patientinfo.Age,
    //                     result["Email"] = val.patientinfo.EmailID,
    //                     result["Gender"] = val.patientinfo.Gender,
    //                     result["PhoneNumber"] = val.patientinfo.CellPhoneNo,
    //                     result["SocialSecurityNumber"] = val.patientinfo.SocialSecurityNo,
    //                     result["MaritalStatus"] = val.patientinfo.MaritalStatus,
    //                     result["Reason_Tomeetoctor"] = val.patientinfo.Appointmentinfo.preexistingdisease,
    //                     result["Appoinmentinfoid"] = val.patientinfo.Appointmentinfo.sno,
    //                     result["Preexistingdisease"] = val.patientinfo.Appointmentinfo.preexistingdisease,
    //                     result["WaitingPatientinfos"] = null




    //             })
    //             return APIResp.getSuccessResult([result], "Success", res)

    //         }
    //         else {
    //             return res.status(200).json({
    //                 success: true, responsecode: 201, message: "No data found", "data": []

    //             })
    //         }

    //     }
    //     catch (err) {
    //         res.status(400).json({ error: `${err}` });
    //     }
    // };
    const visistedpatient = async (req, res) => {
        
        try{
            let userInput = helper.getReqValues(req)
            var currdate=moment().format('YYYY-MM-DD').toString()
            let todayvisitedPatient = await Patientvisited.aggregate(
                [
                    {
                      '$match': {
                        'visiteddate': currdate
                      }
                    }
                    
                  ]
            )

            var resultNew=[]
            var resultunisue;
            
            todayvisitedPatient.forEach((val)=>{

                resultNew.push(val.patientId)
                resultunisue = [...new Set(resultNew)];
              
            })
            console.log(resultunisue,"resultNew")
            let todayvisitedPatientall = await PatientRegistration.aggregate(
                [
                    // {
                    //   '$match': {
                    //     'visiteddate': currdate
                    //   }
                    // }
                    
                    // {
                    //     '$match': {
                    //     _id:{$in:resultunisue}
                    // }
                    // }
                    { "$match": { 
                        "patientid": { "$in": resultunisue },
                        
                    } }, 
                    
                     {
                        '$unwind': {
                            'path': '$patientinfo.Appointmentinfo'
                        }
                      }
                      , {
                        '$match': {
                          'patientinfo.Appointmentinfo.isvisited': true, 
                          'patientinfo.Appointmentinfo.appointmentdate': currdate, 
                          'patientinfo.Appointmentinfo.doctornameid': parseInt(userInput.doctorId)
                        }
                      }
                    
                  ]
            )
            // console.log("todayvisitedPatientall",todayvisitedPatientall)
    
let result=[]
            todayvisitedPatientall.forEach((val)=>{
                let obj = {}
                console.log("val -----------",val)
                // if(userInput.doctorId == val.result.patientinfo.Appointmentinfo.doctornameid){
                   
                obj['PatientID'] = val.patientid
                obj["ID"] = val._id
                obj['Visiteddate'] = currdate
                obj['Patientname'] = val.patientinfo.LastName
                obj['DOB'] = val.patientinfo.Dob
                obj['Age'] = val.patientinfo.Age
                obj['Email'] = val.patientinfo.EmailID
                obj['Gender'] = val.patientinfo.Gender
                obj['MaritalStatus'] = val.patientinfo.MaritalStatus
                obj['PhoneNumber'] = val.patientinfo.CellPhoneNo
                obj['SocialSecurityNumber'] = val.patientinfo.SocialSecurityNo
                obj['Appoinmentinfoid'] = val.patientinfo.Appointmentinfo.sno
                obj['Reason_Tomeetoctor'] = val.patientinfo.Appointmentinfo.Reasonformeetingdoctor
                obj['Preexistingdisease'] = val.patientinfo.Appointmentinfo.preexistingdisease
                
result.push(obj)
            })
              APIResp.getSuccessResult(result,"Success",res)
        }
        catch (err) {
            console.log(err)
            res.status(400).json({ error: `${err}` });
        }
    };

    const getPatientRegistrationByEmail = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)
console.log(userInput)
            let patientemail = await PatientRegistration.findOne({ 'patientinfo.EmailID': userInput.emailid })
            if (!patientemail) return res.status(200).json({
                success: false, responsecode: 201, message: "No record found", "data": null

            })
            console.log(patientemail,"----")
            // res.status(200).json({ success: true, patientemail })
            // let result=[]
// patientemail.forEach((val)=>{
let obj={}
obj["Id"]= patientemail._id,
obj["PatientId"]= patientemail.patientid,
obj["PatientInfo"]= patientemail.patientinfo,
obj["FileDetailjson"]= null,
obj["FileDetailList"]= patientemail.documentinfo,
obj["KitType"]= patientemail.kittype,
obj["MacId"]= patientemail.macid,
obj["UserName"]= patientemail.username
// result.push(obj)
// })
            APIResp.getSuccessResult(obj, "Success", res)
        } 
        catch (err) {
            res.status(400).json({ error: `${err}` });
        }

    }


    const getPatientRegistrationByid = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)

            const patient = await PatientRegistration.findOne({ _id: userInput.id })
            if (!patient) return res.status(200).json({
                success: false, responsecode: 201, message: "No record found", "data": null

            })

            // res.status(200).json({ success: true, patient })
            APIResp.getSuccessResult(patient, "Success", res)
        }
        catch (err) {
            res.status(400).json({ error: `${err}` });
        }
    }
    const getPatientRegistrationBy = async (req, res) => {
//         try {
//             let userInput = helper.getReqValues(req)
// console.log(userInput)
// let patient=''
// let patientall=[]
//             if(userInput.id!=0){

//                   patient = await PatientRegistration.findOne({ _id: userInput.id })
//             }
//             patientall.push(patient)
           
//             console.log("patientall",patient._id)
//             let resultall = []
//             if(userInput.appointmentId!=null){
//                 const todayPatient = await PatientRegistration.aggregate([
//                     {
//                         '$unwind': {
//                             'path': '$patientinfo.Appointmentinfo'
//                         }
//                     }, {
//                         '$match': {
//                              "_id":patient._id, 
//                             'patientinfo.Appointmentinfo.sno': 1
//                         }
//                     }
//                 ])
//                 console.log("todayPatient",todayPatient[0].patientinfo.Appointmentinfo.Reasonformeetingdoctor)
                 
                
//                 patientall.forEach((val) => {

//                   let allnew=[]
//                   let allnew2=[]
//                   allnew.push(val.patientinfo)
                 
//                   allnew.filter(val=>{
//                     if(val.ReasonFormeetingDoctor==null){
//                         val.ReasonFormeetingDoctor=todayPatient[0].patientinfo.Appointmentinfo.Reasonformeetingdoctor,
//                         console.log(todayPatient[0].patientinfo.Appointmentinfo.Reasonformeetingdoctor)

//                     }
//                 //   allnew2.push([...allnew,...val.ReasonFormeetingDoctor])
//                   console.log(allnew2)
//                   })
                

//                 // let result = {}
              
//                 // result["Id"] = val._id
//                 // result["PatientId"] = val.patientid
//                 // // result["PatientInfo"] = todayPatient[0].patientinfo.Appointmentinfo.Reasonformeetingdoctor
//                 //     result["PatientInfo"] = val.patientinfo
//                 //     // {
//                 //     //     "RegDate":val.patientinfo.Appointmentinfo.RegDate
                       
//                 //     // } 

                    
//                 // result["FileDetailList"] = val.documentinfo
//                 // result["KitType "] = val.kittype
               



//                 //     // resultall.push(result)




//                 //   resultall.push(result) 
//                 // //   if(index+1==resultall.length){
//                 //     return APIResp.getSuccessResult(resultall, "Success", res)
                  
                

//             })
            
           
//         }
//         else{

//             patientall.forEach(val => {
//                 // console.log("val",val.patientinfo.Appointmentinfo.doctornameid)
//                 console.log("2")
//                 // console.log("val",val.patientinfo.Appointmentinfo.doctornameid)
//                 let result = {}
              
//                 result["Id"] = val._id
//                 result["PatientId"] = val.patientid
//                 result["PatientInfo"] = val.patientinfo
//                 result["FileDetailList"] = val.documentinfo
//                 result["KitType "] = val.kittype
               
//                 return APIResp.getSuccessResult(result, "Success", res)

//             })
//         }

            
          
 
         
//         }
try{
                let userInput = helper.getReqValues(req)
                let condn=[]
                // console.log( userInput.id,typeof userInput.patientId,typeof userInput.id)
console.log(userInput,"userInput---")
    
    if(userInput.id!='0' && userInput.patientId==null){
        condn=[
        {
            _id:parseInt(userInput.id),
        },
        {
            'patientinfo.Appointmentinfo.sno': parseInt(userInput.appoinmentinfoid),
        }]
    }
    if(userInput.id == '0' && userInput.patientId!=null){
        
        if(userInput.appoinmentinfoid !='0'){
            console.log("appointment present -----")
            condn=[
                {
                    patientid:userInput.patientId,
                },
                {
                    'patientinfo.Appointmentinfo.sno': parseInt(userInput.appoinmentinfoid),
                }]
        }
        else{
            console.log('appointment not present -----')
            condn=[
                {
                    patientid:userInput.patientId,
                }]
        }
        
    }
// }
// else{
//     console.log("appointment id is null")
//     return APIResp.getErrorResult("appointment id is null",res)
// }
    console.log(condn,"condn------")
                
                // console.log(data)
                var arr=[]
                if(userInput.appoinmentinfoid !='0'){
                    let data=await PatientRegistration.aggregate([
                        {
                          $unwind: {
                            path: "$patientinfo.Appointmentinfo",
                          },
                        },
                        {
                          $match: {
                            '$and':condn
                          
                          },
                        },
                      ]);
    
                    data[0].patientinfo.ReasonFormeetingDoctor = data[0].patientinfo.Appointmentinfo.Reasonformeetingdoctor
                    data[0]['Id'] = data[0]._id
                    data[0]['PatientId'] = data[0].patientid
                    data[0]['PatientInfo'] = data[0].patientinfo
                      arr.push(data[0].patientinfo.Appointmentinfo)
                    data[0].patientinfo.Appointmentinfo = arr
                    
                   
                    //   data[0]['MacId'] = data[0].macid
  
  data[0]['FileDetailList'] = data[0].documentinfo
  data[0]['KitType'] = data[0].kittype
      delete data[0].__v
      delete data[0].createdAt
      delete data[0].updatedAt
      delete data[0].appointmentdate
      delete data[0]._id
      delete data[0].patientid,
      delete data[0].macid
      delete data[0].kittype
      delete data[0].documentinfo
      delete data[0].patientinfo
      delete data[0].username
                    return APIResp.getSuccessResult(helper.convertarrtoObj(data),"Success",res)
                }  
                else{
                    let data=await PatientRegistration.aggregate([
                        // {
                        //   $unwind: {
                        //     path: "$patientinfo.Appointmentinfo",
                        //   },
                        // },
                        {
                          $match: {
                            '$and':condn
                          
                          },
                        },
                      ]);
    
                    data[0].patientinfo.ReasonFormeetingDoctor = data[0].patientinfo.Appointmentinfo.Reasonformeetingdoctor
                  data[0]['Id'] = data[0]._id
                  data[0]['PatientId'] = data[0].patientid
                  data[0]['PatientInfo'] = data[0].patientinfo
                //     arr.push(data[0].patientinfo.Appointmentinfo)
                  data[0].patientinfo.Appointmentinfo = arr
                  
                 
                  //   data[0]['MacId'] = data[0].macid

data[0]['FileDetailList'] = data[0].documentinfo
// data[0]['KitType'] = data[0].kittype
    delete data[0].__v
    delete data[0].createdAt
    delete data[0].updatedAt
    delete data[0].appointmentdate
    delete data[0]._id
    delete data[0].patientid,
    delete data[0].macid
    delete data[0].kittype
    delete data[0].documentinfo
    delete data[0].patientinfo
    delete data[0].username
                return APIResp.getSuccessResult(helper.convertarrtoObj(data),"Success",res)
                }
                
}
        catch (err) {
            console.log(err)
            res.status(400).json({ error: `${err}` });
        }
    }

    const getAllPatientRegistrationByid = async (req, res) => {
        try {
            const personall = await PatientRegistration.find()
            APIResp.getSuccessResult(personall, "Success", res)
        }
        catch (err) {
            console.log(err);
        }

    };
    const gettoken = async (req, res) => {
        try {
            let a = await createJwt('register')
            // res.send(a)
            res.status(200).json({ access_token: a, Error: null })
        }
        catch (err) {
            res.status(400).json({ Error: err })
        }
    }

    const verifyEmailID = async (req, res) => {

        try {
            let userInput = helper.getReqValues(req)
            
            console.log(userInput,"----")

            if(userInput.id == '0'){
            const todayPatient = await PatientRegistration.aggregate([

                {
                    '$unwind': {
                        'path': '$patientinfo'
                    }
                }, {
                    '$match': {
                        // "_id":userInput.id,
                        "patientinfo.EmailID": userInput.email
                    }
                }
            ])

            if(todayPatient.length==0){              
                return res.status(200).json({
                            success: false, responsecode: 201, message: "No data found", data: "null"
        
                        })
            }
            else{
                let result = helper.convertarrtoObj(todayPatient)
                APIResp.getSuccessResult(result.patientinfo, "Success", res)
            }
        }else{
            const todayPatient = await PatientRegistration.find({_id:userInput.id})
            const data = helper.convertarrtoObj(todayPatient)
            
            if(data._doc.patientinfo.EmailID==userInput.email){
                //normal update
                return res.status(200).json({
                    success: false, responsecode: 200, message: "No data found", data: "null"

                })
            }
            else{

                let todayPatient1 = await PatientRegistration.aggregate([

                    {
                        '$unwind': {
                            'path': '$patientinfo'
                        }
                    }, {
                        '$match': {
                            "patientinfo.EmailID": userInput.email
                        }
                    }
                ])

                if(todayPatient1.length!==0){
                    //can't able to update email id, email already exixts
                    return APIResp.getSuccessResult(todayPatient1, "Success", res)
                 
                }
                else{
                  //normal update
                return res.status(200).json({
                    success: false, responsecode: 200, message: "No data found", data: "null"

                })  
                }

            }
        }
        }
        catch (err) {
            console.log(err,"----")
            res.status(400).json({ error: `${err}` });
        }



    }
    const verifycellnumber = async (req, res) => {

        try {
            let userInput = helper.getReqValues(req)
        
        if(userInput.id=='0'){
            const todayPatient = await PatientRegistration.aggregate([

                {
                    '$unwind': {
                        'path': '$patientinfo'
                    }
                }, {
                    '$match': {
                        // "_id":mongoose.Types.ObjectId(userInput.id),
                        "patientinfo.CellPhoneNo": userInput.cellnumber
                    }
                }
            ])
        
            if(todayPatient.length==0){
                return res.status(200).json({
                            success: true,
                            responsecode: 200,
                            message: "No data found",
                            data: "null"
                        })
            }
            else{
                let result = helper.convertarrtoObj(todayPatient)
                    APIResp.getSuccessResult(result.patientinfo, "Success", res)
               
            }

        }else{
            const todayPatient = await PatientRegistration.find({_id:userInput.id})
            const data = helper.convertarrtoObj(todayPatient)
            
            if(data._doc.patientinfo.CellPhoneNo == userInput.cellnumber){
                //normal update
                return res.status(200).json({
                    success: false, responsecode: 200, message: "No data found", data: "null"

                })
            }
            else{

                let todayPatient1 = await PatientRegistration.aggregate([

                    {
                        '$unwind': {
                            'path': '$patientinfo'
                        }
                    }, {
                        '$match': {
                            "patientinfo.CellPhoneNo": userInput.cellnumber
                        }
                    }
                ])

                if(todayPatient1.length!==0){
                    //can't able to update email id, email already exixts
                    return APIResp.getSuccessResult(todayPatient1, "Success", res)
                 
                }
                else{
                  //normal update
                return res.status(200).json({
                    success: false, responsecode: 200, message: "No data found", data: "null"

                })  
                }

            }
        }
        }
        catch (err) {
            res.status(400)
                .json({
                    error: `${err}`
                });
        }
    }

    const getVisitedPatients = async (req, res) => {
        try {
            
            
            const visitedpatienttoday = await PatientRegistration.aggregate([
                {
                    '$unwind': {
                        'path': '$patientinfo.Appointmentinfo'
                    }
                }, {
                    '$match': {
                        'patientinfo.Appointmentinfo.appointmentdateforstring': moment().format("YYYY-MM-DD").toString(),
                        'patientinfo.Appointmentinfo.isvisited': false
                    }
                }
                
            ])
            console.log("visitedpatienttoday",visitedpatienttoday)
            // res.status(200).json({ success: true, message: "today visited patients list", response: visitedpatienttoday })
            if (visitedpatienttoday.length > 0) {
                let resultall = []
                visitedpatienttoday.forEach(val => {
                    // console.log("val",val.patientinfo.Appointmentinfo.doctornameid)
                    console.log(val,"val")
                    let result = {}
    
                    result["Id"] = val._id,
                    result["PatientId"] = val.patientid,
                     
                    result["PatientName"] = val.patientinfo.LastName,
                    result["Age"] = val.patientinfo.Age,
                    result["Gender"] = val.patientinfo.Gender,
                    result["MobileNo"] = val.patientinfo.CellPhoneNo,
                    result["ReasonForMeeting"] = val.patientinfo.ReasonFormeetingDoctor,
                    result["Specialist"] = val.patientinfo.Appointmentinfo.doctorpractitionername,
                    result["AppointmentStatus"] = "Booked",
                    result["AppointmentDate"] =val.patientinfo.Appointmentinfo.appointmentdate,
                     result["DoctorName"] = val.patientinfo.Appointmentinfo.doctorname,
                    result["AppointmentId"] = val.patientinfo.Appointmentinfo.sno,
    
    
                        resultall.push(result)
    
    
    
    
                    //   resultall.push(result) 
    
    
                })
                // res.status(200).json({ success: true, result: visitedpatienttoday })
                APIResp.getSuccessResult(resultall, "Success", res)
            }
            else {
                return res.status(200).json({
                    success: true, responsecode: 201, message: "No data found", "data": []

                })
            }
        }
        catch (err) {
            res.status(400).json({ error: `${err}` });
        }

    }

    const viewVisitedDoctorlistBy = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)

            const visitedpatienttoday = await PatientRegistration.aggregate([
                {
                    '$match': {
                        '_id': mongoose.Types.ObjectId(userInput.Patientid)
                    }
                }, {
                    '$unwind': {
                        'path': '$patientinfo.Appointmentinfo'
                    }
                }, {
                    '$project': {
                        'patientinfo.Appointmentinfo.doctornameid': 1
                    }
                }
            ])

            // visitedpatienttoday
            let resultall = []
            visitedpatienttoday.forEach(val => {
                // console.log("val",val.patientinfo.Appointmentinfo.doctornameid)
                let result = {}

                result["doctorid"] = val.patientinfo.Appointmentinfo.doctornameid,


                    resultall.push(result)




                //   resultall.push(result) 


            })
            // res.status(200).json({ success: true, message: "patient booked doctor list", response: visitedpatienttoday })
            APIResp.getSuccessResult(resultall, "Success", res)
        }
        catch (err) {
            res.status(400).json({ error: `${err}` });
        }


    }

    const updateDoctorRatingAndCommendByAppointment = async (req, res) => {
        try {
            let msg = ''
            let data = {}
            let userInput = helper.getReqValues(req)

            const patient = await PatientRegistration.findOne({ _id: userInput.Id })
            if (!patient) return APIResp.getErrorResult('patient not found', res)


            const doctor = await Users.findOne({ _id: userInput.DoctorId })
            if (!doctor) return APIResp.getErrorResult("doctor not found", res)

            const app = await PatientRegistration.aggregate([
                {
                    '$unwind': {
                        'path': '$patientinfo.Appointmentinfo'
                    }
                }, {
                    '$match': {
                        'patientinfo.Appointmentinfo.sno': userInput.AppointmentId
                    }
                }
            ])

            if (app.length == 0) return APIResp.getErrorResult("appointment not found", res)

            app.forEach(val => {
                // userInput.appointmentlocation = val.patientinfo.Appointmentinfo.appointmentlocation
                data.sno = val.patientinfo.Appointmentinfo.sno
                data.patientId = userInput.Id
                data.userid = userInput.DoctorId
                data.appointmentdate = val.patientinfo.Appointmentinfo.appointmentdateforstring
                data.userdoctorcommand = userInput.Comments
                data.userdoctorrating = userInput.Rating
                data.patientage = val.patientinfo.Age
                data.patientgender = val.patientinfo.Gender

            })

            const updateit = await PatientRegistration.updateOne(
                {
                    _id: userInput.Id,
                    'patientinfo.Appointmentinfo.patientId': userInput.Id,
                    'patientinfo.Appointmentinfo.doctornameid': userInput.DoctorId,
                    'patientinfo.Appointmentinfo.sno': userInput.AppointmentId,
                },
                {
                    $set: {
                        'patientinfo.Appointmentinfo.$.Rating': userInput.Rating,
                        'patientinfo.Appointmentinfo.$.Comments': userInput.Comments
                    }
                })

            if (updateit.modifiedCount === 0) return APIResp.getErrorResult("Invalid appointment!", res)

            const docrate = await Users.aggregate([
                {
                    '$unwind': {
                        'path': '$Doctorrating'
                    }
                }, {
                    '$match': {
                        'Doctorrating.sno': userInput.AppointmentId
                    }
                }
            ])
            if (docrate.length === 0) {
                //push
                var doctorrating = await Users.findOneAndUpdate({
                    _id: userInput.DoctorId
                },
                    {
                        $push: { "Doctorrating": data }
                    })
                msg = 'Inserted'
            }
            else {
                var doctorrating = await Users.updateOne(
                    {
                        _id: userInput.DoctorId,
                        'Doctorrating.Id': userInput.Id,
                        'Doctorrating.DoctorId': userInput.DoctorId,
                        'Doctorrating.AppointmentId': userInput.AppointmentId
                    },
                    {
                        $set: {
                            'Doctorrating.$.Rating': userInput.Rating,
                            'Doctorrating.$.Comments': userInput.Comments
                        }
                    })
                msg = 'Updated'
            }
            APIResp.getSuccessResult({ Result: true }, "Success", res)
            // res.status(200).json({ success: true, message: `Rating and Comments -> Updated in patient, ${msg} in doctor!!`, response: { updateit, doctorrating } })

        }
        catch (err) {
            // res.status(400).json({ error: `${err}` });
            APIResp.getErrorResult(err, res)
        }
    }
    
    // const fileUpload = async (req, res) => {
    //     try {

    //         try {
    //             // await PatientRegistration.findOneAndUpdate({
    //             //     _id: "62d6457cb0fe6e3acc06cf33"
    //             // },
    //             //     {
    //             //         $push: { "documentinfo": [...req.files] }
    //             //     })
    //             res.status(200).json({
    //                 status: "success",
    //                 message: "File Uploaded successfully!!",
    //                 response: req.files
    //             });

    //         } catch (err) {
    //             res.status(400).send({ error: `${err}` });
    //         }
    //     } catch (err) {
    //         res.status(500).json({ error: `${err}` });
    //     }
    // }
    const mappingkit = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)
            // console.log(userInput,"-----")
            let all=[]
            for(var i=0;i<Object.keys(userInput).length;i++){
                
                var doctorlist = await PatientKitMapping.aggregate([
                    {
                      '$match': {
                        'patientid': userInput[i].patientid, 
                        'patientkitid': userInput[i].patientkitid
                      }
                    }, {
                      '$lookup': {
                        'from': 'PatientRegistration', 
                        'localField': 'patientid', 
                        'foreignField': 'patientid', 
                        'as': 'result'
                      }
                    }
                  ])

                all.push(helper.convertarrtoObj(doctorlist))
                  }
                  let data=[]
all.forEach(val=>{
      var result = {}

                  result["Patientname"] = val.result[0].patientinfo.FirstName,
                  result["patientkitid"] = val.macid,
                  result["patientid"] = val.patientid,
                  result["moblienumber"] = val.result[0].patientinfo.CellPhoneNo,
                  result["aadharnumber"] = val.result[0].patientinfo.AadharNo
data.push(result)
})
                



            //   console.log(doctorlist,"----")
            // let a = []
            // a.push(doctorlist)
            // // console.log(a,"aaaa")
            // let result = {}
            // a.forEach(val => {
            //     console.log(val, "-=-=-=-")
            //     result = {}

            //     result["Patientname"] = val.patientid.patientinfo.FirstName,
            //         result["patientkitid"] = val.patientkitid,
            //         result["patientid"] = val.patientid._id,
            //         result["moblienumber"] = val.patientid.patientinfo.CellPhoneNo,
            //         result["aadharnumber"] = val.patientid.patientinfo.AadharNo

            // })
             APIResp.getSuccessResult(data, "Success", res)
        }
        catch (err) {
            console.log(err)
            res.status(400).json({ error: `${err}` });
        }

    }
    const storingdata = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)
            // console.log(userInput,"--------")
            const { id, deviceid, patientId, visitedid, imageno, jsondata, doctorId, appointmentId, visistedstarttime, visistedendtime } = req.body

            // if (deviceid == "") return res.status(400).json({ error: 'deviceid not found!' })
            // if (patientId == null) return res.status(400).json({ error: 'patientId not found!' })
            // if (doctorId == null) return res.status(400).json({ error: 'doctorId not found!' })
            // if (appointmentId == null) return res.status(400).json({ error: 'appointmentId not found!' })
            // if (imageno > 10) return res.status(400).json({ error: 'please correct imageno!' })



            const condn = await PatientRegistration.aggregate(

                [{
                    $unwind: {
                        path: '$patientinfo.Appointmentinfo'
                    }
                }, {
                    $match: {
                        'patientinfo.Appointmentinfo.doctornameid': doctorId,
                        'patientinfo.Appointmentinfo.sno': appointmentId
                    }
                }]
            )



 console.log(condn,"----")



            if (condn.length !== 0) {
                console.log("deviceid",deviceid)
                console.log("id",id)
                console.log("visitedid",visitedid)

                if (deviceid == "0" && id == 0 && visitedid == 0) {
                    // console.log("deviceid", deviceid)
                    // const patientinfodata = await Patientvisited.save({  })
                    // res.json({ patientinfodata })
                    const user = new Patientvisited({
                        patientId: patientId,
                        doctorId: doctorId,
                        visistedstarttime: visistedstarttime,
                        visistedendtime: visistedendtime,
                        appointmentid: appointmentId,
                        patientinfo: jsondata,
                        visiteddate:moment().format("YYYY-MM-DD")
                    })
                    try {
                        const savedUser = await user.save()

                        // const token = createJwt(user._id)
                        // res.header('auth-token', token)
                        // console.log(savedUser,"savedUsersavedUsersavedUsersavedUser")
                        console.log("patientId1",patientId)
                        console.log("patientId2",savedUser.patientId)
                        console.log("patientId3",savedUser.appointmentid)
                        console.log("patientId4",appointmentId)
                         const personupdate = await PatientRegistration.updateOne({patientid:patientId,'patientinfo.Appointmentinfo.sno':appointmentId }, { $set: {'patientinfo.Appointmentinfo.$.isvisited': true } })
                         console.log(personupdate)
                        return APIResp.getSuccessResult({Result:user._id},"Registered successfully",res)
                        // return res.status(200).json({
                        //     success: true,
                        //     message: 'Registered successfully',
                        //     registeredUserId: user._id,
                        //     // accessToken: token
                        // })

                    } catch (err) {
                        res.status(400).send({ error: `${err}` });
                    }
                }

                if (deviceid == "5" && id !=0 && visitedid !=0) {

                    try {
                        console.log("temperatureinfo", deviceid)

                        await Patientvisited.findOneAndUpdate({
                            _id: id
                        },
                            {
                                $set: { "temperatureinfo": jsondata }
                            })
                            const personupdate = await Patientvisited.updateOne({patientId:patientId}, { $set: {visiteddate:moment().format("YYYY-MM-DD")} })
                            return APIResp.getSuccessResult({Result:id},"Registered successfully",res)
                        // res.status(200).json({
                        //     success: true,

                        // })

                    } catch (err) {
                        console.log(err,"errr")
                        res.status(400).send({ error: `${err}` });
                    }

                }
                if (deviceid == "102" && id !=0 && visitedid !=0) {

                    try {
                        console.log("deviceid", deviceid)

                        await Patientvisited.findOneAndUpdate({
                            _id: id
                        },
                            {
                                $set: { "patientDescription": jsondata }
                            })
                            const personupdate = await Patientvisited.updateOne({patientId:patientId}, { $set: {visiteddate:moment().format("YYYY-MM-DD")} })
                            return APIResp.getSuccessResult({Result:id},"Registered successfully",res)
                        // res.status(200).json({
                        //     success: true,

                        // })

                    } catch (err) {
                        res.status(400).send({ error: `${err}` });
                    }

                }
                if (deviceid == null && id !=0 && visitedid !=0) {

                    try {
                        console.log("deviceidallllll", deviceid)

                        await Patientvisited.findOneAndUpdate({
                            _id: id
                        },
                            {
                                $set: { "visistedendtime": visistedendtime }
                            })
                            const personupdate = await Patientvisited.updateOne({patientId:patientId}, { $set: {visiteddate:moment().format("YYYY-MM-DD")} })
                            return APIResp.getSuccessResult({Result:id},"Registered successfully",res)
                        // res.status(200).json({
                        //     success: true,

                        // })

                    } catch (err) {
                        res.status(400).send({ error: `${err}` });
                    }

                }
                if (deviceid == "7" && id != 0 && visitedid != 0) {
                    try {

                        await Patientvisited.findOneAndUpdate({
                            _id: id
                        },
                            {
                                $set: { "bpinfo": jsondata }
                            })
                            const personupdate = await Patientvisited.updateOne({patientId:patientId}, { $set: {visiteddate:moment().format("YYYY-MM-DD")} })
                            return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

                    } catch (err) {
                        res.status(400).send({ error: `${err}` });
                    }

                }
                if (deviceid == "8" && id !=0 && visitedid !=0)  {
                    try {
                        console.log("hwinfo", deviceid)

// var obj={}
// obj['type'] = JSON.parse(jsondata).data.type
// obj['HeightValue'] = JSON.parse(jsondata).data.HeightValue
// obj['WeightValue'] = JSON.parse(jsondata).data.WeightValue
// obj['BmiValue'] = JSON.parse(jsondata).data.BmiValue
                        await Patientvisited.findOneAndUpdate({
                            _id: id
                        },
                            {
                                $set: { "hwinfo": jsondata }
                            })
                            const personupdate = await Patientvisited.updateOne({patientId:patientId}, { $set: {visiteddate:moment().format("YYYY-MM-DD")} })
                            return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

                    } catch (err) {
                        res.status(400).send({ error: `${err}` });
                    }

                }
                if (deviceid == "100" && id != 0 && visitedid != 0) {
                    try {

                        await Patientvisited.findOneAndUpdate({
                            _id: id
                        },
                            {
                                $set: { "spO2info": jsondata }
                            })
                            const personupdate = await Patientvisited.updateOne({patientId:patientId}, { $set: {visiteddate:moment().format("YYYY-MM-DD")} })
                            return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

                    } catch (err) {
                        res.status(400).send({ error: `${err}` });
                    }

                }
                if (deviceid == "101" && id != 0 && visitedid != 0) {
                    try {

                        await Patientvisited.findOneAndUpdate({
                            _id: id
                        },
                            {
                                $set: { "bodyfatinfo": jsondata }
                            })
                            const personupdate = await Patientvisited.updateOne({patientId:patientId}, { $set: {visiteddate:moment().format("YYYY-MM-DD")} })
                            return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

                    } catch (err) {
                        res.status(400).send({ error: `${err}` });
                    }

                }
                if (deviceid == "6" && id != 0 && visitedid != 0) {
                    try {

                        await Patientvisited.findOneAndUpdate({
                            _id: id
                        },
                            {
                                $set: { "ecginfo": jsondata }
                            })
                            const personupdate = await Patientvisited.updateOne({patientId:patientId}, { $set: {visiteddate:moment().format("YYYY-MM-DD")} })
                            return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

                    } catch (err) {
                        res.status(400).send({ error: `${err}` });
                    }

                }
                if (deviceid == "10" && id != 0 && visitedid != 0) {
                    try {

                        await Patientvisited.findOneAndUpdate({
                            _id: id
                        },
                            {
                                $set: { "emginfo": jsondata }
                            })
                            const personupdate = await Patientvisited.updateOne({patientId:patientId}, { $set: {visiteddate:moment().format("YYYY-MM-DD")} })
                            return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

                    } catch (err) {
                        res.status(400).send({ error: `${err}` });
                    }

                }
                if (deviceid == "11" && id != 0 && visitedid != 0) {
                    try {

                        await Patientvisited.findOneAndUpdate({
                            _id: id
                        },
                            {
                                $set: { "gsrinfo": jsondata }
                            })
                            const personupdate = await Patientvisited.updateOne({patientId:patientId}, { $set: {visiteddate:moment().format("YYYY-MM-DD")} })
                            return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

                    } catch (err) {
                        res.status(400).send({ error: `${err}` });
                    }

                }
                
                if (deviceid == "1" && id != 0 && visitedid != 0) {
                    try {

                        await Patientvisited.findOneAndUpdate({
                            _id: id
                        },
                            {
                                $set: { "stestoscopeinfo": jsondata }
                            })
                            const personupdate = await Patientvisited.updateOne({patientId:patientId}, { $set: {visiteddate:moment().format("YYYY-MM-DD")} })
                            return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

                    } catch (err) {
                        res.status(400).send({ error: `${err}` });
                    }

                }
                if (deviceid == "700" && id != 0 && visitedid != 0) {
                    try {

                        await Patientvisited.findOneAndUpdate({
                            _id: id
                        },
                            {
                                $set: { "glucometerinfo": jsondata }
                            })
                            const personupdate = await Patientvisited.updateOne({patientId:patientId}, { $set: {visiteddate:moment().format("YYYY-MM-DD")} })
                            return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

                    } catch (err) {
                        res.status(400).send({ error: `${err}` });
                    }

                }
                if (deviceid == "4") {

                    try {
                      console.log("imageno1----",imageno)
                    //   var updData = { `otoscopeinfo${imageno}` : jsondata } 
                      var update = { $set : {} };
                          update.$set['otoscopeinfo' + imageno] = jsondata;
//{$set:{$set}}
                          console.log(update,"update-----")
                        await Patientvisited.findOneAndUpdate({
                            _id: id
                        },
                        update)
                        const personupdate = await Patientvisited.updateOne({patientId:patientId}, { $set: {visiteddate:moment().format("YYYY-MM-DD")} })
                            return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

                    } catch (err) {
                        console.log(err,"err")
                        res.status(400).send({ error: `${err}` });
                    }

                }
                // if (deviceid == "4" && imageno == 2) {
                //     try {

                //         await Patientvisited.findOneAndUpdate({
                //             _id: id
                //         },
                //             {
                //                 $set: { "otoscopeinfo2": jsondata }
                //             })
                //             return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

                //     } catch (err) {
                //         res.status(400).send({ error: `${err}` });
                //     }

                // }
                // if (deviceid == "4" && imageno == 3) {
                //     try {

                //         await Patientvisited.findOneAndUpdate({
                //             _id: id
                //         },
                //             {
                //                 $set: { "otoscopeinfo3": jsondata }
                //             })
                //             return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

                //     } catch (err) {
                //         res.status(400).send({ error: `${err}` });
                //     }

                // }
                // if (deviceid =="4" && imageno == 4) {
                //     try {

                //         await Patientvisited.findOneAndUpdate({
                //             _id: id
                //         },
                //             {
                //                 $set: { "otoscopeinfo4": jsondata }
                //             })
                //             return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

                //     } catch (err) {
                //         res.status(400).send({ error: `${err}` });
                //     }

                // }
                // if (deviceid == "4" && imageno == 5) {
                //     try {

                //         await Patientvisited.findOneAndUpdate({
                //             _id: id
                //         },
                //             {
                //                 $set: { "otoscopeinfo5": jsondata }
                //             })
                //             return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

                //     } catch (err) {
                //         res.status(400).send({ error: `${err}` });
                //     }

                // }
                // if (deviceid == "4" && imageno == 6) {
                //     try {

                //         await Patientvisited.findOneAndUpdate({
                //             _id: id
                //         },
                //             {
                //                 $set: { "otoscopeinfo6": jsondata }
                //             })
                //             return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

                //     } catch (err) {
                //         res.status(400).send({ error: `${err}` });
                //     }

                // }
                // if (deviceid == "4" && imageno == 7) {
                //     try {

                //         await Patientvisited.findOneAndUpdate({
                //             _id: id
                //         },
                //             {
                //                 $set: { "otoscopeinfo7": jsondata }
                //             })
                //             return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

                //     } catch (err) {
                //         res.status(400).send({ error: `${err}` });
                //     }

                // }

                // if (deviceid == "4" && imageno == 8) {
                //     try {

                //         await Patientvisited.findOneAndUpdate({
                //             _id: id
                //         },
                //             {
                //                 $set: { "otoscopeinfo8": jsondata }
                //             })
                //             return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

                //     } catch (err) {
                //         res.status(400).send({ error: `${err}` });
                //     }

                // }
                // if (deviceid == "4" && imageno == 9) {
                //     try {

                //         await Patientvisited.findOneAndUpdate({
                //             _id: id
                //         },
                //             {
                //                 $set: { "otoscopeinfo9": jsondata }
                //             })
                //             return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

                //     } catch (err) {
                //         res.status(400).send({ error: `${err}` });
                //     }

                // }
                // if (deviceid == "4" && imageno == 10) {
                //     try {

                //         await Patientvisited.findOneAndUpdate({
                //             _id: id
                //         },
                //             {
                //                 $set: { "otoscopeinfo10": jsondata }
                //             })
                //             return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

                //     } catch (err) {
                //         res.status(400).send({ error: `${err}` });
                //     }

                // }
            }
            else {
                return res.status(400).json({ error: 'some data is invalid!' })
            }

            // const user = new Patientvisited({

            //     visiteddate: userInput.visiteddate,
            //     patientId: userInput.patientId,
            //     visistedstarttime: userInput.visistedstarttime,
            //     visistedendtime: userInput.visistedendtime,
            //     visitedid: userInput.visitedid,
            //     appointmentId: userInput.appointmentId,
            //     deviceid: userInput.deviceid,
            //     temperatureinfo: userInput.temperatureinfo,
            //     appoinmentid: userInput.appoinmentid,
            //     patientinfo: userInput.patientinfo,
            //     doctorid: userInput.doctorid,
            //     otoscopeinfo1:userInput.otoscopeinfo1,
            //     otoscopeinfo2:userInput.otoscopeinfo2,
            // })
            // console.log(user, "user")
            // try {
            //     const savedUser = await user.save()
            //     // console.log(savedUser,"savedUser")

            //     // const token = createJwt(user._id)
            //     // res.header('auth-token', token)
            //     res.status(200).json({
            //         success: true,
            //         message: 'Registered successfully',

            //         // accessToken: token
            //     })

        } catch (err) {
            console.log(err,"err")
            res.status(400).send({ error: `${err}` });
        }

    }

    const Fileupload = (req, res) => {
        // console.log(req)
        try {
            // await PatientRegistration.findOneAndUpdate({
            //     _id: "62d6457cb0fe6e3acc06cf33"
            // },
            //     {
            //         $push: { "documentinfo": [...req.files] }
            //     })
            // res.status(200).json({
            //     status: "success",
            //     message: "File Uploaded successfully!!",
            //     response: req.files
            // });
            console.log(req.files)
            let result = []
            req.files.forEach((val, i) => {
                let obj = {}
                obj["Sno"] = i + 1,
                    obj["Orignalfilename"] = val.originalname,
                    obj["Storedfilename"] = val.filename,
                    obj["Phycicalpath"] = val.path,
                    obj["Filetype"] = 'pdf'
                result.push(obj)
            })
            // APIResp.getSuccessResult(result, "Success", res)
            res.status(200).send(result)

        } catch (err) {
            res.status(400).send({ error: `${err}` });
        }
    }

    const GetPatientinfoBy=async (req,res)=>{
        try{
let userInput = helper.getReqValues(req)
let input = await PatientRegistration.find({'patientinfo.EmailID':userInput.email})
let obj={}
// obj["Id"]=input[0]._id
// "patientinfo"]={
    obj["FirstName"]=input[0].patientinfo.FirstName,
 obj["LastName"]=input[0].patientinfo.LastName   
// }

APIResp.getSuccessResult(obj,"Success",res)        
}catch(err){
            APIResp.getErrorResult(err,res)
        }
    }



    const AWScreatePhoneNumber = (req,res)=>{
        let userInput = helper.getReqValues(req)
        try{
              AWS.config.update({
                  region: 'us-east-1',
                  credentials: {
                    accessKeyId: 'AKIAZIM2IOOFVISM64PO',
                    secretAccessKey: '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'
                  }
                });
              
              var params = {
                PhoneNumber: `+91${userInput.mobile}`,
                LanguageCode: 'en-US'
              };
              new AWS.SNS({apiVersion: 'latest'}).createSMSSandboxPhoneNumber(params, function(err, data) {
                if (err) console.log(err, err.stack,"error------"); // an error occurred
                else{
                    // console.log(data,"data==-----");           // successful response
                    APIResp.getSuccessResult(data,"Success",res)
                }   
              });
              
        }
        catch(err){
            // console.log(err)
            APIResp.getErrorResult(err,res)
        }
    }
    const AWSverifyPhoneNumber = (req,res)=>{
        let userInput = helper.getReqValues(req)

        try{
              AWS.config.update({
                  region: 'us-east-1',
                  credentials: {
                    accessKeyId: 'AKIAZIM2IOOFVISM64PO',
                    secretAccessKey: '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'
                  }
                }); 
              
                var params = {
                  OneTimePassword: userInput.otp,
                  PhoneNumber: `+91${userInput.mobile}`
                };
                new AWS.SNS({apiVersion: 'latest'}).verifySMSSandboxPhoneNumber(params, function(err, data) {
                  if (err) console.log(err, err.stack); // an error occurred
                  else{
                    // console.log(data,"data==-----");           // successful response
                    APIResp.getSuccessResult(data,"Success",res)
                }   
                });
              
        }
        catch(err){
            console.log(err)
            APIResp.getErrorResult(err,res)
        }
    }
    const AWSSubscription = (req,res)=>{
        let userInput = helper.getReqValues(req)

        try{
                AWS.config.update({
                  region: 'us-east-1',
                  credentials: {
                    accessKeyId: 'AKIAZIM2IOOFVISM64PO',
                    secretAccessKey: '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'
                  }
                });
                var params = {
                  Protocol: 'sms', /* required */
                  TopicArn: 'arn:aws:sns:us-east-1:636515545995:KevellCare', /* required */
                  Endpoint: `+91${userInput.mobile}`
                };
                new AWS.SNS({apiVersion: 'latest'}).subscribe(params, function(err, data) {
                  if (err) console.log(err, err.stack,"error----"); // an error occurred
                  else{
                    // console.log(data,"data==-----");           // successful response
                    APIResp.getSuccessResult(data,"Success",res)
                }   
                });
        }
        catch(err){
            console.log(err)
            APIResp.getErrorResult(err,res)
        }
    }
    const AWSmessage = (req,res)=>{
        let userInput = helper.getReqValues(req)

        try{
            
                AWS.config.update({
                  region: 'us-east-1',
                  // AWSAccessKeyId : 'AKIAZIM2IOOFVISM64PO',
                  // AWSSecretKey:'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',
                  // apiVersion: 'latest',
                  credentials: {
                    accessKeyId: 'AKIAZIM2IOOFVISM64PO',
                    secretAccessKey: '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'
                  }
                });
              
                // Create publish parameters
                var params = {
                  Message: 'You are successfully registered with KevellCare', /* required */
                  PhoneNumber: `+91${userInput.mobile}`
                };
                // Create promise and SNS service object
  var publishTextPromise = new AWS.SNS({apiVersion: 'latest'}).publish(params).promise();
  
  // Handle promise's fulfilled/rejected states
  publishTextPromise.then(
    function(data) {
    //   console.log("MessageID is " + data.MessageId);
      APIResp.getSuccessResult(data.MessageId,"Success",res)

    }).catch(
      function(err) {
      console.error(err, err.stack);
    });

        }
        catch(err){
            console.log(err)
            APIResp.getErrorResult(err,res)
        }
    }
    return {


        todayWaitingHallPatientsdetails,
        visistedpatient,
        getPatientRegistrationByEmail,
        getPatientRegistrationByid,
        getAllPatientRegistrationByid,
        verifyEmailID,
        verifycellnumber,
        getVisitedPatients,
        viewVisitedDoctorlistBy,
        updateDoctorRatingAndCommendByAppointment,
        getPatientRegistrationBy,
        mappingkit,
        storingdata,
        SaveAndUpdatePatintRegistration,
        Fileupload,
        gettoken,
        GetPatientinfoBy,
       
        AWScreatePhoneNumber,
        AWSverifyPhoneNumber,
        AWSSubscription,
        AWSmessage

    }
}

module.exports = DoctorController()

