/* eslint-disable no-console */
const express = require('express');
const http = require('http');
const DoctorRoutes = require('./routes/doctor');
const PatientRoutes = require('./routes/patient');
const AdminRoutes = require('./routes/admin');
const VideoConRoutes = require('./routes/videocon');
const ReportRoutes = require('./routes/report');
const InsuranceRoutes = require('./routes/insurance');
const PatientKitRoutes = require('./routes/patientkit');
const AppointmentRoutes = require('./routes/appointment');

class Server {
  constructor() {
    this.app = express();
    this.http = http.Server(this.app);
  }

  /* Including app Routes starts */
  includeRoutes() {
    new DoctorRoutes(this.app).doctorConfig(); //5001
    new PatientRoutes(this.app).patientConfig(); //5002
    new AdminRoutes(this.app).adminConfig();//5003
    new VideoConRoutes(this.app).videoconConfig();//5004
    new ReportRoutes(this.app).reportConfig();//5005
    new InsuranceRoutes(this.app).insuranceConfig();//5006
    new PatientKitRoutes(this.app).patientkitConfig();//5007
    new AppointmentRoutes(this.app).appointmentConfig();//5008
  }
  /* Including app Routes ends */

  startTheServer() {
    this.includeRoutes();

    const port = process.env.NODE_SERVER_POST || 8000;
    const host = process.env.NODE_SERVER_HOST || 'localhost';

    this.http.listen(port, host, () => {
      console.log(`started on ${port}`);
    });
  }
}

module.exports = new Server();