const httpProxy = require('express-http-proxy');

const appointmentServiceProxy = httpProxy('http://localhost:5008');
const { verifyDoctorAuthentication } = require('../middlewares/doctor')

// const { createProxyMiddleware } = require('http-proxy-middleware');

class Routes {
    constructor(app) {
        this.app = app;
    }
    appRoutes() {

        // this.app.use('/api/patient',createProxyMiddleware({
        //     target: 'http://localhost:5001',
        // }))

        this.app.post('/api/appointment/BookAppointment', (req, res) => {
            appointmentServiceProxy(req, res);
        });
        this.app.put('/api/appointment/changeAppointment', (req, res) => {
            appointmentServiceProxy(req, res);
        });

    }
    
    appointmentConfig() {
        this.appRoutes();
    }
}


module.exports = Routes;
