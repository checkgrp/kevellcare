const httpProxy = require('express-http-proxy');

const insuranceServiceProxy = httpProxy('http://localhost:5006');
const { verifyDoctorAuthentication } = require('../middlewares/doctor')

// const { createProxyMiddleware } = require('http-proxy-middleware');

class Routes {
    constructor(app) {
        this.app = app;
    }
    appRoutes() {

        // this.app.use('/api/patient',createProxyMiddleware({
        //     target: 'http://localhost:5001',
        // }))

        this.app.get('/api/insurance/getAllInsurancepolicy', (req, res) => {
            insuranceServiceProxy(req, res);
        });

    }
    
    insuranceConfig() {
        this.appRoutes();
    }
}


module.exports = Routes;
