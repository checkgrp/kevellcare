const httpProxy = require('express-http-proxy');

const reportServiceProxy = httpProxy('http://localhost:5005');
const { verifyDoctorAuthentication } = require('../middlewares/doctor')

// const { createProxyMiddleware } = require('http-proxy-middleware');

class Routes {
    constructor(app) {
        this.app = app;
    }
    appRoutes() {

        // this.app.use('/api/patient',createProxyMiddleware({
        //     target: 'http://localhost:5001',
        // }))


        this.app.post('/api/report/GetPatientPrescription', (req, res) => {
            reportServiceProxy(req, res);
        });

        this.app.get('/api/report/getviewreport', (req, res) => {
            reportServiceProxy(req, res);
        });

        this.app.post('/api/report/VisitedPatientReport', (req, res) => {
            reportServiceProxy(req, res);
        });

        this.app.post('/api/report/storingdata', (req, res) => {
            reportServiceProxy(req, res);
        });

        this.app.post('/api/report/VisitedPatientReportdata', (req, res) => {
            reportServiceProxy(req, res);
        });

        this.app.post('/api/report/ViewPatientReport', (req, res) => {
            reportServiceProxy(req, res);
        });
       
    }
    
    reportConfig() {
        this.appRoutes();
    }
}


module.exports = Routes;
