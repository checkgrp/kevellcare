const httpProxy = require('express-http-proxy');

const patientServiceProxy = httpProxy('http://localhost:5002');
const { verifyDoctorAuthentication } = require('../middlewares/doctor')

// const { createProxyMiddleware } = require('http-proxy-middleware');

class Routes {
    constructor(app) {
        this.app = app;
    }
    appRoutes() {

        // this.app.use('/api/patient',createProxyMiddleware({
        //     target: 'http://localhost:5001',
        // }))


        this.app.post('/api/patient/register', (req, res) => {
            patientServiceProxy(req, res);
        });

        this.app.post('/api/patient/sendotp', (req, res) => {
            patientServiceProxy(req, res);
        });

        this.app.post('/api/patient/verifyotp', (req, res) => {
            patientServiceProxy(req, res);
        });

        this.app.get('/api/patient/getPatientRegistrationByid', (req, res) => {
            patientServiceProxy(req, res);
        });

        this.app.get('/api/patient/viewVisitedDoctorlistBy', (req, res) => {
            patientServiceProxy(req, res);
        });

        this.app.get('/api/patient/getVisitedPatients', (req, res) => {
            patientServiceProxy(req, res);
        });

        this.app.get('/api/patient/getPatientRegistrationByEmail', (req, res) => {
            patientServiceProxy(req, res);
        });

        this.app.put('/api/patient/updateDoctorRatingAndCommendByAppointment', (req, res) => {
            patientServiceProxy(req, res);
        });
        this.app.get('/api/patient/getallPatientRegistration', (req, res) => {
            patientServiceProxy(req, res);
        });

        this.app.get('/api/patient/verifyEmailID', (req, res) => {
            patientServiceProxy(req, res);
        });
        this.app.get('/api/patient/cellnumber', (req, res) => {
            patientServiceProxy(req, res);
        });
    }
    
    patientConfig() {
        this.appRoutes();
    }
}


module.exports = Routes;
