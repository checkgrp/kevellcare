const httpProxy = require('express-http-proxy');

const adminServiceProxy = httpProxy('http://localhost:5003');
const { verifyDoctorAuthentication } = require('../middlewares/doctor')

// const { createProxyMiddleware } = require('http-proxy-middleware');

class Routes {
    constructor(app) {
        this.app = app;
    }
    appRoutes() {

        // this.app.use('/api/patient',createProxyMiddleware({
        //     target: 'http://localhost:5001',
        // }))


        this.app.get('/api/admin/adminDevice', (req, res) => {
            adminServiceProxy(req, res);
        });

        this.app.get('/api/admin/getDeviceUnitValue', (req, res) => {
            adminServiceProxy(req, res);
        });

        this.app.post('/api/admin/getAllKioskCountry', (req, res) => {
            adminServiceProxy(req, res);
        });

        this.app.get('/api/admin/getAllKioskStateBy', (req, res) => {
            adminServiceProxy(req, res);
        });

        this.app.get('/api/admin/getAllKioskCityBy', (req, res) => {
            adminServiceProxy(req, res);
        });

        this.app.get('/api/admin/getAllKioskLocationBy', (req, res) => {
            adminServiceProxy(req, res);
        });

        this.app.get('/api/admin/getPreExistingDisease', (req, res) => {
            adminServiceProxy(req, res);
        });

        this.app.put('/api/admin/getPractitioners', (req, res) => {
            adminServiceProxy(req, res);
        });
        this.app.get('/api/admin/getDoctorLocations', (req, res) => {
            adminServiceProxy(req, res);
        });

        this.app.put('/api/admin/saveSetting', (req, res) => {
            adminServiceProxy(req, res);
        });
        this.app.get('/api/admin/getRoles', (req, res) => {
            adminServiceProxy(req, res);
        });
        this.app.get('/api/admin/getAllRole', (req, res) => {
            adminServiceProxy(req, res);
        });
        this.app.get('/api/admin/getAllInsurancepolicy', (req, res) => {
            adminServiceProxy(req, res);
        });
        this.app.get('/api/admin/getdetailEmail', (req, res) => {
            adminServiceProxy(req, res);
        });
       
    }
    
    adminConfig() {
        this.appRoutes();
    }
}


module.exports = Routes;
