const httpProxy = require('express-http-proxy');

const doctorServiceProxy = httpProxy('http://localhost:5001');
const { verifyDoctorAuthentication } = require('../middlewares/doctor')

// const { createProxyMiddleware } = require('http-proxy-middleware');
const morgan = require("morgan");


class Routes {
    constructor(app) {
        this.app = app;
    }

    appRoutes() {
        this.app.use(morgan("dev"));

        // this.app.use('/api/doctor',createProxyMiddleware({
        //     target: 'http://localhost:5001',
        // }))
        
        this.app.post('/api/doctor/register', (req, res) => {
            doctorServiceProxy(req, res)
        });

        this.app.post('/api/doctor/login', (req, res) => {
            doctorServiceProxy(req, res)
        });

        this.app.post('/api/doctor/forgotpassword', (req, res) => {
            doctorServiceProxy(req, res)
        });
        this.app.get('/api/doctor/todayVisitedPatientsdetails', (req, res) => {
            doctorServiceProxy(req, res)
        });
        
        this.app.delete('/api/doctor/deleteUser', (req, res) => {
            doctorServiceProxy(req, res)
        });
        this.app.get('/api/doctor/todayWaitingHallPatientsdetails', (req, res) => {
            doctorServiceProxy(req, res)
        });

        this.app.put('/api/doctor/saveAndUpdateUser', (req, res) => {
            doctorServiceProxy(req, res)
        });

        this.app.get('/api/doctor/uploadProfileImage', (req, res) => {
            doctorServiceProxy(req, res)
        });
        
        this.app.get('/api/doctor/getDoctorsList',  (req, res) => {
            doctorServiceProxy(req, res)
        });

        this.app.get('/api/doctor/getAllDoctorRole', (req, res) => {
            doctorServiceProxy(req, res)
        });
       
        this.app.get('/api/doctor/getDoctorProfile',  (req, res) => {
            doctorServiceProxy(req, res)
        });

        this.app.get('/api/doctor/getDoctors',  (req, res) => {
            doctorServiceProxy(req, res)
        });

        this.app.post('/api/doctor/htmlToPdfUpload',  (req, res) => {
            doctorServiceProxy(req, res)
        });

        this.app.post('/api/doctor/getAvailableDoctorlist',  (req, res) => {
            doctorServiceProxy(req, res)
        });

        this.app.get('/api/doctor/getDoctorScheduleList',  (req, res) => {
            doctorServiceProxy(req, res)
        });

        this.app.post('/api/doctor/getAvailableDoctorSpecialist',  (req, res) => {
            doctorServiceProxy(req, res)
        });

        this.app.get('/api/doctor/getAvailableDoctorbyDateAvaliabledate',  (req, res) => {
            doctorServiceProxy(req, res)
        });

        this.app.get('/api/doctor/getUserdetailsById',  (req, res) => {
            doctorServiceProxy(req, res)
        });

        this.app.get('/api/doctor/getUserdetailsByIdAndEmail',  (req, res) => {
            doctorServiceProxy(req, res)
        });

        this.app.get('/api/doctor/getUserdetailsByIdAndMobile',  (req, res) => {
            doctorServiceProxy(req, res)
        });

        this.app.get('/api/doctor/getUserdetailsByIdAndUsername',  (req, res) => {
            doctorServiceProxy(req, res)
        });

        this.app.get('/api/doctor/getNamerole',  (req, res) => {
            doctorServiceProxy(req, res)
        });

        this.app.get('/api/doctor/getAllDoctororAdminRole',  (req, res) => {
            doctorServiceProxy(req, res)
        });
      
    }

    doctorConfig() {
        this.appRoutes();
    }
}


module.exports = Routes;
