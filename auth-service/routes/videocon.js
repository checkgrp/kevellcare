const httpProxy = require('express-http-proxy');

const videoconServiceProxy = httpProxy('http://localhost:5004');
const { verifyDoctorAuthentication } = require('../middlewares/doctor')

// const { createProxyMiddleware } = require('http-proxy-middleware');

class Routes {
    constructor(app) {
        this.app = app;
    }
    appRoutes() {

        // this.app.use('/api/patient',createProxyMiddleware({
        //     target: 'http://localhost:5001',
        // }))


        this.app.post('/api/zoom/GetDoctorMeetingbyIds', (req, res) => {
            videoconServiceProxy(req, res);
        });

        this.app.get('/api/zoom/GetDoctorsWithMeeting', (req, res) => {
            videoconServiceProxy(req, res);
        });

        this.app.post('/api/zoom/createDoctorMeetingbydoctor', (req, res) => {
            videoconServiceProxy(req, res);
        });

        this.app.get('/api/zoom/GetZoomMeetingInfo', (req, res) => {
            videoconServiceProxy(req, res);
        });

        this.app.post('/api/zoom/createzoommeeting', (req, res) => {
            videoconServiceProxy(req, res);
        });
       
    }
    
    videoconConfig() {
        this.appRoutes();
    }
}


module.exports = Routes;
