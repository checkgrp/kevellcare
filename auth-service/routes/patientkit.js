const httpProxy = require('express-http-proxy');

const patientkitServiceProxy = httpProxy('http://localhost:5007');
const { verifyDoctorAuthentication } = require('../middlewares/doctor')

// const { createProxyMiddleware } = require('http-proxy-middleware');

class Routes {
    constructor(app) {
        this.app = app;
    }
    appRoutes() {

        // this.app.use('/api/patient',createProxyMiddleware({
        //     target: 'http://localhost:5001',
        // }))


        this.app.get('/api/patientkit/kitmapping', (req, res) => {
            patientkitServiceProxy(req, res);
        });

        this.app.get('/api/patientkit/getPatientKitMappingBymacId', (req, res) => {
            patientkitServiceProxy(req, res);
        });

        this.app.post('/api/patientkit/getPatientKitMappingByPaitentid', (req, res) => {
            patientkitServiceProxy(req, res);
        });

        this.app.post('/api/patientkit/getallkit', (req, res) => {
            patientkitServiceProxy(req, res);
        });

        this.app.post('/api/patientkit/validatemacId', (req, res) => {
            patientkitServiceProxy(req, res);
        });

        this.app.post('/api/patientkit/getMeetingDoctorlistBypatientid', (req, res) => {
            patientkitServiceProxy(req, res);
        });

        this.app.put('/api/patientkit/updatePatientidmacid', (req, res) => {
            patientkitServiceProxy(req, res);
        });

        this.app.post('/api/patientkit/getMaxNumberForPatientKitId', (req, res) => {
            patientkitServiceProxy(req, res);
        });

        this.app.put('/api/patientkit/saveAndUpdatePatientkitMapping', (req, res) => {
            patientkitServiceProxy(req, res);
        });

    }
    
    patientkitConfig() {
        this.appRoutes();
    }
}


module.exports = Routes;
