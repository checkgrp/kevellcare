const mongoose = require('mongoose')
const autoIncrement = require("mongoose-auto-increment");


const InsurancepolicySchema = new mongoose.Schema({
    Insurancename: {type: String},
},{ timestamps: false });
autoIncrement.initialize(mongoose.connection);
InsurancepolicySchema.plugin(autoIncrement.plugin, {
    model: "Insurancepolicy", // collection or table name in which you want to apply auto increment
    field: "_id", // field of model which you want to auto increment
    startAt: 1000, // start your auto increment value from 1
    incrementBy: 1, // incremented by 1
});



const Insurancepolicy = mongoose.model('Insurancepolicy', InsurancepolicySchema,'Insurancepolicy');


module.exports = {
   
    Insurancepolicy,
   
}