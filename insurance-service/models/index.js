const mongoose = require("mongoose");

const { DB_HOST }=require('../utils/config');

mongoose.connect(DB_HOST,
 {
    useNewUrlParser: true,
    // useFindAndModify: false,
    retryWrites: true,
    w: "majority",
  }
);

const db = mongoose.connection;

db.once("open", function () {
  console.log("DB Connected successfully");
});
db.on("error", console.error.bind(console, "connection error: "));


