var express = require('express');
var router = express.Router();
const adminRoute = require('./users')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('index');
});

router.use('/insurance', adminRoute);

module.exports = router;
