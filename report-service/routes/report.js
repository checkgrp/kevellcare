const express = require('express');
const router = express.Router();
const reportController = require('../controllers/reportController')
const {verifyToken} = require('../utils/verifyToken')

// const {
//   validateUserSignUpfields,
//   // validateUserLoginfields,
//   validateUserForgotPasswordFields,
//   // validateUserResetPasswordFields
// } = require('../validations/doctorValidation');
// const upload = require('../utils/multer')
// const auth = require("../utils/auth")


// const { verifyDoctorAuthentication } = require('./verifyDoctorAuthentication');



router.get("/VisitedViewPatientReport",verifyToken,reportController.VisitedViewPatientReport)
router.post("/GetAllPatientRegistrations",reportController.GetAllPatientRegistrations)
router.post("/VisitedPatientReport",reportController.VisitedPatientReport)
//PatientReportList
router.post("/ViewPatientReport",reportController.ViewPatientReport)

router.get("/RegistrationDetailReportBy",reportController.RegistrationDetailReportBy)
router.get("/RegistrationDetailReportBy",reportController.RegistrationDetailReportBy)

router.post("/GetAppointmentData",reportController.GetAppointmentData)
// router.get("/GetPatientPrescription",reportController.GetPatientPrescription)


router.get('/PatientVisitedReportById',reportController.PatientVisitedReportById);

router.post("/GetPatientPrescription",verifyToken,reportController.getviewreport)
// router.post('/storingdata',verifyToken,reportController.storingdata);
router.post("/VisitedPatientReportdata",reportController.VisitedPatientReportdata)
router.get("/MedicalRecords",reportController.MedicalRecords)

router.get("/gettoken",reportController.gettoken)

module.exports = router;
