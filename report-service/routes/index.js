var express = require('express');
var router = express.Router();
const doctorRoute = require('./report')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('index');
});

router.use('/report', doctorRoute);

module.exports = router;

