const helper = require("../utils/helper");
const { createJwt, encrypt, decrypt } = require("../utils/common");
const { validationResult } = require("express-validator");

const axios = require("axios");

const {
  Users,
  PatientRegistration,

  Patientvisited,
} = require("../models/kevellCareModels");
const moment = require("moment");
const mongoose = require("mongoose");
const _ = require("lodash");
const { forEach } = require("lodash");
const APIResp = require("../utils/APIResp");

const DoctorController = () => {
  //  const ViewPatientReport = async (req, res) => {
  //     try {
  //         let userInput = helper.getReqValues(req)

  //         const ViewPatientReport = await Patientvisited.aggregate([
  //             {
  //                 '$match': {
  //                     patientId: userInput.patientId,
  //                     appointmentId: userInput.appointmentId
  //                 }
  //             }
  //         ])
  //        console.log("userInput.patientId",userInput.patientId)
  //        console.log("userInput.appointmentId",userInput.appointmentId)
  //         res.status(200).json({ success: true, result: ViewPatientReport })

  //     } catch (err) {
  //         res.status(400).send({ error: `${err}` });
  //     }
  // }
  const GetAllPatientRegistrations = async (req, res) => {
    try {
      // let userInput = helper.getReqValues(req)
      const doctorprofilelist = await PatientRegistration.find({});
      let obj = [];
      doctorprofilelist.forEach((val) => {
        console.log(val, "val");

        let result = {};
        (result["Id"] = val._id),
          // moment(testDate).format('MM/DD/YYYY')
          (result["RegDate"] = moment(val.createdAt).format("MM/DD/YYYY")),
          (result["PatientId"] = val.patientid),
          (result["PatientName"] = val.patientinfo.FirstName),
          (result["Age"] = val.patientinfo.Age),
          (result["AadharNo"] = val.patientinfo.AadharNo),
          (result["Gender"] = val.patientinfo.Gender),
          (result["MobileNo"] = val.patientinfo.CellPhoneNo),
          (result["Diagnosis"] = ""),
          obj.push(result);
      });

      // res.status(200).json({ success: true, result: obj })
      APIResp.getSuccessResult(obj, "Success", res);

      // res.status(200).json({ success: true, result: doctorprofilelist })
      // res.status(200).json({ success: true, result: a })
    } catch (err) {
      res.status(400).json({ error: `${err}` });
    }
  };

  // const GetPatientPrescription = async (req, res) => {
  //     try {
  //         let userInput = helper.getReqValues(req)
  //         const doctorprofilelist = await Patientvisited.find({ patientId: userInput.PatientId,
  //             appointmentId:userInput.AppointmentId
  //              })
  //         res.status(200).json({ success: true, result: doctorprofilelist })
  //         // res.status(200).json({ success: true, result: a })
  //     }
  //     catch (err) {
  //         res.status(400).json({ error: `${err}` });
  //     }
  // }
  const ViewPatientReport = async (req, res) => {
    try {
      let pid
      // , aaid;
      let userInput = helper.getReqValues(req);
      console.log(userInput)
      const doctorprofilelist = await Patientvisited.find({
        _id: parseInt(userInput.Id),
        appointmentid: parseInt(userInput.Appointmentid),
      });
      doctorprofilelist.length == 1 &&
        doctorprofilelist.forEach((val) => {
          pid = val.patientId
          // aaid = val.appointmentId;
        });

      let b = await PatientRegistration.find({
        $and: [
          { patientid: pid },
          // { "patientinfo.Appointmentinfo": { $elemMatch: { sno: aaid } } },
        ],
      });

      if(doctorprofilelist.length !=0){

b[0].patientinfo.ReasonFormeetingDoctor = JSON.parse(doctorprofilelist[0].patientinfo).reasonformeetingdoctor
b[0].patientinfo.preexistingdisease = JSON.parse(doctorprofilelist[0].patientinfo).preexistingdisease     

let obj={}
// console.log(doctorprofilelist[0])
obj['Id'] = doctorprofilelist[0]._id
obj['PatientId'] = doctorprofilelist[0].patientId
obj['Visiteddate'] = doctorprofilelist[0].visiteddate
obj['Patientinfo'] = doctorprofilelist[0].patientinfo
obj['Patientinfos'] = b[0].patientinfo
obj['VisitedPatientInfo'] =JSON.parse(doctorprofilelist[0].patientinfo)

if(doctorprofilelist[0].temperatureinfo){
  // console.log(JSON.parse(doctorprofilelist[0].temperatureinfo).id,"----")
  obj['Temperatureinfo'] = !doctorprofilelist[0].temperatureinfo ? "" : doctorprofilelist[0].temperatureinfo
  var tempobj={}
  tempobj['Id'] = JSON.parse(doctorprofilelist[0].temperatureinfo).id ? `${parseInt(JSON.parse(doctorprofilelist[0].temperatureinfo).id)}` : 0
  tempobj['Type'] = JSON.parse(doctorprofilelist[0].temperatureinfo).type ? JSON.parse(doctorprofilelist[0].temperatureinfo).type : ""
  tempobj['Message'] = JSON.parse(doctorprofilelist[0].temperatureinfo).message ? JSON.parse(doctorprofilelist[0].temperatureinfo).message : ""
  tempobj['State'] = JSON.parse(doctorprofilelist[0].temperatureinfo).state ? JSON.parse(doctorprofilelist[0].temperatureinfo).state : ""
  tempobj['Number'] = JSON.parse(doctorprofilelist[0].temperatureinfo).number ? JSON.parse(doctorprofilelist[0].temperatureinfo).number : ""
  tempobj['Date'] = JSON.parse(doctorprofilelist[0].temperatureinfo).date ? JSON.parse(doctorprofilelist[0].temperatureinfo).date : ""
  tempobj['Data'] = JSON.parse(doctorprofilelist[0].temperatureinfo).data ? {
    "Type":JSON.parse(doctorprofilelist[0].temperatureinfo).data.type ? JSON.parse(doctorprofilelist[0].temperatureinfo).data.type : "",
    "Content":JSON.parse(doctorprofilelist[0].temperatureinfo).data.content ? JSON.parse(doctorprofilelist[0].temperatureinfo).data.content : ""
  } : null
  obj['TemperatureData'] = tempobj
}
  
// obj['HwData'] = !doctorprofilelist[0].hwinfo ? {} : JSON.parse(doctorprofilelist[0].hwinfo)
if(doctorprofilelist[0].hwinfo){
  obj['Hwinfo'] = !doctorprofilelist[0].hwinfo ? "" : doctorprofilelist[0].hwinfo
  var hwobj={}
 
  
  
  hwobj['Id'] = JSON.parse(doctorprofilelist[0].hwinfo).id ? `${parseInt(JSON.parse(doctorprofilelist[0].hwinfo).id)}` : 0
    hwobj['Type'] = JSON.parse(doctorprofilelist[0].hwinfo).type ? JSON.parse(doctorprofilelist[0].hwinfo).type : ""
    hwobj['Message'] = JSON.parse(doctorprofilelist[0].hwinfo).message ? JSON.parse(doctorprofilelist[0].hwinfo).message : ""
    hwobj['State'] = JSON.parse(doctorprofilelist[0].hwinfo).state ? JSON.parse(doctorprofilelist[0].hwinfo).state : ""
    hwobj['Number'] = JSON.parse(doctorprofilelist[0].hwinfo).number ? JSON.parse(doctorprofilelist[0].hwinfo).number : ""
    hwobj['Date'] = JSON.parse(doctorprofilelist[0].hwinfo).date ? JSON.parse(doctorprofilelist[0].hwinfo).date : ""
    hwobj['Data'] = JSON.parse(doctorprofilelist[0].hwinfo).data ? {
      "Type":JSON.parse(doctorprofilelist[0].hwinfo).data.type ? JSON.parse(doctorprofilelist[0].hwinfo).data.type : "",
      "Content": 
     {
      "HeightValue": JSON.parse(doctorprofilelist[0].hwinfo)?.data?.content?.HeightValue == undefined ? '' : JSON.parse(doctorprofilelist[0].hwinfo)?.data?.content?.HeightValue,
      "WeightValue": JSON.parse(doctorprofilelist[0].hwinfo)?.data?.content?.WeightValue == undefined ? '' : JSON.parse(doctorprofilelist[0].hwinfo).data.content.WeightValue
    },
      
  } : null
    obj['HwData'] = hwobj  
}


    // obj['BodyfatData'] = !doctorprofilelist[0].bodyfatinfo ? {} : JSON.parse(doctorprofilelist[0].bodyfatinfo)
    if(doctorprofilelist[0].bodyfatinfo){
    obj['Bodyfatinfo'] = !doctorprofilelist[0].bodyfatinfo ? "" : doctorprofilelist[0].bodyfatinfo
    var bodyfatinfoobj={}
      bodyfatinfoobj['Id'] = JSON.parse(doctorprofilelist[0].bodyfatinfo).id ? `${parseInt(JSON.parse(doctorprofilelist[0].bodyfatinfo).id)}` : 0
      bodyfatinfoobj['Type'] = JSON.parse(doctorprofilelist[0].bodyfatinfo).type ? JSON.parse(doctorprofilelist[0].bodyfatinfo).type : ""
      bodyfatinfoobj['Message'] = JSON.parse(doctorprofilelist[0].bodyfatinfo).message ? JSON.parse(doctorprofilelist[0].bodyfatinfo).message : ""
      bodyfatinfoobj['State'] = JSON.parse(doctorprofilelist[0].bodyfatinfo).state ? JSON.parse(doctorprofilelist[0].bodyfatinfo).state : ""
      bodyfatinfoobj['Number'] = JSON.parse(doctorprofilelist[0].bodyfatinfo).number ? JSON.parse(doctorprofilelist[0].bodyfatinfo).number : ""
      bodyfatinfoobj['Date'] = JSON.parse(doctorprofilelist[0].bodyfatinfo).date ? JSON.parse(doctorprofilelist[0].bodyfatinfo).date : ""
      bodyfatinfoobj['Data'] = JSON.parse(doctorprofilelist[0].bodyfatinfo).data ? {
        "Type":JSON.parse(doctorprofilelist[0].bodyfatinfo).data.type ? JSON.parse(doctorprofilelist[0].bodyfatinfo).data.type : "",
        "Content":JSON.parse(doctorprofilelist[0].bodyfatinfo).data.content ? JSON.parse(doctorprofilelist[0].bodyfatinfo).data.content : ""
      }: null
      obj['BodyfatData'] = bodyfatinfoobj    
  }
  else{
    obj['Bodyfatinfo'] = ""
    var bodyfatinfoobj={}
      bodyfatinfoobj['Id'] =  ""
      bodyfatinfoobj['Type'] = ""
      bodyfatinfoobj['Message'] = ""
      bodyfatinfoobj['State'] = ""
      bodyfatinfoobj['Number'] = ""
      bodyfatinfoobj['Date'] = ""
      bodyfatinfoobj['Data'] = {
        "Type": "",
        "Content":""
      }
      obj['BodyfatData'] = bodyfatinfoobj    
    
  }



//  obj['BpData'] = !doctorprofilelist[0].bpinfo ? {} : JSON.parse(doctorprofilelist[0].bpinfo)
if(doctorprofilelist[0].bpinfo){
  obj['Bpinfo'] = !doctorprofilelist[0].bpinfo ? "" : doctorprofilelist[0].bpinfo
  var bpinfoobj={}
    bpinfoobj['Id'] = JSON.parse(doctorprofilelist[0].bpinfo).id ? `${parseInt(JSON.parse(doctorprofilelist[0].bpinfo).id)}` : 0
    bpinfoobj['Message'] = JSON.parse(doctorprofilelist[0].bpinfo).message ? JSON.parse(doctorprofilelist[0].bpinfo).message : ""
    bpinfoobj['State'] = JSON.parse(doctorprofilelist[0].bpinfo).state ? JSON.parse(doctorprofilelist[0].bpinfo).state : ""
    bpinfoobj['Number'] = JSON.parse(doctorprofilelist[0].bpinfo).number ? JSON.parse(doctorprofilelist[0].bpinfo).number : ""
    bpinfoobj['Date'] = JSON.parse(doctorprofilelist[0].bpinfo).date ? JSON.parse(doctorprofilelist[0].bpinfo).date : ""
    bpinfoobj['Data'] = JSON.parse(doctorprofilelist[0].bpinfo).data ?  {
      "Type":JSON.parse(doctorprofilelist[0].bpinfo).data.type ? JSON.parse(doctorprofilelist[0].bpinfo).data.type : "",
      "Content":JSON.parse(doctorprofilelist[0].bpinfo).data.content ? JSON.parse(doctorprofilelist[0].bpinfo).data.content : ""
    } : null
    obj['BpData'] = bpinfoobj    
}else{
  obj['Bpinfo'] = ""
  var bpinfoobj={}
    bpinfoobj['Id'] =  ""
    bpinfoobj['Type'] = ""
    bpinfoobj['Message'] = ""
    bpinfoobj['State'] = ""
    bpinfoobj['Number'] = ""
    bpinfoobj['Date'] = ""
    bpinfoobj['Data'] = {
      "Type": "",
      "Content":""
    }
    obj['BpData'] = bpinfoobj    
  
}

// obj['SpO2Data'] = !doctorprofilelist[0].spO2info ? {} : JSON.parse(doctorprofilelist[0].spO2info)
if(doctorprofilelist[0].spO2info){
  obj['SpO2info'] = !doctorprofilelist[0].spO2info ? "" : doctorprofilelist[0].spO2info
  var SpO2infoobj={}
    SpO2infoobj['Id'] = JSON.parse(doctorprofilelist[0].spO2info).id ? `${parseInt(JSON.parse(doctorprofilelist[0].spO2info).id)}` : ""
    SpO2infoobj['Message'] = JSON.parse(doctorprofilelist[0].spO2info).message ? JSON.parse(doctorprofilelist[0].spO2info).message : ""
    SpO2infoobj['State'] = JSON.parse(doctorprofilelist[0].spO2info).state ? JSON.parse(doctorprofilelist[0].spO2info).state : ""
    SpO2infoobj['Number'] = JSON.parse(doctorprofilelist[0].spO2info).number ? JSON.parse(doctorprofilelist[0].spO2info).number : ""
    SpO2infoobj['Date'] = JSON.parse(doctorprofilelist[0].spO2info).date ? JSON.parse(doctorprofilelist[0].spO2info).date : ""
    SpO2infoobj['Data'] = JSON.parse(doctorprofilelist[0].spO2info).data ? {
      "Type":JSON.parse(doctorprofilelist[0].spO2info).data.type ? JSON.parse(doctorprofilelist[0].spO2info).data.type : "",
      "Content":JSON.parse(doctorprofilelist[0].spO2info).data.content ? JSON.parse(doctorprofilelist[0].spO2info).data.content : ""
    } : null
    obj['SpO2Data'] = SpO2infoobj    
}else{
  obj['SpO2info'] = ""
  var SpO2infoobj={}
    SpO2infoobj['Id'] =  ""
    SpO2infoobj['Type'] = ""
    SpO2infoobj['Message'] = ""
    SpO2infoobj['State'] = ""
    SpO2infoobj['Number'] = ""
    SpO2infoobj['Date'] = ""
    SpO2infoobj['Data'] = {
      "Type": "",
      "Content":""
    }
    obj['SpO2Data'] = SpO2infoobj
}
        return APIResp.getSuccessResult(obj, "Success", res);
      }
      else{
        return APIResp.getErrorResult({Result:"false"},res)
      }




    } catch (err) {
      console.log(err)
      APIResp.getErrorResult(err, res);
    }
  };

  const RegistrationDetailReportBy = async (req, res) => {
    try {
      let userInput = helper.getReqValues(req);
      const doctorprofilelist = await PatientRegistration.find({
        _id: userInput.id,
      });
      console.log(doctorprofilelist, "doctorprofilelist");
      let obj = [];
      doctorprofilelist.forEach((val, i) => {
        console.log(val, "val");
        // val.Appointmentinfo.sno

        let result = {};
        (result["Id"] = val._id),
          (result["PatientId"] = val.patientid),
          (result["PatientInformation"] = val.patientinfo),
          (result["FileDetailList"] = val.documentinfo);
        // obj.push(result)
        if (i + 1 == doctorprofilelist.length) {
          res.status(200).json({ success: true, data: result });
        }
      });
    } catch (err) {
      console.log(err);
      res.status(400).json({ error: `${err}` });
    }
  };

  const getviewreport = async (req, res) => {
    try {
      let userInput = helper.getReqValues(req);
      const doctorprofilelist = await Patientvisited.find({
        patientId: userInput.PatientId,
        appointmentId: userInput.AppointmentId,
      });

      APIResp.getSuccessResult(doctorprofilelist, "Success", res);
    } catch (err) {
      APIResp.getErrorResult(err, res);
    }
  };
  const MedicalRecords = async (req, res) => {
    try {
      let userInput = helper.getReqValues(req);
      const doctorprofilelist = await Patientvisited.find({
        patientId: userInput.patientId,
      });
      // let  result = {}
      let user = userInput.devices.split(",");
      let result2 = [];
      let result1 = [];
      let result = {};
      let result3 = {};
      let result21 = [];
      let result11 = [];
      let result24 = {};
      let result25 = {};

      //      doctorprofilelist.forEach((val)=>{

      // })
      doctorprofilelist.forEach((val) => {
        console.log("valitiiiiiiiiiiiiiiiiiii", val.doctorId
        );
        if (val.bpinfo && user.includes("BP")) {
          result3 = {};

          (result3["Id"] = val.bpinfo && JSON.parse(val.bpinfo).id),
            (result3["DeviceName"] = "BP"),
            (result3["PatientId"] = val.id),
            (result3["DoctorId"] = val.doctorId),
            (result3["VisitedDate"] = val.visistedstarttime),
            (result3["JsonData"] =
            JSON.parse(val.bpinfo).data.content.BpsysValue +
              " " +
              JSON.parse(val.bpinfo).data.content.BpDiaValue +
              " " +
              JSON.parse(val.bpinfo).data.content.BpPulseValue),
           

            
              //  console.log(val.Temperature)
            result1.push(result3);
            if(result1.length==0){
              result1.push(null);
            }
           
        }
        if (val.temperatureinfo && user.includes("Temperature")) {
          result = {};
          (result["Id"] = JSON.parse(val.temperatureinfo).id),
            (result["DeviceName"] = "Temperature"),
            (result["PatientId"] = val.patientId),
            (result["VisitedDate"] = val.visistedstarttime),
            (result["JsonData"] = "T=" + JSON.parse(val.temperatureinfo).data.content),
            // JSON.parse(val.bpinfo).data.content.BpsysValue +
            result["DoctorId"] = val.doctorId,
            //    console.log(val.Temperature)
            result2.push(result);
            if(result2.length==0){
              console.log("1")
              result2.push(null);
            }
        }

        if (val.hwinfo && user.includes("HeightWeight")) {
          console.log("val", val);
          result24 = {};

          (result24["Id"] = val.hwinfo && val.hwinfo.id),
            (result24["DeviceName"] = "HeightWeight"),
            (result24["PatientId"] = val._id),
            (result24["VisitedDate"] = val.visistedstarttime),
            (result24["JsonData"] =
            JSON.parse(val.hwinfo).data.HeightValue +
              " " +
              JSON.parse(val.hwinfo).data.WeightValue),
            

              result24["DoctorId"] = val.doctorId,
            //    console.log(val.Temperature)
            result21.push(result24);
        }

        if (val.spO2info && user.includes("SPO2")) {
          console.log("val", val);

          result25 = {};

          (result25["Id"] = val.spO2info && val.spO2info.id),
            (result25["DeviceName"] = "SPO2"),
            (result25["PatientId"] = val._id),
            (result25["VisitedDate"] = val.visistedstarttime),
            (result25["JsonData"] =
              JSON.parse(val.spO2info).data.content.Spo2Value +
              " " +
              JSON.parse(val.spO2info).data.content.PrValue +
              " " +
              JSON.parse(val.spO2info).data.content.RrValue),
            // result3["Visite"] = "----------------",

            result25["DoctorId"] = val.doctorId,
            //    console.log(val.Temperature)
         
            result11.push(result25);
            
        }
      });

      res
        .status(200)
        .json({
          success: true,
          data: {
            DeviceData1: [result1],
            DeviceData2: [result2],
            DeviceData3: [result21],
            DeviceData4: [result11],
            
          },
        });
    } catch (err) {
      res.status(400).json({ error: `${err}` });
    }
  };

  const GetAppointmentData = async (req, res) => {
    try {
      let userInput = helper.getReqValues(req);
      let condn1=[]
      console.log(Object.keys(userInput))

if(Object.keys(userInput).length == 1){
    if(userInput.AppointmentDateforstring){
        condn1=[{
            "patientinfo.Appointmentinfo.appointmentdateforstring": userInput.AppointmentDateforstring 
        }]
    }
    if(userInput.SelectedDoctorId){
        condn1=[{
            "patientinfo.Appointmentinfo.doctornameid": userInput.SelectedDoctorId              
        }]
    }
    if(userInput.SelectedSpecialistId){
        condn1=[{
            'patientinfo.Appointmentinfo.doctorpractitionerid': userInput.SelectedSpecialistId
        }]
    }
}

      if (Object.keys(userInput).length == 2) {
        if(userInput.AppointmentDateforstring && userInput.SelectedDoctorId){
            condn1 = [
                {
                  "patientinfo.Appointmentinfo.appointmentdateforstring": userInput.AppointmentDateforstring,
                },
                {
                  "patientinfo.Appointmentinfo.doctornameid": userInput.SelectedDoctorId,
                }
              ]
        }
        if(userInput.AppointmentDateforstring && userInput.SelectedSpecialistId){
            condn1 = [
                {
                  "patientinfo.Appointmentinfo.appointmentdateforstring": userInput.AppointmentDateforstring,
                },
                {
                    'patientinfo.Appointmentinfo.doctorpractitionerid': userInput.SelectedSpecialistId
                }
              ]
        }
        
        if(userInput.SelectedDoctorId && userInput.SelectedSpecialistId){
            condn1 = [
                {
                    "patientinfo.Appointmentinfo.doctornameid": userInput.SelectedDoctorId,
                },
                {
                    'patientinfo.Appointmentinfo.doctorpractitionerid': userInput.SelectedSpecialistId
                }
              ]
        }
      }
      

      if(Object.keys(userInput).length == 3){
        if(userInput.AppointmentDateforstring && userInput.SelectedDoctorId && userInput.SelectedSpecialistId){
            condn1=[{
                "patientinfo.Appointmentinfo.appointmentdateforstring": userInput.AppointmentDateforstring,
              },
              {
                "patientinfo.Appointmentinfo.doctornameid": userInput.SelectedDoctorId,
              },{
                'patientinfo.Appointmentinfo.doctorpractitionerid': userInput.SelectedSpecialistId
              }]
        }
      }
      
      let condn = await PatientRegistration.aggregate([
        {
          $unwind: {
            path: "$patientinfo.Appointmentinfo",
          },
        },
        {
          $match: {
            '$and':condn1
          
          },
        },
      ]);

      let obj = [];
      condn.forEach((val) => {
        let result = {};
        (result["Sno"] = val.patientinfo.Appointmentinfo.sno),
          (result["AppointmentId"] = val.patientinfo.Appointmentinfo.sno),
          (result["PatientId"] = val.patientinfo.Appointmentinfo.patientId),
          (result["PatientName"] = val.patientinfo.LastName),
          (result["DoctorId"] = val.patientinfo.Appointmentinfo.doctornameid),
          (result["DoctorName"] = val.patientinfo.Appointmentinfo.doctorname),
          (result["SpecialistId"] =
            val.patientinfo.Appointmentinfo.doctorpractitionerid),
          (result["Specialist"] =
            val.patientinfo.Appointmentinfo.doctorpractitionername),
          // result["Appoinmentid"] = val.appointmentId
          (result["Age"] = val.patientinfo.Age),
          (result["Gender"] = val.patientinfo.Gender),
          (result["MobileNo"] = val.patientinfo.CellPhoneNo),
          (result["ReasonFormeetingDoctor"] = val.patientinfo.Appointmentinfo.Reasonformeetingdoctor),
          (result["AppointmentDate"] =
            val.patientinfo.Appointmentinfo.appointmentdate),
          obj.push(result);
      });
      // console.log(obj)
      // res.status(200).json({ success: true, result: obj })
      APIResp.getSuccessResult(obj, "Success", res);
    } catch (err) {
      console.log(err);
      res.status(400).json({ error: `${err}` });
    }
  };

  const PatientVisitedReportById =async (req,res)=>{
try{
let userInput = helper.getReqValues(req)
  let data= await Patientvisited.find({
    patientId:userInput.patientId
  })
  // .sort({data:-1})
  let obj = [];
  data.forEach((val) => {
        console.log(val);

        let result = {};
        (result["Id"] = val._id),
          (result["VistedDate"] = val.visiteddate),
          (result["patientId"] = val.patientId),
          (result["patientinfo"] = !val.patientinfo ? "":val.patientinfo),
          
          (result["temperatureinfo"] = !val.temperatureinfo ? "": val.temperatureinfo),
          (result["bpinfo"] = !val.bpinfo ? "":val.bpinfo),
          (result["hwinfo"] = !val.hwinfo ? "":val.hwinfo),
          (result["spO2info"] = !val.spO2info ? "":val.spO2info),
          (result["ecginfo"] = ''),
          (result["stestoscopeinfo"] = '');

          (result["otoscopeinfo1"] = ''),
          (result["otoscopeinfo2"] = ''),
          (result["otoscopeinfo3"] = ''),
          (result["otoscopeinfo4"] = ''),
          (result["otoscopeinfo5"] = ''),
          (result["otoscopeinfo6"] = ''),
          (result["otoscopeinfo7"] = ''),
          (result["otoscopeinfo8"] = '');
          (result["otoscopeinfo9"] = ''),
          (result["otoscopeinfo10"] = '');
          (result["appoinmentid"] = val.appointmentid);
          (result["patientDescription"] =!val.patientDescription ? "" : val.patientDescription);
          (result["doctorid"] = val.doctorId);

        obj.push(result);
      });
      // obj.reverse()
      obj.sort((a, b) => b.Id - a.Id);

APIResp.getSuccessResult(obj[0],"success",res)
}
catch(err){
  console.log(err)
  APIResp.getErrorResult(err,res)
}
  }

  const VisitedPatientReport = async (req, res) => {
    try {
      let userInput = helper.getReqValues(req);
      console.log(userInput,"userInput")
      const doctorprofilelist = await Patientvisited.find({
        visiteddate: { $gte: userInput.FramDateforstring, $lte: userInput.ToDateforstring },
      });
      // console.log("visistedstarttime",visistedstarttime)
      let obj = [];
      doctorprofilelist.forEach((val) => {
        // console.log(val,"vaaaaaaa");

        let result = {};
        (result["Id"] = val._id),
          (result["VistedDate"] = val.visiteddate),
          (result["patientId"] = val.patientId),
          (result["PatientName"] = JSON.parse(val.patientinfo).patient_name),
          (result["Age"] = JSON.parse(val.patientinfo).age),
          //VistedDate
          (result["VistedDate"] = (val.visistedstarttime).split(' ')[1]),
         
          (result["Gender"] = JSON.parse(val.patientinfo).gender),
          (result["MobileNo"] = JSON.parse(val.patientinfo).mobileno),
          (result["Diagnosis"] = JSON.parse(val.patientinfo).reasonformeetingdoctor),
          (result["Appoinmentid"] = val.appointmentid);

        obj.push(result);
      });

      // res.status(200).json({ success: true, result: obj })
      APIResp.getSuccessResult(obj, "Success", res);

      // res.status(200).json({ success: true, result: a })
    } catch (err) {
      res.status(400).json({ error: `${err}` });
    }
  };
  const VisitedPatientReportdata = async (req, res) => {
    try {
      let userInput = helper.getReqValues(req);
      const doctorprofilelist = await Patientvisited.find({
        visistedstarttime: { $gte: userInput.fromdate, $lte: userInput.todate },
      });
      // console.log("visistedstarttime",visistedstarttime)
      let obj = [];
      doctorprofilelist.forEach((val) => {
        console.log(val);

        let result = {};
        (result["Id"] = val._id),
          (result["VistedDate"] = val.visistedstarttime),
          (result["patientId"] = val.patientId),
          (result["PatientName"] = val.patientinfo.patient_name),
          (result["Age"] = val.patientinfo.age),
          (result["Gender"] = val.patientinfo.gender),
          (result["MobileNo"] = val.patientinfo.mobileno),
          (result["Diagnosis"] = val.patientinfo.reasonformeetingdoctor),
          (result["Appoinmentid"] = val.appointmentId);

        obj.push(result);
      });

      // res.status(200).json({ success: true, result: obj })
      APIResp.getSuccessResult(obj, "Success", res);

      // res.status(200).json({ success: true, result: a })
    } catch (err) {
      res.status(400).json({ error: `${err}` });
    }
  };
//   const storingdata = async (req, res) => {
//     try {
//         let userInput = helper.getReqValues(req)
//         // console.log(userInput,"--------")
//         const { id, deviceid, patientId, visitedid, imageno, jsondata, doctorId, appointmentId, visistedstarttime, visistedendtime } = req.body

//         // if (deviceid == "") return res.status(400).json({ error: 'deviceid not found!' })
//         // if (patientId == null) return res.status(400).json({ error: 'patientId not found!' })
//         // if (doctorId == null) return res.status(400).json({ error: 'doctorId not found!' })
//         // if (appointmentId == null) return res.status(400).json({ error: 'appointmentId not found!' })
//         // if (imageno > 10) return res.status(400).json({ error: 'please correct imageno!' })



//         const condn = await PatientRegistration.aggregate(

//             [{
//                 $unwind: {
//                     path: '$patientinfo.Appointmentinfo'
//                 }
//             }, {
//                 $match: {
//                     'patientinfo.Appointmentinfo.doctornameid': doctorId,
//                     'patientinfo.Appointmentinfo.sno': appointmentId
//                 }
//             }]
//         )



// console.log(condn,"----")



//         if (condn.length !== 0) {
//             console.log("deviceid",deviceid)
//             console.log("id",id)
//             console.log("visitedid",visitedid)

//             if (deviceid == "0" && id == 0 && visitedid == 0) {
//                 // console.log("deviceid", deviceid)
//                 // const patientinfodata = await Patientvisited.save({  })
//                 // res.json({ patientinfodata })
//                 const user = new Patientvisited({
//                     patientId: patientId,
//                     doctorId: doctorId,
//                     visistedstarttime: visistedstarttime,
//                     visistedendtime: visistedendtime,
//                     appointmentid: appointmentId,
//                     patientinfo: jsondata,
//                     visiteddate:moment().format("YYYY-MM-DD")
//                 })
//                 try {
//                     const savedUser = await user.save()

//                     // const token = createJwt(user._id)
//                     // res.header('auth-token', token)
//                     // console.log(savedUser,"savedUsersavedUsersavedUsersavedUser")
//                     console.log("patientId1",patientId)
//                     console.log("patientId2",savedUser.patientId)
//                     console.log("patientId3",savedUser.appointmentid)
//                     console.log("patientId4",appointmentId)
//                      const personupdate = await PatientRegistration.updateOne({patientid:patientId,'patientinfo.Appointmentinfo.sno':appointmentId }, { $set: {'patientinfo.Appointmentinfo.$.isvisited': true } })
//                      console.log(personupdate)
//                     return APIResp.getSuccessResult({Result:user._id},"Registered successfully",res)
//                     // return res.status(200).json({
//                     //     success: true,
//                     //     message: 'Registered successfully',
//                     //     registeredUserId: user._id,
//                     //     // accessToken: token
//                     // })

//                 } catch (err) {
//                     res.status(400).send({ error: `${err}` });
//                 }
//             }

//             if (deviceid == "5" && id !=0 && visitedid !=0) {

//                 try {
//                     console.log("temperatureinfo", deviceid)

//                     await Patientvisited.findOneAndUpdate({
//                         _id: id
//                     },
//                         {
//                             $set: { "temperatureinfo": jsondata }
//                         })
//                         const personupdate = await Patientvisited.updateOne({patientId:patientId}, { $set: {visiteddate:moment().format("YYYY-MM-DD")} })
//                         return APIResp.getSuccessResult({Result:id},"Registered successfully",res)
//                     // res.status(200).json({
//                     //     success: true,

//                     // })

//                 } catch (err) {
//                     console.log(err,"errr")
//                     res.status(400).send({ error: `${err}` });
//                 }

//             }
//             if (deviceid == "102" && id !=0 && visitedid !=0) {

//                 try {
//                     console.log("deviceid", deviceid)

//                     await Patientvisited.findOneAndUpdate({
//                         _id: id
//                     },
//                         {
//                             $set: { "patientDescription": jsondata }
//                         })
//                         const personupdate = await Patientvisited.updateOne({patientId:patientId}, { $set: {visiteddate:moment().format("YYYY-MM-DD")} })
//                         return APIResp.getSuccessResult({Result:id},"Registered successfully",res)
//                     // res.status(200).json({
//                     //     success: true,

//                     // })

//                 } catch (err) {
//                     res.status(400).send({ error: `${err}` });
//                 }

//             }
//             if (deviceid == null && id !=0 && visitedid !=0) {

//                 try {
//                     console.log("deviceidallllll", deviceid)

//                     await Patientvisited.findOneAndUpdate({
//                         _id: id
//                     },
//                         {
//                             $set: { "visistedendtime": visistedendtime }
//                         })
//                         const personupdate = await Patientvisited.updateOne({patientId:patientId}, { $set: {visiteddate:moment().format("YYYY-MM-DD")} })
//                         return APIResp.getSuccessResult({Result:id},"Registered successfully",res)
//                     // res.status(200).json({
//                     //     success: true,

//                     // })

//                 } catch (err) {
//                     res.status(400).send({ error: `${err}` });
//                 }

//             }
//             if (deviceid == "7" && id != 0 && visitedid != 0) {
//                 try {

//                     await Patientvisited.findOneAndUpdate({
//                         _id: id
//                     },
//                         {
//                             $set: { "bpinfo": jsondata }
//                         })
//                         const personupdate = await Patientvisited.updateOne({patientId:patientId}, { $set: {visiteddate:moment().format("YYYY-MM-DD")} })
//                         return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

//                 } catch (err) {
//                     res.status(400).send({ error: `${err}` });
//                 }

//             }
//             if (deviceid == "8" && id !=0 && visitedid !=0)  {
//                 try {
//                     console.log("hwinfo", deviceid)
//                     await Patientvisited.findOneAndUpdate({
//                         _id: id
//                     },
//                         {
//                             $set: { "hwinfo": jsondata }
//                         })
//                         const personupdate = await Patientvisited.updateOne({patientId:patientId}, { $set: {visiteddate:moment().format("YYYY-MM-DD")} })
//                         return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

//                 } catch (err) {
//                     res.status(400).send({ error: `${err}` });
//                 }

//             }
//             if (deviceid == "100" && id != 0 && visitedid != 0) {
//                 try {

//                     await Patientvisited.findOneAndUpdate({
//                         _id: id
//                     },
//                         {
//                             $set: { "spO2info": jsondata }
//                         })
//                         const personupdate = await Patientvisited.updateOne({patientId:patientId}, { $set: {visiteddate:moment().format("YYYY-MM-DD")} })
//                         return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

//                 } catch (err) {
//                     res.status(400).send({ error: `${err}` });
//                 }

//             }
//             if (deviceid == "101" && id != 0 && visitedid != 0) {
//                 try {

//                     await Patientvisited.findOneAndUpdate({
//                         _id: id
//                     },
//                         {
//                             $set: { "bodyfatinfo": jsondata }
//                         })
//                         const personupdate = await Patientvisited.updateOne({patientId:patientId}, { $set: {visiteddate:moment().format("YYYY-MM-DD")} })
//                         return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

//                 } catch (err) {
//                     res.status(400).send({ error: `${err}` });
//                 }

//             }
//             if (deviceid == "6" && id != 0 && visitedid != 0) {
//                 try {

//                     await Patientvisited.findOneAndUpdate({
//                         _id: id
//                     },
//                         {
//                             $set: { "ecginfo": jsondata }
//                         })
//                         const personupdate = await Patientvisited.updateOne({patientId:patientId}, { $set: {visiteddate:moment().format("YYYY-MM-DD")} })
//                         return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

//                 } catch (err) {
//                     res.status(400).send({ error: `${err}` });
//                 }

//             }
//             if (deviceid == "1" && id != 0 && visitedid != 0) {
//                 try {

//                     await Patientvisited.findOneAndUpdate({
//                         _id: id
//                     },
//                         {
//                             $set: { "stestoscopeinfo": jsondata }
//                         })
//                         const personupdate = await Patientvisited.updateOne({patientId:patientId}, { $set: {visiteddate:moment().format("YYYY-MM-DD")} })
//                         return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

//                 } catch (err) {
//                     res.status(400).send({ error: `${err}` });
//                 }

//             }
//             if (deviceid == "700" && id != 0 && visitedid != 0) {
//                 try {

//                     await Patientvisited.findOneAndUpdate({
//                         _id: id
//                     },
//                         {
//                             $set: { "glucometerinfo": jsondata }
//                         })
//                         const personupdate = await Patientvisited.updateOne({patientId:patientId}, { $set: {visiteddate:moment().format("YYYY-MM-DD")} })
//                         return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

//                 } catch (err) {
//                     res.status(400).send({ error: `${err}` });
//                 }

//             }
//             if (deviceid == "4") {

//                 try {
//                   console.log("imageno1----",imageno)
//                 //   var updData = { `otoscopeinfo${imageno}` : jsondata } 
//                   var update = { $set : {} };
//                       update.$set['otoscopeinfo' + imageno] = jsondata;
// //{$set:{$set}}
//                       console.log(update,"update-----")
//                     await Patientvisited.findOneAndUpdate({
//                         _id: id
//                     },
//                     update)
//                     const personupdate = await Patientvisited.updateOne({patientId:patientId}, { $set: {visiteddate:moment().format("YYYY-MM-DD")} })
//                         return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

//                 } catch (err) {
//                     console.log(err,"err")
//                     res.status(400).send({ error: `${err}` });
//                 }

//             }
//             // if (deviceid == "4" && imageno == 2) {
//             //     try {

//             //         await Patientvisited.findOneAndUpdate({
//             //             _id: id
//             //         },
//             //             {
//             //                 $set: { "otoscopeinfo2": jsondata }
//             //             })
//             //             return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

//             //     } catch (err) {
//             //         res.status(400).send({ error: `${err}` });
//             //     }

//             // }
//             // if (deviceid == "4" && imageno == 3) {
//             //     try {

//             //         await Patientvisited.findOneAndUpdate({
//             //             _id: id
//             //         },
//             //             {
//             //                 $set: { "otoscopeinfo3": jsondata }
//             //             })
//             //             return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

//             //     } catch (err) {
//             //         res.status(400).send({ error: `${err}` });
//             //     }

//             // }
//             // if (deviceid =="4" && imageno == 4) {
//             //     try {

//             //         await Patientvisited.findOneAndUpdate({
//             //             _id: id
//             //         },
//             //             {
//             //                 $set: { "otoscopeinfo4": jsondata }
//             //             })
//             //             return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

//             //     } catch (err) {
//             //         res.status(400).send({ error: `${err}` });
//             //     }

//             // }
//             // if (deviceid == "4" && imageno == 5) {
//             //     try {

//             //         await Patientvisited.findOneAndUpdate({
//             //             _id: id
//             //         },
//             //             {
//             //                 $set: { "otoscopeinfo5": jsondata }
//             //             })
//             //             return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

//             //     } catch (err) {
//             //         res.status(400).send({ error: `${err}` });
//             //     }

//             // }
//             // if (deviceid == "4" && imageno == 6) {
//             //     try {

//             //         await Patientvisited.findOneAndUpdate({
//             //             _id: id
//             //         },
//             //             {
//             //                 $set: { "otoscopeinfo6": jsondata }
//             //             })
//             //             return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

//             //     } catch (err) {
//             //         res.status(400).send({ error: `${err}` });
//             //     }

//             // }
//             // if (deviceid == "4" && imageno == 7) {
//             //     try {

//             //         await Patientvisited.findOneAndUpdate({
//             //             _id: id
//             //         },
//             //             {
//             //                 $set: { "otoscopeinfo7": jsondata }
//             //             })
//             //             return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

//             //     } catch (err) {
//             //         res.status(400).send({ error: `${err}` });
//             //     }

//             // }

//             // if (deviceid == "4" && imageno == 8) {
//             //     try {

//             //         await Patientvisited.findOneAndUpdate({
//             //             _id: id
//             //         },
//             //             {
//             //                 $set: { "otoscopeinfo8": jsondata }
//             //             })
//             //             return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

//             //     } catch (err) {
//             //         res.status(400).send({ error: `${err}` });
//             //     }

//             // }
//             // if (deviceid == "4" && imageno == 9) {
//             //     try {

//             //         await Patientvisited.findOneAndUpdate({
//             //             _id: id
//             //         },
//             //             {
//             //                 $set: { "otoscopeinfo9": jsondata }
//             //             })
//             //             return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

//             //     } catch (err) {
//             //         res.status(400).send({ error: `${err}` });
//             //     }

//             // }
//             // if (deviceid == "4" && imageno == 10) {
//             //     try {

//             //         await Patientvisited.findOneAndUpdate({
//             //             _id: id
//             //         },
//             //             {
//             //                 $set: { "otoscopeinfo10": jsondata }
//             //             })
//             //             return APIResp.getSuccessResult({Result:id},"Registered successfully",res)

//             //     } catch (err) {
//             //         res.status(400).send({ error: `${err}` });
//             //     }

//             // }
//         }
//         else {
//             return res.status(400).json({ error: 'some data is invalid!' })
//         }

//         // const user = new Patientvisited({

//         //     visiteddate: userInput.visiteddate,
//         //     patientId: userInput.patientId,
//         //     visistedstarttime: userInput.visistedstarttime,
//         //     visistedendtime: userInput.visistedendtime,
//         //     visitedid: userInput.visitedid,
//         //     appointmentId: userInput.appointmentId,
//         //     deviceid: userInput.deviceid,
//         //     temperatureinfo: userInput.temperatureinfo,
//         //     appoinmentid: userInput.appoinmentid,
//         //     patientinfo: userInput.patientinfo,
//         //     doctorid: userInput.doctorid,
//         //     otoscopeinfo1:userInput.otoscopeinfo1,
//         //     otoscopeinfo2:userInput.otoscopeinfo2,
//         // })
//         // console.log(user, "user")
//         // try {
//         //     const savedUser = await user.save()
//         //     // console.log(savedUser,"savedUser")

//         //     // const token = createJwt(user._id)
//         //     // res.header('auth-token', token)
//         //     res.status(200).json({
//         //         success: true,
//         //         message: 'Registered successfully',

//         //         // accessToken: token
//         //     })

//     } catch (err) {
//         res.status(400).send({ error: `${err}` });
//     }

// }

  // "Sno": 3,
  // "Id": 8124,
  // "VistedDate": "2021-12-21T13:33:18.1",
  // "VistedDateforstring": "21-Dec-2021",
  // "PatientId": "P8009",
  // "PatientName": "parthipan",
  // "Age": "27",
  // "Gender": "Male",
  // "MobileNo": "9478569824",
  // "Diagnosis": "",
  // "Appoinmentid": 2
  const VisitedViewPatientReport = async (req, res) => {
    try {
      let userInput = helper.getReqValues(req);

      let a = await Patientvisited.find({ patientId: userInput.patientId });

      let result = [];
      a.length > 0 &&
        a.forEach((val) => {
          let obj = {};
          (obj["Id"] = val._id),
            (obj["VistedDate"] = val.visistedstarttime),
            (obj["VistedDateforstring"] = val.visistedstarttime),
            (obj["PatientId"] = val.patientId),
            (obj["PatientName"] = val.patientinfo.patient_name),
            (obj["Age"] = val.patientinfo.age),
            (obj["Gender"] = val.patientinfo.gender),
            (obj["MobileNo"] = val.patientinfo.mobileno),
            (obj["Diagnosis"] = val.patientinfo.reasonformeetingdoctor),
            (obj["Appoinmentid"] = val.appointmentId);
          result.push(obj);
        });
      APIResp.getSuccessResult(result, "Success", res);
    } catch (err) {
      res.status(400).send({ error: `${err}` });
    }
  };

  const gettoken = async (req, res) => {
    try {
      let a = await createJwt("Report");
      // res.send(a)
      res.status(200).json({ access_token: a, Error: null });
    } catch (err) {
      console.log(err);
      res.status(400).json({ Error: err });
    }
  };

  return {
    GetAllPatientRegistrations,
    // GetPatientPrescription,
    getviewreport,
    VisitedPatientReportdata,
    VisitedPatientReport,
    ViewPatientReport,
    RegistrationDetailReportBy,
    GetAppointmentData,
    MedicalRecords,
    VisitedViewPatientReport,
    gettoken,
    PatientVisitedReportById
  };
};

module.exports = DoctorController();
