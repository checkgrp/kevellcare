const helper = require("../utils/helper");
const { createJwt, encrypt, decrypt } = require('../utils/common');
const { validationResult } = require('express-validator');
// const sendEmail = require("../utils/sendEmail")
const {
    PatientRegistration,
    Users,
    PatientKitMapping
} = require('../models/kevellCareModels')
const crypto = require('crypto')
const moment = require('moment')
const mongoose = require('mongoose')
const APIResp = require('../utils/APIResp.js');
const { Console } = require("console");

const patientController = () => {
   

   

   

  

   
    



    

    const mappingkit = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)
    // const doctorprofilelist = await PatientKitMapping.aggregate(
    //     [
    //         {
    //             '$match': {
    //                 'patientid': userInput.PatientId,
    //                 'patientkitid':userInput.PatientKitId
    //             }
    //         }
    //     ]
    // )
    const doctorlist = await PatientKitMapping.find({"patientid":userInput.PatientId,"patientkitid":userInput.PatientKitId}).populate([{
        path: "patientid",
        model: 'PatientRegistration',
    }])
                    
      
    res.status(200).json({ success: true, response: doctorlist })
        }
        catch (err) {
            res.status(400).json({ error: `${err}` });
        }

    }
    const getVisitedPatients = async (req, res) => {
        try {
            const visitedpatienttoday = await PatientRegistration.aggregate([
                {
                    '$unwind': {
                        'path': '$patientinfo.Appointmentinfo'
                    }
                }, {
                    '$match': {
                        'patientinfo.Appointmentinfo.appointmentdateforstring': moment().format("YYYY/MM/DD")
                    }
                }
            ])
            res.status(200).json({ success: true, message: "today visited patients list", response: visitedpatienttoday })
        }
        catch (err) {
            res.status(400).json({ error: `${err}` });
        }

    }
    const viewVisitedDoctorlistBy = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)

            const visitedpatienttoday = await PatientRegistration.aggregate([
                {
                    '$match': {
                        '_id': mongoose.Types.ObjectId(userInput.Patientid)
                    }
                }, {
                    '$unwind': {
                        'path': '$patientinfo.Appointmentinfo'
                    }
                }, {
                    '$project': {
                        'patientinfo.Appointmentinfo.doctornameid': 1
                    }
                }
            ])
            res.status(200).json({ success: true, message: "patient booked doctor list", response: visitedpatienttoday })
        }
        catch (err) {
            res.status(400).json({ error: `${err}` });
        }


    }

    const getPatientKitMappingBymacId = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)
            
            const doctorprofilelist = await PatientKitMapping.find({ macid: userInput.macid })

            let resultall=[]


          
            doctorprofilelist.forEach(val => {
                console.log("val",val)
              let result={}
            
                result["Id"] = val._id,
                    result["macid"] = val.macid,
                    result["patientkitid"] = val.patientkitid,
                    result["patientid"]=val.patientid
                  
                    
                   


                   
                  resultall.push(result) 
                  
                   
            })
                  
                  
                    APIResp.getSuccessResult(resultall, "Success", res)

            // res.status(200).json({ success: true, result: doctorprofilelist })
        }
        catch (err) {
            res.status(400).send({ error: `${err}` });
        }
    }
    const validatemacId = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)
            
            const doctorprofilelist = await PatientKitMapping.find({ macid: userInput.macid })
           console.log("doctorprofilelist",doctorprofilelist)
            if(doctorprofilelist){
              return res.status(200).json({ success: true,responsecode:200, message: "Success","data": {
                "Result": true
            } })
            }
            else{
                return res.status(400).json({ success: false,responsecode:201, message: "No record found","data": {
                    "Result": false
                } })

            }
        }
        catch (err) {
            res.status(400).send({ error: `${err}` });
        }
    }
    const getPatientKitMappingByPaitentid = async (req, res) => {
        // try {
        //     let userInput = helper.getReqValues(req)
            
        //     const doctorprofilelist = await PatientKitMapping.find({ patientid: userInput.patientid })
        //     res.status(200).json({ success: true, result: doctorprofilelist })
        // }
        // catch (err) {
        //     res.status(400).send({ error: `${err}` });
        // }
        try {
            let userInput = helper.getReqValues(req)
            console.log(userInput,"-----")
            const doctorprofilelist = await PatientKitMapping.find({ patientid: userInput.patientId})

if(doctorprofilelist.length==0){
    let obj={}
    obj["Id"] = 0,
    obj["macid"] = null,
    obj["patientkitid"] = null,
    obj["patientid"] = null
    return APIResp.getSuccessResult(obj, "Success", res)
}

          
            doctorprofilelist.forEach(val => {
                // console.log("val",val)
              let result={}
            
                result["Id"] = val._id,
                    result["macid"] = val.macid,
                    result["patientkitid"] = val.patientkitid,
                    result["patientid"]=val.patientid
                
                  resultall.push(result) 
                  
                   
            })
                  
                  
                    APIResp.getSuccessResult(resultall, "Success", res)

            // res.status(200).json({ success: true, result: doctorprofilelist })
        }
        catch (err) {
            res.status(400).send({ error: `${err}` });
        }
    }
    const getallkit = async (req, res) => {
        // try {
           
        //     const todayPatient = await PatientKitMapping.find()
        //     res.status(200).json({ success: true, result: todayPatient })
        // }
        // catch (err) {
        //     res.status(400).json({ error: `${err}` });
        // }
        try {
            let userInput = helper.getReqValues(req)
            
            const doctorprofilelist = await PatientKitMapping.find({})

            let resultall=[]


          
            doctorprofilelist.forEach(val => {
                console.log("val2222222222",val)
                if(val.patientid!=""){
                    let result={}
            
                result["Id"] = val._id,
                    result["macid"] = val.macid,
                    result["patientkitid"] = val.patientkitid,
                    result["patientid"]=val.patientid
                  
                    
                   


                   
                  resultall.push(result) 
                }
              
                  
                   
            })
                  
                  
                    APIResp.getSuccessResult(resultall, "Success", res)

            // res.status(200).json({ success: true, result: doctorprofilelist })
        }
        catch (err) {
            res.status(400).send({ error: `${err}` });
        }
    }
    const  getMeetingDoctorlistBypatientid = async(req,res) =>{
        try{
        let userInput = helper.getReqValues(req)
        // const doctorprofilelist = await PatientKitMapping.findOne({ macid: userInput.macid })

        const doctorprofilelist = await PatientKitMapping.aggregate(
            [
                {
                    '$match': {
                        'patientid': userInput.patientid
                    }
                }, {
                    '$project': { 
                        'Doctorlist': '$doctorlist'
                    }
                }
            ]
        )
        
        let result={}
            let all=[]
            console.log(doctorprofilelist[0].Doctorlist)
//             doctorprofilelist[0].doctorlist.forEach(val=>{
//                 result={}
//                 // console.log("val",val.doctorlist)
//                 result["Id"] =val.Id,
//                 result["Doctorid"] =val.Doctorid,
//                 result["Doctorname"] =val.Doctorname,
//                 result["Zoomid"] =val.Zoomid,
//                 result["Zoompassword"] =val.Zoompassword,
//                 result["Zoomlink"] =val.Zoomlink
                
// all.push(result)
//             })
            APIResp.getSuccessResult(doctorprofilelist[0].Doctorlist==null? [] : doctorprofilelist[0].Doctorlist, "Success", res)


    }
    catch (err) {
        console.log(err)
        res.status(400).json({ error: `${err}` });
    }
    }

    const UpdatePatientidmacid = async(req,res) =>{
        let userInput = helper.getReqValues(req)
        console.log("userInput",userInput)

        const doctorprofilelist = await PatientKitMapping.aggregate(
            [
                {
                    '$match': {
                        'macid': userInput.macid
                    }
                } 
                
            ]
        )
        // console.log("doctorprofilelist",doctorprofilelist)
        // if(doctorprofilelist.length >0)  return res.status(200).json({ success: true,responsecode:200, message: "mac id already use","data": {
        //     "Result": false
        // } })

        try {
            
           const personupdate = await PatientKitMapping.updateOne({ macid: userInput.macid  },
             { $set: { patientid: userInput.patientid} })
           //  res.status(200).json(personupdate)
        //    console.log(personupdate,"----")
           return res.status(200).json({ success: true,responsecode:200, message: "success","data": {
            "Result": true
        } })


          
        }
        catch (err) {
           console.log(err);
        //    res.status(400).json({Result:"false"})
        res.status(400).json({ success: true,responsecode:400, message: "failed","data": {
            "Result": false
        } })   
    }
     
     
     }
    //  const SaveAndUpdatePatientkitMapping = async(req,res) =>{
       
    //     try {
    //         let userInput = helper.getReqValues(req)
    //         // const {  PatientKitDoctorlist } = userInput


    //         console.log("userInput.patientid",userInput.PatientKitDoctorlist)
    //        await PatientKitMapping.findByIdAndUpdate({
    //             patientid: mongoose.Types.ObjectId (userInput.patientid)
    //         },
    //             {
    //                 $set: { "doctorlist": userInput.PatientKitDoctorlist }
    //             },{new:true}
    //             )
    //         res.status(200).json({
    //             success: true,
               
    //         })

    //     } catch (err) {
    //         res.status(400).send({ error: `${err}` });
    //     }

     
     
    //  }
    const SaveAndUpdatePatientkitMapping = async(req,res) =>{
       
        try {
            let userInput = helper.getReqValues(req)
            // console.log(userInput,"----")
            const {  PatientKitDoctorlist,patientid } = userInput

           
            
            const PatientKit = await PatientKitMapping.updateOne({
                patientid: patientid
            },
                {
                    $set: { doctorlist: PatientKitDoctorlist }
                })
            // res.status(200).json({
            //     success: true,
               
            // })
           const doctorprofilelist = await PatientKitMapping.find({
            patientid:patientid
           })
            // console.log("PatientKit",PatientKit)
            let result={}
            let all=[]
            doctorprofilelist[0].doctorlist.forEach(val=>{
                result={}
                // console.log("val",val.doctorlist)
                result["Id"] =val.Id,
                result["Doctorid"] =val.Doctorid,
                result["Doctorname"] =val.Doctorname,
                result["Zoomid"] =val.Zoomid,
                result["Zoompassword"] =val.Zoompassword,
                result["Zoomlink"] =val.Zoomlink
                
all.push(result)
            })
            APIResp.getSuccessResult(all, "Success", res)

        } catch (err) {
            console.log(err)
            res.status(400).send({ error: `${err}` });
        }

     
     
     }
     


     const AddPatientKitId = async(req,res) =>{
       
        try {
            let userInput = helper.getReqValues(req)
console.log(userInput,"---")
              const doctorprofilelist = await PatientKitMapping.aggregate(
                [
                    {
                      '$count': 'macid'
                    }
                  ]
                )
                console.log("doctorprofilelist",doctorprofilelist)
                
            const macid = await PatientKitMapping.findOne({ macid: userInput.macid, })
            if (macid)       return res.status(200).json({ success: false,responsecode:200, message: "mac id already use","data": {
                "Result": false
            } })
            let patientkitid1=''
            if(doctorprofilelist.length==0){

                patientkitid1="I0000"+1

            }
            else{

                patientkitid1=  `I0000${doctorprofilelist[0].macid+1}`
            }
            const savePerson = await new PatientKitMapping({
               macid:userInput.macid,
            //    patientkitid:  `I0000${doctorprofilelist[0].macid+1}`
            patientkitid:patientkitid1,
            patientid:"",
            doctorlist:null
             })
             console.log("savePerson",savePerson)

             const savedataperson = await savePerson.save();
             
            //  console.log("savedataperson",savedataperson)
let result={}
result["patientkitid"]=savedataperson.patientkitid,
result["macid"]=savedataperson.macid,
result["Id"]=savedataperson._id,
result["paitentid"]=savedataperson.patientid

            
            //  console.log(savedataperson,savedataperson)
            //  res.status(200).json(savedataperson)
            APIResp.getSuccessResult(result, "Success", res)
             
           } catch (err) {
            res.status(400).send({ error: `${err}` });
        }

     
     
     }
     const GetMaxNumberForPatientKitId = async(req,res) =>{
        try{
        
        const doctorprofilelist = await PatientKitMapping.find().sort({patientkitid:-1}).limit(1)
        // console.log(helper.convertarrtoObj(doctorprofilelist)._doc.patientkitid +,"---")
        let paitentkitid =helper.convertarrtoObj(doctorprofilelist)._doc.patientkitid
        let paitentkitid1 = 'I0000'+(parseInt(paitentkitid.split('I')[1])+1).toString()
        return res.status(200).json({ success: false,responsecode:201, message: "Success","data": {
            Result:paitentkitid1
        } })
        
        
    }
    catch (err) {
        res.status(400).json({ error: `${err}` });
    }
    }
    const gettoken=async (req,res)=>{
        try{
    let a=await createJwt('PatientKit')
    // res.send(a)
    res.status(200).json({ access_token: a, Error: null })
        }
        catch(err){
            res.status(400).json({Error:err})
        }
    }
    return {
       
        mappingkit,
        // fileUpload
        // login,
        // forgotpassword,
        // reset

        getPatientKitMappingBymacId,
        validatemacId,
        getPatientKitMappingByPaitentid,
        getallkit,
        getMeetingDoctorlistBypatientid,
        UpdatePatientidmacid,
        SaveAndUpdatePatientkitMapping,
        GetMaxNumberForPatientKitId,
        AddPatientKitId,
        gettoken
    }
}

module.exports = patientController()

