const express = require('express');
const router = express.Router();
const patientController = require('../controllers/patientkitController')
const {verifyToken} = require('../utils/verifyToken')

// const {
//   validateUserSignUpfields,
  // validateUserLoginfields,
//   validateUserForgotPasswordFields,
//   validateUserResetPasswordFields
// } = require('../validations/patientValidation');
// const {verifyPatientAuthentication} = require('./verifyPatientAuthentication');
// const upload = require('../utils/multer')

// router.post('/register', upload.array('file',3),patientController.register);

router.get("/kitmapping",verifyToken,patientController.mappingkit)
router.get("/GetPatientKitMappingBymacId",verifyToken,patientController.getPatientKitMappingBymacId)
router.get("/GetPatientKitMappingByPatientId",verifyToken,patientController.getPatientKitMappingByPaitentid)
router.get("/GetAllPatientKitDetails",patientController.getallkit)
router.get("/ValidateMacId",patientController.validatemacId)
router.get("/GetMeetingDoctorlistBy",verifyToken,patientController.getMeetingDoctorlistBypatientid)
router.post("/UpdatePatientid",verifyToken,patientController.UpdatePatientidmacid)
router.get("/getMaxNumberForPatientKitId",verifyToken,patientController.GetMaxNumberForPatientKitId)
router.post("/saveAndUpdatePatientkitMapping",verifyToken,patientController.SaveAndUpdatePatientkitMapping)

router.post("/AddPatientKitId",verifyToken,patientController.AddPatientKitId)

router.get("/gettoken",patientController.gettoken)

// router.post('/fileupload',patientController.fileUpload)

module.exports = router;

