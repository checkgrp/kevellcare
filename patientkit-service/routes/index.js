var express = require('express');
var router = express.Router();
const patientRoute = require('./users')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('index');
});

router.use('/patientkit', patientRoute);

module.exports = router;
