const mongoose = require('mongoose')
var autoIncrement = require('mongoose-auto-increment')


const AdminSettingSchema = new mongoose.Schema({
    Devicename: {type: JSON},
},{ timestamps: false });

const DoctorLocationSchema = new mongoose.Schema({
    Location: {type: String},
},{ timestamps: false });


const DoctorScheduleSchema = new mongoose.Schema({
    scheduleDate: {type: Date},
    schedulemonth: {type: Number},
    scheduleyear: {type: Number},
    doctorscheduleinfo: {type: JSON},
    createdby:{type: String},
    modifiedby:{type: String}
},{ timestamps: true });


const InsurancepolicySchema = new mongoose.Schema({
    Insurancename: {type: String},
},{ timestamps: false });


const PatientAppointmentSchema = new mongoose.Schema({
    appointmentinfo: {type: String},
    createdby:{type: String},
    modifiedby:{type: String}
},{ timestamps: true });


const PatientKitMappingSchema = new mongoose.Schema({
    macid: {type: String},
    patientkitid: {type: String},
    patientid: {type: String},
    doctorlist: {type: JSON},
    createdby:{type: String},
    modifiedby:{type: String}
},{ timestamps: true });
autoIncrement.initialize(mongoose.connection);
PatientKitMappingSchema.plugin(autoIncrement.plugin, {
    model: "PatientKitMapping", // collection or table name in which you want to apply auto increment
    field: "_id", // field of model which you want to auto increment
    startAt: "1000",////"I0000"+{autoIncrement.initialize(mongoose.connection)}, // start your auto increment value from 1
    incrementBy: 1, // incremented by 1
  });

const PatientRegistrationSchema = new mongoose.Schema({
    // patientid: {type: String},
    patientinfo: {type: JSON},
    documentinfo: {type: JSON},
    appointmentdate: {type: Date},
    kittype: {type: String},
    createdby:{type: String},
    modifiedby:{type: String},
    otp:{type:String}
},{ timestamps: true });

const PatientvisitedSchema = new mongoose.Schema({
    visiteddate: {type: Date},
    patientId: {type: String},
    patientinfo: {type: String},
    temperatureinfo: {type: String},
    bpinfo: {type: String},
    hwinfo: {type: String},
    spO2info: {type: String},
    bodyfatinfo: {type: String},
    ecginfo: {type: Date},
    stestoscopeinfo: {type: String},
    otoscopeinfo1: {type: String},
    otoscopeinfo2: {type: String},
    otoscopeinfo3: {type: String},
    otoscopeinfo4: {type: Date},
    otoscopeinfo5: {type: String},
    otoscopeinfo6: {type: String},
    otoscopeinfo7: {type: String},
    otoscopeinfo8: {type: String},
    otoscopeinfo9: {type: Date},
    otoscopeinfo10: {type: String},
    appoinmentid: {type: Number},
    patientDescription: {type: Date},
    visisted_start_time:{type: String},
    visisted_end_time:{type: String},
    glucometerinfo:{type: String},
    doctorid: {type: Number},
    createdby:{type: String},
    modifiedby:{type: String}
},{ timestamps: true });


const PreexistingdiseaseSchema = new mongoose.Schema({
    existingdisease: {type: String},
},{ timestamps: false });


const RolesSchema = new mongoose.Schema({
    RoleName: {type: String},
    createdby:{type: String},
    modifiedby:{type: String}
},{ timestamps: true });


const SpecialistSchema = new mongoose.Schema({
    name: {type: String},
    createdby:{type: String},
    modifiedby:{type: String}
},{ timestamps: true });


const UsersSchema = new mongoose.Schema({
    Name: { type: String },
    Username: { type: String },
    Password: { type: JSON },
    ConfirmPassword: { type: JSON },
    Roleid: {type: String},
    //Role: { type: String },
    Emailid: { type: String },
    MoblieNo: { type: String },
    Gender: { type: String },
    City: { type: String },
    Country: { type: String },
    Address: { type: String },
    Pincode: { type: String },
    SpecialistId: {type: String},
    IsActive: { type: String },
    ProfileImagelink: { type: JSON },
    DoctorProfile: { type: String },
    Doctorrating: {type: JSON},
    Doctorapproved: {type: String},//true/false
    // SelectedRoleId: {type: String},
    // SelectedRole: { type: String },
    // SelectedSpecialistId: { type: String },
    Qualification: { type: String },
    MedicalSchool: { type: String },
    Yearsofexperience: { type: String },
    Languages: { type: String },
    Doctorfees: { type: String },
    ServerPath: { type: String },
    About: { type: String },
    Insuranceid: { type: String },
    //Insurancename: { type: String },
    
    createdby: { type: String },
    modifiedby: { type: String }
}, { timestamps: true });


const ZoomMeetingSettingsSchema = new mongoose.Schema({
    doctorId: {type: Number},
    meetingId: {type: String},
    meetingpassword: {type: String},
    meetinglink: {type: String},
    isactive: {type: String},
    createdby:{type: String},
    modifiedby:{type: String}
},{ timestamps: true });

const AdminSetting = mongoose.model('AdminSetting', AdminSettingSchema,'AdminSetting');
const DoctorLocation = mongoose.model('DoctorLocation', DoctorLocationSchema,'DoctorLocation');
const DoctorSchedule = mongoose.model('DoctorSchedule', DoctorScheduleSchema,'DoctorSchedule');
const Insurancepolicy = mongoose.model('Insurancepolicy', InsurancepolicySchema,'Insurancepolicy');
const PatientAppointment= mongoose.model('PatientAppointment', PatientAppointmentSchema,'PatientAppointment');
const PatientKitMapping= mongoose.model('PatientKitMapping', PatientKitMappingSchema,'PatientKitMapping');
const PatientRegistration= mongoose.model('PatientRegistration', PatientRegistrationSchema,'PatientRegistration');
const Patientvisited= mongoose.model('Patientvisited', PatientvisitedSchema,'Patientvisited');
const Preexistingdisease= mongoose.model('Preexistingdisease', PreexistingdiseaseSchema,'Preexistingdisease');
const Roles= mongoose.model('Roles', RolesSchema,'Roles');
const Specialist= mongoose.model('Specialist', SpecialistSchema,'Specialist');
const Users= mongoose.model('Users', UsersSchema,'Users');
const ZoomMeetingSettings= mongoose.model('ZoomMeetingSettings', ZoomMeetingSettingsSchema,'ZoomMeetingSettings');


module.exports = {
    AdminSetting,
    DoctorLocation,
    DoctorSchedule,
    Insurancepolicy,
    PatientAppointment,
    PatientKitMapping,
    PatientRegistration,
    Patientvisited,
    Preexistingdisease,
    Roles,
    Specialist,
    Users,
    ZoomMeetingSettings
}