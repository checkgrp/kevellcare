const multer = require('multer');


var multerStorage  = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/pdf')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now()+file.originalname);
  }
})
//MULTER FILTER
const multerFilter = (req, file, cb) => {
  if (file.mimetype.split("/")[1] === "pdf") {
    cb(null, true);
  } else {
    cb(new Error("Not a PDF File!!"), false);
  }
};

const upload = multer({
  storage: multerStorage,
  fileFilter: multerFilter,
});



module.exports = upload