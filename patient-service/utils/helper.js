const _ = require('lodash')
exports.getReqValues = (req) => {
  return _.pickBy(_.extend(req.body, req.params, req.query), _.identity);
}


exports.convertarrtoObj = (array) => {
  return _.reduce(array, function(memo, current) { return _.assign(memo, current) },  {})
}

