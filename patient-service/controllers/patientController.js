const helper = require("../utils/helper");
const { createJwt, encrypt, decrypt } = require('../utils/common');
const { validationResult } = require('express-validator');
const sendEmail = require("../utils/sendEmail")
const {
    PatientRegistration,
    Users,
    PatientKitMapping,
    Roles
} = require('../models/kevellCareModels')
const crypto = require('crypto')
const moment = require('moment')
const mongoose = require('mongoose')
const APIResp = require('../utils/APIResp')

const patientController = () => {
    const SaveAndUpdatePatintRegistration = async (req, res) => {

        let userInput = helper.getReqValues(req)

        // const emailExist = await PatientRegistration.findOne({ 'patientinfo.email': userInput.email })
        // if (emailExist) return res.status(400).json({ error: 'Email already exists!' })

        // const MoblieNoExist = await PatientRegistration.findOne({ 'patientinfo.mobileno': userInput.mobile })
        // if (MoblieNoExist) return res.status(400).json({ error: 'MoblieNo already exists!' })

        // const AadharExist = await PatientRegistration.findOne({ 'patientinfo.aadhar': userInput.aadhar }) 
        // if (AadharExist) return res.status(400).json({ error: 'AadharNo already exists!' })

        const patientExist = await PatientRegistration.findOne({ _id: userInput.PatientId })
        if (patientExist) {
            console.log(userInput.PatientId)
            const patientupdate= await PatientRegistration.updateOne(
                { _id: userInput.PatientId }, 
                { $set: { 
                    patientinfo: userInput.PatientInfo,
                    filedetailjson:userInput.FileDetailjson,
                    documentinfo:userInput.FileDetailList,
                    kittype:userInput.KitType,
                    macid:userInput.MacId,
                    username:userInput.UserName
                 } })

            return res.status(200).json({ patientupdate,msg: 'Updated it !' })
        }

        const patient = new PatientRegistration({
             patientinfo: userInput.PatientInfo,
             filedetailjson:userInput.FileDetailjson,
             documentinfo:userInput.FileDetailList,
             kittype:userInput.KitType,
             macid:userInput.MacId,
             username:userInput.UserName
        })
        try {
            const savedPatient = await patient.save()

            // const token = createJwt(user._id)
            // res.header('auth-token', token)
            res.status(200).json({
                success: true,
                message: 'Registered successfully',
                registeredUserId: patient._id,
                // accessToken: token
            })

        } catch (err) {
            res.status(400).send({ error: `${err}` });
        }
    }

    const sendotp = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)
            console.log("userInput",userInput)
            // console.log(userInput)
            const patient = await PatientRegistration.findOne({ 'patientinfo.EmailID': userInput.PatientEmailID })

            if (!patient) {
                return res.status(201).json({ success: false, error: "Email could not be sent" })
            }
            let otp = crypto.randomBytes(5).toString('hex');
            const message = `
             <h3>Hi ${patient.patientinfo.LastName} </h3>
            <h4>Please enter the below mentioned verification code for Patient Registration</h4>
           
            <h3>${otp}</h3>
`
            try {
                console.log(patient,"---")
                await sendEmail({
                    to: patient.patientinfo.EmailID,
                    subject: "Patient registration - verification",
                    text: message
                });
                // await patient.updateOne({ otp })
                // const savedPatient = await patient.save()

                // res.status(200).json({ success: true, message: "Email sented", otp })
                const personupdate = await PatientRegistration.updateOne({'patientinfo.EmailID': userInput.PatientEmailID }, { $set: {otp:otp } })
                return res.status(200).json({ success: true,responsecode:200, message: "Send OTP check your register mail","data": {
                    "Result": otp
                } })
            }
            catch (err) {
                console.log(err,"akakaka")
                res.status(400).json({ success: false, error: "Email could not be sent" })
            }
        }
        catch (err) {
            console.log(err,"patientinfo")
            res.status(400).json({ error: `${err}` });
        }
    }

    const verifyotp = async (req, res) => {
       
        try {
            let userInput = helper.getReqValues(req)
            console.log(userInput,"userInput")
            const patient = await PatientRegistration.findOne({  'patientinfo.EmailID': userInput.patientEmailID })
            
            if (!patient) return res.status(400).json({ success: false, error: "Email could not be sent" })
           console.log(patient.otp,"patient.otp")
            if (patient.otp === userInput.otp) {
                console.log(patient.otp)
                // return res.status(200).json({ success: true, message: "otp verified" })
                return res.status(200).json({ success: true,responsecode:200, message: "otp verified","data": {
                    "Result": true
                } })
            }
            else {
                // return res.status(400).json({ success: false, message: "otp mismatch!" })
                return res.status(200).json({ success: true,responsecode:200, message: "otp mismatch!","data": {
                    "Result": false
                } })
            }
        }
        catch (err) {
            res.status(400).json({ error: `${err}` });
        }
    }

    const bookAppointment = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)
            const patient = await PatientRegistration.findOne({ _id: userInput.patientId })
            if (!patient) return res.status(400).json({ success: false, error: "patient not found" })

            const docotr = await Users.findOne({ _id: userInput.doctornameid })
            if (!docotr) return res.status(400).json({ success: false, error: "doctor not found" })

            if (docotr.Username !== userInput.doctorname) return res.status(400).json({ success: false, error: "doctor name mismatch!" })
            try {
                console.log(Username)
                userInput.sno = crypto.randomBytes(4).toString("hex");
                userInput.isvisited = false
                userInput.date = moment().format("YYYY/MM/DD HH:mm:ss")
                await PatientRegistration.findOneAndUpdate({
                    _id: userInput.patientId
                },
                    {
                        $push: { "patientinfo.Appointmentinfo": userInput }
                    })
                res.status(200).json({
                    success: true,
                    message: `${patient.patientinfo.username} - Booked Appointment (${patient._id})`,
                })

            } catch (err) {
                res.status(400).send({ error: `${err}` });
            }

        }
        catch (err) {
            res.status(400).json({ error: `${err}` });
        }
    }

    

 

   



    

    const mappingkit = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)
    // const doctorprofilelist = await PatientKitMapping.aggregate(
    //     [
    //         {
    //             '$match': {
    //                 'patientid': userInput.PatientId,
    //                 'patientkitid':userInput.PatientKitId
    //             }
    //         }
    //     ]
    // )
    const doctorlist = await PatientKitMapping.find({"patientid":userInput.PatientId,"patientkitid":userInput.PatientKitId}).populate([{
        path: "patientid",
        model: 'PatientRegistration',
    }])
                    
      
    res.status(200).json({ success: true, response: doctorlist })
        }
        catch (err) {
            res.status(400).json({ error: `${err}` });
        }

    }
    
   

    const getPatientKitMappingBymacId = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)
            
            const doctorprofilelist = await PatientKitMapping.find({ macid: userInput.macid })
            res.status(200).json({ success: true, result: doctorprofilelist })
        }
        catch (err) {
            res.status(400).send({ error: `${err}` });
        }
    }
    const validatemacId = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)
            
            const doctorprofilelist = await PatientKitMapping.findOne({ macid: userInput.macid })
           console.log("doctorprofilelist",doctorprofilelist)
            if(doctorprofilelist){
            return res.status(200).json({ success: true,message:"success",responsecode:200, result: true })
            }
            else{
                return res.status(400).json({ success: true,message:"success",responsecode:200, result: false })

            }
        }
        catch (err) {
            res.status(400).send({ error: `${err}` });
        }
    }
    const getPatientKitMappingByPaitentid = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)
            
            const doctorprofilelist = await PatientKitMapping.find({ patientid: userInput.patientid })
            res.status(200).json({ success: true, result: doctorprofilelist })
        }
        catch (err) {
            res.status(400).send({ error: `${err}` });
        }
    }
    const getallkit = async (req, res) => {
        try {
           
            const todayPatient = await PatientKitMapping.find()
            res.status(200).json({ success: true, result: todayPatient })
        }
        catch (err) {
            res.status(400).json({ error: `${err}` });
        }
    }
    const  getMeetingDoctorlistBypatientid = async(req,res) =>{
        try{
        let userInput = helper.getReqValues(req)
        // const doctorprofilelist = await PatientKitMapping.findOne({ macid: userInput.macid })

        const doctorprofilelist = await PatientKitMapping.aggregate(
            [
                {
                    '$match': {
                        'patientid': userInput.patientid
                    }
                }, {
                    '$project': {
                        '_id': 0, 
                        'path': '$doctorlist'
                    }
                }
            ]
        )
        res.status(200).json({ success: true, result: doctorprofilelist })
        // res.status(200).json({ success: true, result: a })
    }
    catch (err) {
        res.status(400).json({ error: `${err}` });
    }
    }

    const UpdatePatientidmacid = async(req,res) =>{
        let userInput = helper.getReqValues(req)
        console.log("userInput",userInput)

        const doctorprofilelist = await PatientKitMapping.aggregate(
            [
                {
                    '$match': {
                        'macid': userInput.macid
                    }
                } 
                
            ]
        )
        console.log("doctorprofilelist",doctorprofilelist)
        if(doctorprofilelist.length >0) return res.status(400).json({ error: 'mac id already use it!' })

        try {
            
           const personupdate = await PatientKitMapping.updateOne({ patientid: userInput.patientid }, { $set: { macid: userInput.macid } })
           //  res.status(200).json(personupdate)
           res.status(200).send({
              message: "update Successfully"
           });
        }
        catch (err) {
           console.log(err);
        }
     
     
     }
     const SaveAndUpdatePatientkitMapping = async(req,res) =>{
       
        try {
            let userInput = helper.getReqValues(req)
            const {  PatientKitDoctorlist,patientid } = userInput


            
           await PatientKitMapping.findOneAndUpdate({
                patientid: patientid
            },
                {
                    $set: { "doctorlist": PatientKitDoctorlist }
                })
            res.status(200).json({
                success: true,
               
            })

        } catch (err) {
            res.status(400).send({ error: `${err}` });
        }

     
     
     }

     const GetMaxNumberForPatientKitId = async(req,res) =>{
        try{
        
        const doctorprofilelist = await PatientKitMapping.find().sort({patientkitid:-1}).limit(1)
        res.status(200).json({ success: true, result: doctorprofilelist })
        
        
    }
    catch (err) {
        res.status(400).json({ error: `${err}` });
    }
    }
    const Login = async(req,res) =>{
        try{
            let userInput = helper.getReqValues(req)
            console.log(req.body,"req.body")
            // var decryptPassword = await decrypt(userInput.Password)
        console.log(userInput,"newonetwo")
        
            let flag=0
          
            const doctorlist = await Users.findOne({"usename":userInput.username})
            .populate([{
                path: 'roleid',
                model: 'Roles',
            }])
// console.log(doctorlist,"----")
            if(!doctorlist)  
             return res.status(200).json({ success: false,responsecode:200, message: "doctorlist not match","data": {
                "Result": false
            } })
            // const {Password} = helper.convertarrtoObj(doctorlist)
            // console.log("doctorlist",await decrypt(Password),userInput.Password)
            
             if(doctorlist.password == userInput.password){
                flag=1
             }
            if(flag==1){
                // console.log(doctorlist,"--")
                // doctorlist.forEach(val=>{
               
                    let check = doctorlist.roleid.RoleName
                    if(check ==userInput.role){
    
                        let result={}
                
                        result["Id"] = doctorlist._id,
                            result["Name"] = doctorlist.name,
                            result["Username"] = doctorlist.usename,
                            result["Emailid"]=doctorlist.Emailid,
                            result["Password"] = null,
                            result["UserType"]=null,
                            result["Role"] = doctorlist.roleid.RoleName,
                           
                        // res.status(200).json({ success: true, response: result })
                    console.log(result,"---")
                        APIResp.getSuccessResult(result, "Login Record", res)
                    }
                    // else{
                    //     return res.status(200).json({ success: false,responsecode:200, message: "Success","data": {
                    //         "Result": []
                    //     } })
                    // }
                // })
                // console.log("inside if..")
            }  
        else{
            // console.log("inside else//")
            return res.status(201).json({ success: false,responsecode:201, message: "Failed","data": {
                "Result": "Invalid login!"
            } })
        }
        
    }
    catch (err) {
        console.log(err)
        res.status(400).json({ error: `${err}` });
    }
    }

    const forgotpassword = async (req, res) => {

        let userInput = helper.getReqValues(req)
        console.log(userInput,"userInput")

        // const errors = validationResult(req);
        // if (!errors.isEmpty()) {
        //     const err = errors.array()
        //     return res.status(400).json({
        //         success: false,
        //         error: err[0].msg
        //     });
        // }

        try {

            const user = await Users.findOne({ Emailid: userInput.mailid })
            // console.log("userallbelow",user)
            if (!user) {
                return res.status(400).json({ success: false, error: "Email could not be sent" })
            }

            // var decryptPassword = await decrypt(user.Password)
            console.log(user.password,"user.Password")
            // const resetUrl = `http://localhost:5000/UserAuthenticationAPI/api/UserAuthentication/reset/${resetToken}`
            const message = `
            <h3>Hi ${user.usename}</h3>
            <h4>Please use the below password to access your account.</h4>
            <h3> ${user.password}</h3>
     `
            try {
                await sendEmail({
                    to: user.Emailid,
                    subject: "Forgot Password Request",
                    text: message
                });
                // res.status(200).json({ success: true, message: "Email sented" })
                return res.status(200).json({ success: true,responsecode:200, message: "Forgetpassword send user","data": {
                    "Result": true
                } })
            }
            catch (error) {
                res.status(500).json({ success: false, error: "Email could not be sent" })
            }
        }
        catch (err) {
            res.status(400).json({ error: `${err}` });
        }
    }

    return {
        SaveAndUpdatePatintRegistration,
        sendotp,
        verifyotp,
        bookAppointment,
       
        mappingkit,
        Login,
        forgotpassword,
        // fileUpload
        // login,
        // forgotpassword,
        // reset,


        getPatientKitMappingBymacId,
        validatemacId,
        getPatientKitMappingByPaitentid,
        getallkit,
        getMeetingDoctorlistBypatientid,
        UpdatePatientidmacid,
        SaveAndUpdatePatientkitMapping,
        GetMaxNumberForPatientKitId
    }
}

module.exports = patientController()

