var express = require('express');
var router = express.Router();
const patientRoute = require('./patient')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('index');
});

router.use('/patient', patientRoute);

module.exports = router;
