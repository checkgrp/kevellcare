const express = require('express');
const router = express.Router();
const patientController = require('../controllers/patientController')
// const {
//   validateUserSignUpfields,
  // validateUserLoginfields,
//   validateUserForgotPasswordFields,
//   validateUserResetPasswordFields
// } = require('../validations/patientValidation');
// const {verifyPatientAuthentication} = require('./verifyPatientAuthentication');
const upload = require('../utils/multer')
//upload.array('file',3)
router.post('/SaveAndUpdatePatintRegistration', patientController.SaveAndUpdatePatintRegistration); 
router.post('/SendOTPInPatient', patientController.sendotp);
router.post('/verifyotp', patientController.verifyotp);
router.get('/Login', patientController.Login);
router.post('/ForgetUserPassword', patientController.forgotpassword);



// router.post('/BookAppointment', patientController.bookAppointment);
// router.get("/kitmapping",patientController.mappingkit)
// router.get("/getPatientKitMappingBymacId",patientController.getPatientKitMappingBymacId)
// router.get("/getPatientKitMappingByPaitentid",patientController.getPatientKitMappingByPaitentid)
// router.get("/getallkit",patientController.getallkit)
// router.get("/validatemacId",patientController.validatemacId)
// router.get("/getMeetingDoctorlistBypatientid",patientController.getMeetingDoctorlistBypatientid)
// router.put("/updatePatientidmacid",patientController.UpdatePatientidmacid)
// router.get("/getMaxNumberForPatientKitId",patientController.GetMaxNumberForPatientKitId)
// router.put("/saveAndUpdatePatientkitMapping",patientController.SaveAndUpdatePatientkitMapping)


// router.post('/fileupload',patientController.fileUpload)




module.exports = router;

