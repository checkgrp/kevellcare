const { createServer } = require("http");

const cors = require("cors");

const app = require("express")();
const WebSocket = require("ws");

const port = process.env.PORT || 7011;
const server = createServer(app);
const wss = new WebSocket.Server({ path: "/K00002", server });
const logger = require("./initlog");
const { to, detect } = require("./helper");
const { v4: uuidv4 } = require('uuid');

app.use(cors());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

const sockets = {};
let arr = [];

wss.on("connection", (ws, req) => {
  const userId = uuidv4();

  if (arr.length < 2) {
    logger.info(JSON.stringify(`${userId} connected`));
    arr.push(userId);
    ws.id = userId;
    console.log("connected clients - ", arr);
    sockets[userId] = ws;
  } else {
    logger.info(
      JSON.stringify(`detected ${userId} But not connected to websocket`)
    );
  }

  ws.on("message", function incoming(data, isBinary) {
    var msg = isBinary ? data : data.toString();
    // console.log(msg,"msg---")

    let data1 = "";

    // try {
    //wshealth

    // let a = JSON.parse(JSON.stringify(msg));
    // console.log(a,"----")
    // data1 = "";
    // if (a.state == "wshealth") {
    //   data1 = detect(msg);
    // }
    // else{
    if (msg === "JOINED:Doctor" || msg === "JOINED:Patient") {
      data1 = detect(msg);
    }
    // }
    // } catch (err) {
    //   //handshake
    //   data1 = "";
    //   if (msg === "JOINED:Doctor" || msg === "JOINED:Patient") {
    //     data1 = detect(msg);
    //   }
    // }

    var sender = arr.filter((val) => val == ws.id);
    if (
      // arr.length === 1 ||
      sender.length == 0
    ) {
      logger.info(JSON.stringify(`Can't able to send msg`));
    } else {
      if (
        data1 == "Doctor" ||
        data1 == "Patient" ||
        data1.command == "wshealth"
      ) {
        to(sockets, ws.id, data1);
      } else {
        var send = arr.filter((val) => val != ws.id);
        to(sockets, send[0], msg);
      }
    }
  });

  ws.on("close", function incoming(message) {
    console.log(userId)
    delete sockets[userId];
    // arr.splice(arr.length - 1, 1);
    arr = arr.filter(v=>v!=userId)
    logger.info(JSON.stringify(`${userId} Client gets disconnected`));
    console.log("disconnected - ", userId);
    console.log("connected clients - ", arr);
  });
});

server.listen(port, () => {
  console.log(`Starting server on port ${port}`);
});
