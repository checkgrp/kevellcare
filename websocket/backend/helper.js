const WebSocket = require("ws")
const logger = require('./initlog')


module.exports = {
    to(sockets, user, data) {

        if (sockets[user] && sockets[user].readyState === WebSocket.OPEN) {
            
            if (data == 'Doctor' || data == 'Patient' || data.command == 'wshealth') {

                console.log('handshake || wshealth')
                logger.info(JSON.stringify(`message started timestamp -> ${new Date().getTime()}`));
                console.log(`Message send to -> ${user} message-> ${data}`)
                logger.info(JSON.stringify(`Message send to -> ${user} message-> ${data}`));
                
                if(data.command == 'wshealth'){
                    //wshealth
                    sockets[user].send(JSON.stringify(data))
                }
                else{
                    //handshake
                    sockets[user].send(JSON.stringify({
                        message:`${data} connected`
                    }))
                }
            }
            else {
                
                logger.info(JSON.stringify(`message started timestamp -> ${new Date().getTime()}`));
                console.log(`Message send to -> ${user} message-> ${data}`)
                logger.info(JSON.stringify(`Message send to -> ${user} message-> ${data}`));
                
                //remaining
            sockets[user].send(data);
            
        }
            logger.info(JSON.stringify(`message ended timestamp -> ${new Date().getTime()}`));
        }
    },

    detect(str) {
        try {
            //json
            let a =JSON.parse(str)
            if(a.state == 'wshealth'){
                return { 
                    id: a.id,
                    Type:a.type,
                    Pateint: "PateintID",
                    command: "wshealth",
                    date: new Date().getTime()
                }
            }
        } catch (e) {
            //string
        if (str.indexOf('Doctor') > 0) {
            return str.replace('JOINED:', '')
        }
        else if (str.indexOf('Patient') > 0) {
            return str.replace('JOINED:', '')
        }
        else {
            logger.info(JSON.stringify('New string detected - ', str));
            console.log('New string detected - ', str)
        }
        }
    }
}