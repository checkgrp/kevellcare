const winston = require('winston');
var now = new Date();

const logConfiguration = {
    'transports': [
        new winston.transports.File({
            filename: './logs/websocket' + now.getFullYear() + "-" + String(now.getMonth() + 1).padStart(2, '0') + "-" + now.getDate() + '.log'
        })
    ],
    format: winston.format.combine(
        winston.format.label({
            label: `websocket`
        }),
        winston.format.timestamp({
            format: 'MMM-DD-YYYY HH:mm:ss'
        }),
        winston.format.printf(info => `${info.level}: ${info.label}: ${[info.timestamp]}: ${info.message}`),
    )
};
const logger = winston.createLogger(logConfiguration);
module.exports = logger;
