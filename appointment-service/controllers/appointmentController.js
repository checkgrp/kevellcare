const helper = require("../utils/helper");
const { createJwt, encrypt, decrypt } = require('../utils/common');
const { validationResult } = require('express-validator');

const {
    PatientRegistration,
    Users,
    PatientKitMapping,
    Specialist
} = require('../models/kevellCareModels')
const crypto = require('crypto')
const moment = require('moment')
const mongoose = require('mongoose')
const APIResp = require('../utils/APIResp.js')

const patientController = () => {
   

    const bookAppointment = async (req, res) => {
        try {
            let userInput = helper.getReqValues(req)
            console.log(userInput,"-----")
            const patient = await PatientRegistration.find({ _id: userInput.patientId })
      
            try {
                // userInput.sno = crypto.randomBytes(4).toString("hex");
                userInput.isvisited = false
                userInput.date = moment().format("YYYY/MM/DD HH:mm:ss")
                userInput.userdoctorrating = 0
                userInput.userdoctorcommand = ""
                userInput.patientpdflink = "" 
userInput.appointmentdate = moment(userInput.appointmentdate).format("YYYY-MM-DD")
userInput.appointmentdateforstring = moment(userInput.appointmentdateforstring).format("YYYY-MM-DD")
console.log("userInput",userInput)

if(patient[0].patientinfo.Appointmentinfo == null){

console.log("nulll")
    var arr=[]
    arr.push(userInput)
    userInput.sno = 1
    await PatientRegistration.findOneAndUpdate({
        _id: userInput.patientId
    },
        {
            $set: { 
                "patientinfo.Appointmentinfo": arr,
                'appointmentdate':moment().format('YYYY-MM-DD')
            }
        })

        
}

else{
console.log("not nulll")

    userInput.sno = patient[0].patientinfo.Appointmentinfo.length+1
    await PatientRegistration.findOneAndUpdate({
        _id: userInput.patientId
    },
        {
            $push: { "patientinfo.Appointmentinfo": userInput }
        })
        await PatientRegistration.updateOne({
            _id: userInput.patientId
            },{
                $set:{
                    'appointmentdate':moment().format('YYYY-MM-DD')
                }
            })
}
                
                // res.status(200).json({
                //     success: true,
                //     message: `${patient.patientinfo.username} - Booked Appointment (${patient._id})`,
                // })
                APIResp.getSuccessResultsonly({Result:"true"}, "Success", res)

            } catch (err) {
                console.log(err)
                // res.status(400).send({ error: `${err}` });
                APIResp.getErrorResult(err, res)
            }

        }
        catch (err) {
            // res.status(400).json({ error: `${err}` });
            console.log(err)
            APIResp.getErrorResult(err, res)
        }
    }




    const changeAppointment = async (req, res) => {
    
           
        try {
            let userInput = helper.getReqValues(req)
            console.log("userInput",userInput)
            

            // const docotr = await Users.findOne({ _id: userInput.doctornameid })
            // if (!docotr) return res.status(400).json({ success: false,responsecode:201, message: "doctornameid does not exist!!","data": {
            //     "Result": false
            // } })
    
            const doctorprofilelist = await PatientRegistration.aggregate(
                [
                    {
                      '$unwind': {
                        'path': '$patientinfo.Appointmentinfo'
                      }
                    }, {
                      '$match': {
                        'patientinfo.Appointmentinfo.patientId': userInput.PatientAppointmentDetail[0].patientId ,
                        'patientinfo.Appointmentinfo.sno':  userInput.PatientAppointmentDetail[0].sno
                      }
                    }
                  ]
            )
            // res.status(200).json({ success: true, result: doctorprofilelist })
            // if(doctorprofilelist.length ==0)      return res.status(400).json ({ success: false,responsecode:201, message: "patientid and sno dismatch!","data": {
            //     "Result": false
            // } })
           
   


            const doctornamecheck = await Users.find({
                _id:userInput.DoctorId,Username:userInput.DoctorName,
            }).populate([{
                path: "SpecialistId",
                model: "Specialist"
            }])
              console.log(doctornamecheck,"doctornamecheck")  
            
            if(doctornamecheck.length ==0) return res.status(400).json({ error: 'doctorname is not found' })
            

            
            if (helper.convertarrtoObj(doctornamecheck)._doc.SpecialistId.name != userInput.Specialist) return res.status(400).json({ error: 'Invalid SpecialistName ' })

   

            
        //    const personupdate = await PatientRegistration.updateOne({ 

        //     'patientinfo.Appointmentinfo.$.patientId': userInput.patientId,
        //     'patientinfo.Appointmentinfo.$.sno': userInput.sno,
        // }, 
        //     {
        //          $set: { 
        //             'patientinfo.Appointmentinfo.$.doctornameid': userInput.doctornameid,
        //    "patientinfo.Appointmentinfo.$.doctorname": userInput.doctorname,
        //    "patientinfo.Appointmentinfo.$.doctorpractitionername": userInput.doctorpractitionername 
        
        // } })
        const personupdate = await PatientRegistration.updateOne({ 

            // _id: mongoose.Types.ObjectId(),
            'patientid':userInput.PatientAppointmentDetail[0].patientId,
            'patientinfo.Appointmentinfo.sno':userInput.PatientAppointmentDetail[0].AppointmentId
        }, 
            {
                 $set: { 
                    'patientinfo.Appointmentinfo.$.doctornameid': parseInt(userInput.DoctorId),
           "patientinfo.Appointmentinfo.$.doctorname": userInput.DoctorName,
            "patientinfo.Appointmentinfo.$.doctorpractitionername": userInput.Specialist,
            "patientinfo.Appointmentinfo.$.doctorpractitionerid": userInput.SpecialistId,
            "patientinfo.Appointmentinfo.$.appointmentlocation": userInput.City 
        
        } }
    
        )
           console.log("personupdate",personupdate)
        //    res.status(200).send({
        //     personupdate,
        //       message: "update Successfully"
        //    });
        APIResp.getSuccessResultsonly("", "Success", res)
        }
        catch (err) {
           console.log(err);
        }
            // res.status(200).json({ success: true, result: a })
}

        
       
    

 
   
const gettoken=async (req,res)=>{
    try{
let a=await createJwt('Appointment')
// res.send(a)
res.status(200).json({ access_token: a, Error: null })
    }
    catch(err){
        res.status(400).json({Error:err})
    }
}
 
   

   


    

    
   

   

    
    

   

    return {
       
        bookAppointment,
        changeAppointment,
        gettoken
        // fileUpload
        // login,
        // forgotpassword,
        // reset

        
    }
}

module.exports = patientController()

