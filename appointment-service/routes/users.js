const express = require('express');
const router = express.Router();
const patientController = require('../controllers/appointmentController')
const {verifyToken} = require('../utils/verifyToken')


router.post('/BookAppointment',patientController.bookAppointment);
router.post('/ChangeAppointment',verifyToken,patientController.changeAppointment);

router.get('/gettoken',patientController.gettoken);

module.exports = router;









